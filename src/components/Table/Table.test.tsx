import { ReactWrapper, ShallowWrapper } from "enzyme";
import faker from "faker";
import { Factory } from "rosie";

import { mountComponent, shallowComponent } from "../../utils/testUtils";
import { ColumnType, TableProps } from "./Table.styles";
import Table from ".";

faker.seed(312);

Factory.define("tableData")
  .attr("firstName", () => faker.name.firstName())
  .attr("lastName", () => faker.name.lastName())
  .attr("price", () => faker.finance.amount())
  .attr("occupation", () => faker.name.jobTitle())
  .attr("employer", () => faker.company.companyName());

const tableData: any = Factory.buildList("tableData", 100);

describe("Table", () => {
  let props: TableProps;
  let mountTable: ReactWrapper | undefined;
  let shallowTable: ShallowWrapper | undefined;

  const mountTestComponent = (): ReactWrapper => {
    if (!mountTable) {
      mountTable = mountComponent(Table, props);
    }

    return mountTable;
  };

  const shallowTestComponent = (): ShallowWrapper => {
    if (!shallowTable) {
      shallowTable = shallowComponent(Table, props);
    }

    return shallowTable;
  };

  beforeEach(() => {
    props = {
      columns: [
        {
          header: "First Name",
          accessor: "firstName",
          type: ColumnType.string
        },
        {
          header: "Last Name",
          accessor: "lastName"
        },
        {
          header: "Price",
          accessor: "price",
          type: ColumnType.number
        },
        {
          header: "Occupation",
          accessor: "occupation"
        },
        {
          header: "Employer",
          accessor: "employer",
          type: ColumnType.data
        }
      ],
      data: tableData,
      pagination: true
    };

    Element.prototype.getBoundingClientRect = jest.fn(
      () =>
        ({
          width: 1280
        } as any)
    );

    mountTable = undefined;
    shallowTable = undefined;
  });

  it("should render correctly", () => {
    expect(shallowTestComponent()).toMatchSnapshot();
  });

  it("should mount correctly", () => {
    expect(mountTestComponent().find(Table)).toBeDefined();
  });
});
