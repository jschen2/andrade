import React from "react";
import styled from "styled-components";
import { Color } from "../../types/enums";
import { getColor, transition } from "../../utils";

export const StyledLink = styled.a<React.HTMLAttributes<HTMLAnchorElement>>`
  color: ${getColor(Color.blue, 500)};
  padding: 0;
  position: relative;
  text-decoration: none;

  &:before,
  &:after {
    background-color: ${getColor(Color.blue, 500)};
    bottom: 0;
    content: "";
    height: 1px;
    left: 0;
    position: absolute;
    width: 100%;
  }

  &:after {
    bottom: -1px;
    left: 50%;
    ${transition("left width")};
    width: 0;
  }

  &:hover {
    &:after {
      left: 0;
      width: 100%;
    }
  }

  &:visited {
    color: ${getColor(Color.purple, 500)};

    &:before,
    &:after {
      background-color: ${getColor(Color.purple, 500)};
    }
  }
`;

export default StyledLink;
