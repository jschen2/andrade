import styled from "styled-components";
import { Color, Font } from "../../../types/enums";
import { getColor, getFont } from "../../../utils";

const StyledSVGYAxis = styled.g`
  .tick text {
    color: ${getColor(Color.gray, 500)};
    font-family: ${getFont(Font.body)};
    font-size: 0.75rem;
  }
  path,
  line {
    stroke: ${getColor(Color.gray, 300)};
  }
`;

export default StyledSVGYAxis;
