import styled from "styled-components";

const StyledSidebarHeader = styled.div`
  align-items: center;
  display: flex;
  flex: 0 0 54px;
  font-weight: bold;
  justify-content: center;
  overflow: hidden;
  white-space: nowrap;
`;

export default StyledSidebarHeader;
