import styled from "styled-components";
import { Color } from "../../types/enums";
import { borderRadius, getColor, hexToRgb } from "../../utils";

export interface StyledCalendarProps {
  height: number;
}

export interface CalendarDayProps {
  isSelected: boolean;
  isWeekend: boolean;
  left: number;
  top: number;
}

const calendarWidth = "224px";

const StyledCalendar = styled.div<StyledCalendarProps>`
  border: 1px solid
    rgba(${props => hexToRgb(getColor(Color.blue, 100)(props))}, 0.32);
  ${borderRadius};
  height: ${props => props.height}px;
  overflow: hidden;
  margin-top: 0.5rem;
  position: relative;
  width: ${calendarWidth};
`;

export const CalendarDay = styled.div<CalendarDayProps>`
  align-items: center;
  background-color: ${props =>
    props.isSelected
      ? getColor(Color.purple, 100)
      : props.isWeekend
      ? getColor(Color.blue, 100)
      : ""};
  cursor: pointer;
  display: flex;
  height: 32px;
  justify-content: center;
  left: ${props => props.left}px;
  position: absolute;
  width: 32px;
  top: ${props => props.top}px;
`;

export default StyledCalendar;
