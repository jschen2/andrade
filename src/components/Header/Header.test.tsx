import { ReactWrapper, ShallowWrapper } from "enzyme";
import { mountComponent, shallowComponent } from "../../utils/testUtils";

import Header, { HeaderProps } from ".";

describe("Header", () => {
  let props: HeaderProps;
  let mountHeader: ReactWrapper | undefined;
  let shallowHeader: ShallowWrapper | undefined;

  const mountTestComponent = (): ReactWrapper => {
    if (!mountHeader) {
      mountHeader = mountComponent(Header, props);
    }

    return mountHeader;
  };

  const shallowTestComponent = (): ShallowWrapper => {
    if (!shallowHeader) {
      shallowHeader = shallowComponent(Header, props);
    }

    return shallowHeader;
  };

  beforeEach(() => {
    props = {
      size: 1
    };

    mountHeader = undefined;
    shallowHeader = undefined;
  });

  it("should render correctly", () => {
    expect(shallowTestComponent()).toMatchSnapshot();
  });

  it("should 'h1' correctly", () => {
    expect(mountTestComponent().find("h1").length).toBe(1);
  });

  describe("if 'size' is equal to 2", () => {
    beforeEach(() => {
      props.size = 2;
    });

    it("should render correctly", () => {
      expect(shallowTestComponent()).toMatchSnapshot();
    });

    it("should 'h2' correctly", () => {
      expect(mountTestComponent().find("h2").length).toBe(1);
    });
  });

  describe("if 'size' is equal to 3", () => {
    beforeEach(() => {
      props.size = 3;
    });

    it("should render correctly", () => {
      expect(shallowTestComponent()).toMatchSnapshot();
    });

    it("should 'h3' correctly", () => {
      expect(mountTestComponent().find("h3").length).toBe(1);
    });
  });

  describe("if 'size' is equal to 4", () => {
    beforeEach(() => {
      props.size = 4;
    });

    it("should render correctly", () => {
      expect(shallowTestComponent()).toMatchSnapshot();
    });

    it("should 'h4' correctly", () => {
      expect(mountTestComponent().find("h4").length).toBe(1);
    });
  });

  describe("if 'size' is equal to 5", () => {
    beforeEach(() => {
      props.size = 5;
    });

    it("should render correctly", () => {
      expect(shallowTestComponent()).toMatchSnapshot();
    });

    it("should 'h5' correctly", () => {
      expect(mountTestComponent().find("h5").length).toBe(1);
    });
  });

  describe("if 'size' is equal to 6", () => {
    beforeEach(() => {
      props.size = 6;
    });

    it("should render correctly", () => {
      expect(shallowTestComponent()).toMatchSnapshot();
    });

    it("should 'h6' correctly", () => {
      expect(mountTestComponent().find("h6").length).toBe(1);
    });
  });
});
