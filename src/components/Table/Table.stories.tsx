import faker from "faker";
import React from "react";
import { Factory } from "rosie";
import { boolean } from "@storybook/addon-knobs";

import TableReadme from "./README.md";
import { ColumnType } from "./Table.styles";
import Table from ".";

Factory.define("tableData")
  .attr("firstName", () => faker.name.firstName())
  .attr("lastName", () => faker.name.lastName())
  .attr("price", () => faker.finance.amount())
  .attr("occupation", () => faker.name.jobTitle())
  .attr("employer", () => faker.company.companyName());

const tableData: any = Factory.buildList("tableData", 100);

const columns = [
  {
    header: "First Name",
    accessor: "firstName"
  },
  {
    header: "Last Name",
    accessor: "lastName"
  },
  {
    header: "Price",
    accessor: "price",
    type: ColumnType.number
  },
  {
    header: "Occupation",
    accessor: "occupation"
  },
  {
    header: "Employer",
    accessor: "employer"
  }
];

export default {
  component: Table,
  parameters: {
    info: {
      inline: true,
      text: TableReadme
    }
  },
  title: "Table"
};

export const BasicUse = (): React.ReactElement => (
  <div style={{ height: "500px" }}>
    <Table
      columns={columns}
      data={tableData}
      dragSelect={boolean("Drag select?", false)}
      multiSelect={boolean("Multi select?", false)}
      showHideColumns={boolean("Show/hide columns?", true)}
    />
  </div>
);

export const WithPagination = (): React.ReactElement => (
  <Table
    columns={columns}
    data={tableData}
    dragSelect={boolean("Drag select?", false)}
    multiSelect={boolean("Multi select?", false)}
    pagination={boolean("Pagination?", true)}
    showHideColumns={boolean("Show/hide columns?", true)}
  />
);

export const WithMultiSelect = (): React.ReactElement => (
  <Table
    columns={columns}
    data={tableData}
    dragSelect={boolean("Drag select?", false)}
    multiSelect={boolean("Multi select?", true)}
    showHideColumns={boolean("Show/hide columns?", false)}
  />
);

export const WithDragSelect = (): React.ReactElement => (
  <Table
    columns={columns}
    data={tableData}
    dragSelect={boolean("Drag select?", true)}
    multiSelect={boolean("Multi select?", false)}
    showHideColumns={boolean("Show/hide columns?", false)}
  />
);
