import uniqueId from "lodash/uniqueId";
import React from "react";
import { Placement } from "../../types/enums";

import { isNullOrUndefined } from "../../utils";
import Icon from "../Icon";

import StyledNavbarButton, {
  NavbarButtonPopover,
  NavbarButtonProps
} from "./NavbarButton.styles";

const NavbarButton = ({
  children,
  id,
  ...props
}: NavbarButtonProps): React.ReactElement => {
  const buttonRef: React.RefObject<any> = React.useRef();
  const [isOpen, setIsOpen] = React.useState(false);
  const popoverId = React.useRef(id || uniqueId("navbar-button-"));
  const buttonChildren = React.Children.toArray(children);
  const buttonContent: React.ReactNode[] = [];
  const popoverContent: React.ReactElement[] = [];
  buttonChildren.forEach((child: React.ReactElement | string) => {
    if (!isNullOrUndefined(child)) {
      if (
        typeof child === "string" ||
        (child.type as React.ComponentType).displayName !==
          "NavbarDropdownContent"
      ) {
        buttonContent.push(child);
      } else {
        popoverContent.push(child);
      }
    }
  });

  const onMouseOver = (): void => setIsOpen(true);
  const onMouseOut = (e: React.MouseEvent | React.FocusEvent): void => {
    if (
      buttonRef.current &&
      !(buttonRef.current as Element).contains(e.relatedTarget as Element)
    ) {
      setIsOpen(false);
    }
  };

  return (
    <StyledNavbarButton
      id={popoverId.current}
      onMouseOver={onMouseOver}
      onMouseOut={onMouseOut}
      onFocus={onMouseOver}
      onBlur={onMouseOut}
      ref={buttonRef}
      {...props}
    >
      {...buttonContent}
      {popoverContent.length > 0 && (
        <>
          <Icon className="fa-ellipsis-v" noPadding={true} />
          <NavbarButtonPopover
            isOpen={isOpen}
            node={buttonRef.current}
            offsetLeft={0}
            offsetTop={-8}
            onMouseOut={onMouseOut}
            onBlur={onMouseOut}
            placement={Placement.bottomStart}
            showArrow={false}
            target={popoverId.current}
          >
            {...popoverContent}
          </NavbarButtonPopover>
        </>
      )}
    </StyledNavbarButton>
  );
};

export default NavbarButton;
