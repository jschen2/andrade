import React from "react";
import styled, { css } from "styled-components";
import Flex, { FlexProps } from "../Flex";
import {
  borderRadius,
  borderRadiusNumber,
  combineClasses,
  getColor,
  getShadow,
  transition
} from "../../utils";
import { Color } from "../../types/enums";
import Icon from "../Icon";

export interface AccordionProps extends FlexProps {
  title: string;
  isOpen: boolean;
}

export const AccordionContent = styled.div`
  animation: rollOut 332ms ease forwards;
  background-color: ${getColor(Color.gray, 0)};
  border-radius: 0 0 ${borderRadiusNumber}rem ${borderRadiusNumber}rem;
  ${getShadow(0)};
  overflow: hidden;
  margin-top: 0rem;

  @keyframes rollOut {
    from {
      height: 0;
      padding: 0 0.5rem;
    }

    to {
      height: max-content;
      padding: 0.75rem 0.5rem 0.5rem 0.5rem;
    }
  }
`;

export const AccordionTitle = styled.div`
  font-weight: bold;
  padding: 0.375rem;
`;

export const AccordionArrow = styled(({ className, isOpen, ...props }) => (
  <Icon className={combineClasses(className, "fa-chevron-down")} {...props} />
))<{ isOpen: boolean }>`
  transform: rotate(0deg);
  ${transition("transform")};

  ${props =>
    props.isOpen &&
    css`
      transform: rotate(180deg);
    `};
`;

const StyledAccordion = styled(Flex)`
  ${getShadow(0)};
  ${borderRadius};
`;

export default StyledAccordion;
