import React, { Dispatch, SetStateAction } from "react";

export interface SidebarContextProps {
  expanded?: boolean;
  setExpanded?: Dispatch<SetStateAction<boolean>>;
}

export interface SidebarProviderProps extends React.HTMLAttributes<any> {
  expanded?: boolean;
}

export const SidebarContext = React.createContext<SidebarContextProps>({
  expanded: true,
  setExpanded: () => undefined
});

const SidebarProvider = ({
  children,
  expanded
}: SidebarProviderProps): React.ReactElement => {
  const [expandedSidebar, setExpandedSidebar] = React.useState(expanded);
  return (
    <SidebarContext.Provider
      value={{ expanded: expandedSidebar, setExpanded: setExpandedSidebar }}
    >
      {children}
    </SidebarContext.Provider>
  );
};

SidebarProvider.defaultProps = {
  expanded: true
};

export default SidebarProvider;
