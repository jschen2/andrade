import React from "react";
import styled, { css } from "styled-components";
import { borderRadius, getColor, hexToRgb, transition } from "../../utils";
import { ButtonSize, Color } from "../../types/enums";
import Flex from "../Flex";

export interface ButtonProps extends React.HTMLAttributes<HTMLButtonElement> {
  /** Background or outline color */
  color: Color;
  /** Set disabled state */
  disabled?: boolean;
  /** Sets ghost button styling */
  ghost?: boolean;
  /** Set regular or outline button */
  outline?: boolean;
  /** Change amount of padding */
  size?: ButtonSize;
}

export const Button = styled(({ color, ghost, outline, size, ...props }) => (
  <button type="button" {...props} />
))<ButtonProps>`
  background-color: transparent;
  border: 2px solid ${props => getColor(props.color, 500)(props)};
  ${borderRadius};
  box-sizing: border-box;
  color: ${getColor(Color.gray, 100)};
  cursor: pointer;
  font-weight: bold;
  line-height: 1;
  padding: 0.5rem 0.75rem;
  position: relative;
  ${transition("background-color border-color color")};

  ${props =>
    props.disabled &&
    css`
      cursor: not-allowed;
      opacity: 0.5;
    `};

  ${props =>
    props.ghost &&
    css`
      border: none;
    `};

  ${props =>
    props.size === ButtonSize.sm &&
    css`
      padding: 0.25rem 0.5rem;
    `};

  ${props =>
    props.size === ButtonSize.lg &&
    css`
      padding: 0.75rem 1rem;
    `};

  ${props =>
    !props.outline && !props.ghost
      ? css`
          background-color: ${getColor(props.color, 500)};
        `
      : css`
          color: ${getColor(props.color, 500)};

          &:hover,
          &:active {
            color: ${getColor(Color.gray, 100)};
          }
        `};

  &:after {
    border: 2px solid transparent;
    ${borderRadius};
    content: "";
    height: calc(100% + 6px);
    left: -5px;
    position: absolute;
    top: -5px;
    ${transition("border-color")};
    width: calc(100% + 6px);
    z-index: -1;
  }

  &:hover {
    background-color: ${props => getColor(props.color, 300)};
    border-color: ${props => getColor(props.color, 300)};
    outline: none;
  }

  &:active {
    background-color: ${props => getColor(props.color, 600)};
    border-color: ${props => getColor(props.color, 600)};
    outline: none;
  }

  &:focus {
    outline: none;

    &:after {
      background-color: rgba(
        ${props => hexToRgb(getColor(props.color, 500)(props))},
        0.35
      );
    }
  }
`;

export const ButtonContainer = styled(Flex)`
  ${Button} {
    margin: 0 0.25rem;
  }
`;

Button.defaultProps = {
  color: Color.gray,
  size: ButtonSize.md
};

export default Button;
