import React from "react";

import Header from "../../../Header";
import { FlexProps } from "../../../Flex";
import { Alignment } from "../../../../types/enums";
import StyledModalHeader, { CloseButton } from "./ModalHeader.styles";

export interface ModalHeaderProps extends FlexProps {
  title: string;
  toggle(): void;
}

const ModalHeader = ({
  title,
  toggle,
  ...props
}: ModalHeaderProps): React.ReactElement => (
  <StyledModalHeader
    alignItems={Alignment.center}
    justifyContent={Alignment.spaceBetween}
    {...props}
  >
    <Header size={3}>{title}</Header>
    <CloseButton
      onClick={toggle}
      onKeyDown={toggle}
      role="button"
      tabIndex={0}
    />
  </StyledModalHeader>
);

export default ModalHeader;
