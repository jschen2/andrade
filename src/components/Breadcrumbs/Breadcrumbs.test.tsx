import { ReactWrapper, ShallowWrapper } from "enzyme";
import React from "react";

import { mountWithTheme, shallowComponent } from "../../utils/testUtils";

import Breadcrumbs, { Breadcrumb } from ".";

describe("Breadcrumbs", () => {
  let props: any;
  let mountBreadcrumbs: ReactWrapper | undefined;
  let shallowBreadcrumbs: ShallowWrapper | undefined;

  const mountTestComponent = (): ReactWrapper => {
    if (!mountBreadcrumbs) {
      const { active, otherProps } = props;
      mountBreadcrumbs = mountWithTheme(
        <Breadcrumbs {...otherProps}>
          <Breadcrumb active={active}>Test</Breadcrumb>
        </Breadcrumbs>
      );
    }

    return mountBreadcrumbs;
  };

  const shallowTestComponent = (): ShallowWrapper => {
    if (!shallowBreadcrumbs) {
      shallowBreadcrumbs = shallowComponent(Breadcrumbs, props);
    }

    return shallowBreadcrumbs;
  };

  beforeEach(() => {
    props = {
      active: false
    };

    mountBreadcrumbs = undefined;
    shallowBreadcrumbs = undefined;
  });

  it("should render correctly", () => {
    expect(shallowTestComponent()).toMatchSnapshot();
  });

  it("should mount correctly", () => {
    expect(mountTestComponent().find(Breadcrumbs)).toBeDefined();
  });

  describe("if 'active' is set on Breadcrumb", () => {
    beforeEach(() => {
      props.active = true;
    });

    it("should mount correctly", () => {
      expect(mountTestComponent().find(Breadcrumbs)).toBeDefined();
    });
  });
});
