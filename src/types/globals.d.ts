declare module "*.md" {
  const content: string;
  export = content;
}
declare module "*.css";
declare module "*.ttf";
declare module "*.jpg";
