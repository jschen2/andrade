import React from "react";
import styled, { css } from "styled-components";

import {
  borderRadius,
  combineClasses,
  getColor,
  getShadow,
  transition
} from "../../utils";
import { Color } from "../../types/enums";
import Popover from "../Popover";
import Icon, { StyledIcon } from "../Icon";
import Container from "../Container";
import { SidebarContextProps } from "../SidebarContext";
import Flex from "../Flex";

export interface SidebarButtonProps
  extends React.HTMLAttributes<HTMLButtonElement>,
    SidebarContextProps {
  active?: boolean;
  subButtons?: SidebarButtonProps[];
}

export const SidebarSubButtonGroup = styled.div`
  padding-left: 0;
  padding-right: 0;

  & > ${Container} {
    ${borderRadius};
    ${getShadow(0)};
  }
`;

export const SidebarButtonToggle = styled(
  ({ className, showChildren, ...props }) => (
    <Icon
      className={combineClasses(
        className,
        `fa-chevron-${showChildren ? "up" : "down"}`
      )}
      {...props}
    />
  )
)`
  align-items: center;
  ${borderRadius};
  display: flex;
  height: 2rem;
  justify-content: center;
  position: absolute;
  right: 0;
  ${transition("background-color")};
  top: 0;
  width: 2rem;
  &:hover {
    background-color: ${getColor(Color.gray, 200)};
  }
  &:focus {
    border: none;
    outline: none;
  }
`;

export const SidebarButtonPopover = styled(Popover)`
  padding: 0;
`;

const StyledSidebarButton = styled.button<SidebarButtonProps>`
  background-color: transparent;
  border: none;
  ${borderRadius};
  display: flex;
  flex: 0 0 100%;
  line-height: 1rem;
  outline: none;
  ${transition("background-color color")};

  &:hover {
    background-color: ${getColor(Color.gray, 125)};
    color: ${getColor(Color.gray, 900)};
    ${getShadow(0)};
  }

  &:active,
  &:focus {
    background-color: ${getColor(Color.gray, 150)};
    outline: none;
  }

  & > ${Flex} {
    overflow: hidden;
    white-space: nowrap;
    width: 100%;

    ${StyledIcon} {
      align-items: center;
    }

    > ${Container} {
      padding-left: 0.5rem;
      text-align: left;
    }
  }

  ${props =>
    props.active &&
    css`
      background-color: ${getColor(Color.gray, 150)};
      position: relative;

      &:before {
        background-color: ${getColor(Color.purple, 300)};
        border-radius: 0.25rem 0 0 0.25rem;
        content: "";
        height: 100%;
        left: 0;
        position: absolute;
        top: 0;
        width: 0.25rem;
      }
    `};
`;

export default StyledSidebarButton;
