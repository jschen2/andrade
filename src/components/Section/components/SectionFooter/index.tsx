import styled from "styled-components";

const SectionFooter = styled.div`
  display: flex;
  flex: 0 0 40px;
  flex-direction: row;
  align-items: center;
  justify-content: flex-end;
`;

export default SectionFooter;
