import React from "react";

import WelcomeReadme from "./README.md";

export default {
  parameters: {
    info: {
      header: false,
      inline: true,
      source: false,
      text: WelcomeReadme
    }
  },
  title: "Getting Started"
};

export const Andrade = (): React.ReactElement => (
  <div style={{ marginTop: "-20px" }} />
);
