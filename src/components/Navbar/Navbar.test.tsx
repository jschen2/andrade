import { ReactWrapper, ShallowWrapper } from "enzyme";
import { mountComponent, shallowComponent } from "../../utils/testUtils";

import Navbar from ".";

describe("Navbar", () => {
  let props: any;
  let mountNavbar: ReactWrapper | undefined;
  let shallowNavbar: ShallowWrapper | undefined;

  const mountTestComponent = (): ReactWrapper => {
    if (!mountNavbar) {
      mountNavbar = mountComponent(Navbar, props);
    }

    return mountNavbar;
  };

  const shallowTestComponent = (): ShallowWrapper => {
    if (!shallowNavbar) {
      shallowNavbar = shallowComponent(Navbar, props);
    }

    return shallowNavbar;
  };

  beforeEach(() => {
    props = {};

    mountNavbar = undefined;
    shallowNavbar = undefined;
  });

  it("should render correctly", () => {
    expect(shallowTestComponent()).toMatchSnapshot();
  });

  it("should mount correctly", () => {
    expect(mountTestComponent().find(Navbar)).toBeDefined();
  });
});
