import React from "react";

import { Alignment } from "../../types/enums";
import FlexReadme from "./README.md";
import Flex from ".";

export default {
  component: Flex,
  parameters: {
    info: {
      inline: true,
      text: FlexReadme
    }
  },
  title: "Flex"
};

export const BasicUse = (): React.ReactElement => (
  <Flex
    alignItems={Alignment.center}
    justifyContent={Alignment.center}
    style={{
      height: "200px"
    }}
  >
    <div style={{ border: "1px solid black" }}>Content</div>
  </Flex>
);
