import { boolean, object, text } from "@storybook/addon-knobs";
import React from "react";

import SelectReadme from "./README.md";
import Select from ".";

const options = [
  {
    label: "Option 1",
    value: "Option 1"
  },
  {
    label: "Option 2",
    value: "Option 2"
  },
  {
    label: "Option 3",
    value: "Option 3"
  }
];

export default {
  component: Select,
  info: {
    inline: true,
    text: SelectReadme
  },
  title: "Select"
};

export const BasicUse = (): React.ReactElement => (
  <Select
    disabled={boolean("Disabled?", false)}
    id={text("ID", "Select")}
    label={text("Title", "Options")}
    options={object("Options", options)}
  />
);

export const MultiSelect = (): React.ReactElement => (
  <Select
    disabled={boolean("Disabled?", false)}
    id={text("ID", "Select")}
    label={text("Title", "Options")}
    multi={true}
    options={object("Options", options)}
  />
);
