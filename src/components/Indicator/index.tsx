import React from "react";
import { ButtonSize, Color } from "../../types/enums";

import { IndicatorProps, StyledIndicator } from "./Indicator.styles";

export const Indicator = (props: IndicatorProps): React.ReactElement => (
  <StyledIndicator {...props} />
);

Indicator.defaultProps = {
  color: Color.purple,
  intensity: 500,
  size: ButtonSize.md
};

export default Indicator;
