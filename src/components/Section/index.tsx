import React from "react";

import { StyledSection } from "./Section.styles";
import SectionTitle from "./components/SectionTitle";
import SectionBody from "./components/SectionBody";
import SectionFooter from "./components/SectionFooter";

export interface SectionProps extends React.HTMLAttributes<HTMLDivElement> {
  full?: boolean;
  shadow?: number;
}

export const Section = (props: SectionProps): React.ReactElement => (
  <StyledSection {...props} />
);

Section.Title = SectionTitle;
Section.Body = SectionBody;
Section.Footer = SectionFooter;

Section.defaultProps = {
  full: false,
  shadow: 0
};

export default Section;
