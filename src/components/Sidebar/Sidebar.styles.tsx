import styled from "styled-components";
import { getColor, getShadow, transition } from "../../utils";
import { Color } from "../../types/enums";
import Flex, { FlexProps } from "../Flex";
import { SidebarContextProps } from "../SidebarContext";

export type SidebarProps = FlexProps & SidebarContextProps;

export const SidebarBody = styled(Flex)`
  flex: 1 1 auto;
  flex-direction: column;
  padding: 0.5rem;
`;

const StyledSidebar = styled(Flex)<SidebarProps>`
  background-color: ${getColor(Color.gray, 0)};
  flex-direction: column;
  ${getShadow(0)};
  height: 100%;
  left: 0;
  max-width: 100%;
  position: fixed;
  top: 0;
  ${transition("width")};
  width: ${props => (props.expanded ? "224px" : "46px")};
  z-index: 1031;
`;

export default StyledSidebar;
