import React from "react";

import GridReadme from "./README.md";
import Grid from ".";

export default {
  component: Grid,
  parameters: {
    info: {
      inline: true,
      text: GridReadme
    }
  },
  title: "Grid"
};

export const BasicUse = (): React.ReactElement => (
  <Grid>
    <div style={{ border: "1px solid black" }}>Content</div>
  </Grid>
);

export const With12Cols = (): React.ReactElement => (
  <Grid cols={12}>
    {new Array(12).fill("").map(() => (
      <div style={{ border: "1px solid black" }}>Content</div>
    ))}
  </Grid>
);

export const With12ColsAnd12Rows = (): React.ReactElement => (
  <Grid cols={12} rows={12}>
    {new Array(144).fill("").map(() => (
      <div style={{ border: "1px solid black" }}>Content</div>
    ))}
  </Grid>
);

export const WithBlogLayout = (): React.ReactElement => (
  <Grid rows="60px 1fr" style={{ height: "800px" }}>
    <div style={{ border: "1px solid black" }}>Nav</div>
    <Grid cols="0.5fr 1fr 0.5fr">
      <div style={{ border: "1px solid black" }}>List</div>
      <div style={{ border: "1px solid black" }}>Content</div>
      <div style={{ border: "1px solid black" }}>List</div>
    </Grid>
  </Grid>
);
