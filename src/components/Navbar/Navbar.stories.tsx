import React from "react";

import NavbarButton from "../NavbarButton";
import { NavbarDropdownContent } from "../NavbarButton/NavbarButton.styles";
import { PopoverButton } from "../Popover/Popover.styles";
import {
  NavbarHeader,
  NavbarLeftContent,
  NavbarRightContent
} from "./Navbar.styles";
import NavbarReadme from "./README.md";
import Navbar from ".";

export default {
  component: Navbar,
  parameters: {
    info: {
      inline: true,
      text: NavbarReadme
    }
  },
  title: "Navbar"
};

export const BasicUse = (): React.ReactElement => (
  <Navbar>
    <NavbarLeftContent>
      <NavbarHeader>Andrade UI</NavbarHeader>
      <NavbarButton>Home</NavbarButton>
      <NavbarButton active={true}>Content</NavbarButton>
      <NavbarButton>
        About
        <NavbarDropdownContent>
          <PopoverButton>Child Button</PopoverButton>
        </NavbarDropdownContent>
      </NavbarButton>
    </NavbarLeftContent>
    <NavbarRightContent>
      <NavbarButton>Log Out</NavbarButton>
    </NavbarRightContent>
  </Navbar>
);
