import styled, { css } from "styled-components";
import Button, { ButtonProps } from "../Button";
import { ButtonSize } from "../../types/enums";

const CircleButton = styled(Button)<ButtonProps>`
  border-radius: 100%;
  padding: 0.25rem;

  &:after {
    border-radius: 100%;
  }

  ${props =>
    props.size === ButtonSize.sm &&
    css`
      padding: 0;
    `};

  ${props =>
    props.size === ButtonSize.lg &&
    css`
      padding: 0.5rem;
    `};
`;

CircleButton.displayName = "CircleButton";

export default CircleButton;
