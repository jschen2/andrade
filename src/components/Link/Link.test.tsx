import { ReactWrapper, ShallowWrapper } from "enzyme";
import { mountComponent, shallowComponent } from "../../utils/testUtils";

import Link from ".";

describe("Link", () => {
  let props: any;
  let mountLink: ReactWrapper | undefined;
  let shallowLink: ShallowWrapper | undefined;

  const mountTestComponent = (): ReactWrapper => {
    if (!mountLink) {
      mountLink = mountComponent(Link, props);
    }

    return mountLink;
  };

  const shallowTestComponent = (): ShallowWrapper => {
    if (!shallowLink) {
      shallowLink = shallowComponent(Link, props);
    }

    return shallowLink;
  };

  beforeEach(() => {
    props = {
      href: "#"
    };

    mountLink = undefined;
    shallowLink = undefined;
  });

  it("should render correctly", () => {
    expect(shallowTestComponent()).toMatchSnapshot();
  });

  it("should mount correctly", () => {
    expect(mountTestComponent().find(Link)).toBeDefined();
  });
});
