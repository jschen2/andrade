import React from "react";

import StyledSwitch, { SwitchProps } from "./Switch.styles";

export const Switch = ({
  checked,
  disabled,
  onClick,
  ...props
}: SwitchProps): React.ReactElement => {
  const onChange = (e: React.ChangeEvent<HTMLInputElement>): void => {
    if (onClick) {
      onClick(e as any);
    }
  };

  return (
    <StyledSwitch
      checked={checked}
      disabled={disabled}
      onClick={!disabled ? onClick : undefined}
      {...props}
    >
      <input
        type="checkbox"
        checked={checked}
        disabled={disabled}
        onChange={onChange}
      />
    </StyledSwitch>
  );
};

export default Switch;
