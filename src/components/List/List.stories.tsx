import { boolean, object, text } from "@storybook/addon-knobs";
import React from "react";

import Avatar from "../Avatar";
import img from "../Section/example.jpg";

import ListItem from "./components/ListItem";
import ListReadme from "./README.md";
import List from ".";

const listItems = [
  {
    title: "List Item 1",
    content: "This is a standard list item."
  },
  {
    title: "List Item 2",
    content: "This is a second standard list item."
  }
];

const listItemsWithImages = [
  {
    title: "List Item 1",
    content: "This is a standard list item.",
    image: {
      children: "J"
    }
  },
  {
    title: "List Item 2",
    content: "This is a second standard list item.",
    image: {
      src: img
    }
  }
];

export default {
  component: List,
  info: {
    inline: true,
    text: ListReadme
  },
  title: "List"
};

export const BasicUse = (): React.ReactElement => (
  <List
    header={boolean("Header?", true)}
    title={text("Title", "List")}
    ordered={boolean("Ordered?", false)}
  >
    {object("List Items", listItems).map((item: any) => (
      <ListItem key={item.title} title={item.title}>
        {item.content}
      </ListItem>
    ))}
  </List>
);

export const withTextImage = (): React.ReactElement => (
  <List
    header={boolean("Header?", true)}
    title={text("Title", "List")}
    ordered={boolean("Ordered?", false)}
  >
    {object("List Items", listItemsWithImages).map((item: any) => (
      <ListItem key={item.title} title={item.title}>
        <Avatar {...item.image} />
        {item.content}
      </ListItem>
    ))}
  </List>
);
