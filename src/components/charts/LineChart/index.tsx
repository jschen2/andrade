import { max, min } from "d3-array";
import { line } from "d3-shape";
import { scaleLinear, ScaleLinear, ScaleTime, scaleTime } from "d3-scale";
import React from "react";
import { getItemWidth } from "../../../utils";
import SVGBase, {
  DataValue,
  defaultChartProps,
  DefaultChartProps
} from "../SVGBase";
import SVGGridlines from "../SVGGridlines";
import SVGXAxis from "../SVGXAxis";
import SVGYAxis from "../SVGYAxis";
import SVGLine from "./components/SVGLine";

export interface LineData {
  name: string;
  values: DataValue[];
}
export interface LineChartProps extends DefaultChartProps {
  data: LineData[];
}

const getContentWidth = ({
  width,
  marginLeft,
  marginRight
}: LineChartProps & { width: number }): number =>
  width - marginLeft! - marginRight!;

const getContentHeight = ({
  height,
  legend,
  marginBottom,
  marginTop
}: LineChartProps & { height: number }): number =>
  height - marginBottom! - marginTop! - (legend ? 24 : 0);

const getXScale = (
  props: LineChartProps & { width: number }
): ScaleTime<number, number> => {
  const { data } = props;
  const maxVal = max(data[0].values.map(d => d.date)) as number;
  const minVal = min(data[0].values.map(d => d.date)) as number;

  return scaleTime()
    .domain([minVal, maxVal])
    .range([0, getContentWidth(props)]);
};

const getYScale = (
  props: LineChartProps & { height: number }
): ScaleLinear<number, number> => {
  const { data } = props;
  const allValues: number[] = [];
  data.forEach(dLine => {
    dLine.values.forEach(d => allValues.push(d.value));
  });
  const maxVal = max(allValues) as number;
  const minVal = min(allValues) as number;

  return scaleLinear()
    .domain([minVal, maxVal])
    .range([getContentHeight(props), 0]);
};

export const LineChart = ({
  data,
  legend,
  marginLeft,
  marginTop,
  ...props
}: LineChartProps): React.ReactElement => {
  const lineChartRef = React.useRef<SVGElement>();
  const [showTooltip, setShowTooltip] = React.useState(false);
  const [tooltipData, setTooltipData] = React.useState<
    { name: string; value: number }[]
  >([]);
  const [lineLocation, setLineLocation] = React.useState(0);
  const [{ height, width }, setSVGDimensions] = React.useState({
    height: 0,
    width: 0
  });

  const getSVGWidth = (svgHeight: number, svgWidth: number): void => {
    setSVGDimensions({
      height: svgHeight,
      width: svgWidth
    });
  };

  const getLeftMarginWithNumbers = (): number => {
    let marginWidth = 0;
    const allValues: number[] = [];
    data.forEach(dLine => {
      dLine.values.forEach(d => allValues.push(d.value));
    });
    allValues.forEach(d => {
      const dWidth = getItemWidth(String(d));
      if (dWidth > marginWidth) {
        marginWidth = dWidth;
      }
    });

    return marginLeft! + marginWidth;
  };

  const mapProps = React.useMemo(
    (): LineChartProps & { height: number; width: number } => ({
      ...props,
      data,
      height,
      legend,
      marginLeft: getLeftMarginWithNumbers(),
      marginTop,
      width
    }),
    [data, height, width]
  );

  const xScale = getXScale(mapProps);
  const yScale = getYScale(mapProps);
  const path = React.useMemo(
    () =>
      line<DataValue>()
        .x(d => xScale(d.date) as number)
        .y(d => yScale(d.value) as number),
    [xScale, yScale]
  );

  const onMouseEnter = (): void => {
    setShowTooltip(true);
  };

  const onMouseLeave = (
    e: React.MouseEvent | React.FocusEvent | React.TouchEvent
  ): void => {
    const { relatedTarget } = e as React.MouseEvent;
    if (
      relatedTarget &&
      (relatedTarget === window ||
        !e.currentTarget.contains(relatedTarget as any))
    ) {
      setShowTooltip(false);
    }
  };

  const onMouseMove = (
    e: React.MouseEvent<SVGElement> | React.TouchEvent<SVGElement>
  ): void => {
    e.persist();
    const { clientX, target, touches } = e as any;
    const length = data[0].values.length - 1;
    const xLocation = clientX || touches[0].clientX;
    let index = Math.floor(
      (xLocation - (target as any).getBoundingClientRect().left) /
        (getContentWidth(mapProps) / length)
    );
    if (index > length) {
      index = length;
    }
    if (index !== -1) {
      const dataArray: { name: string; value: number }[] = [];
      data.forEach(d => {
        const value = d.values[index];
        dataArray.push({
          name: d.name,
          value: value.value
        });
      });
      setTooltipData(dataArray);
      setLineLocation(xScale(data[0].values[index].date) as number);
    }
  };

  const contentHeight = React.useMemo(() => getContentHeight(mapProps), [
    height
  ]);
  const contentWidth = React.useMemo(() => getContentWidth(mapProps), [width]);

  return (
    <SVGBase
      {...props}
      getSVGWidth={getSVGWidth}
      height={height}
      legend={legend}
      marginLeft={getLeftMarginWithNumbers()}
      marginTop={marginTop! + (legend ? 14 : 0)}
      names={data.map(d => d.name)}
      ref={lineChartRef}
      showTooltip={showTooltip}
      tooltipData={tooltipData}
      width={width}
    >
      {height && width && (
        <>
          <SVGGridlines
            height={contentHeight}
            width={contentWidth}
            xScale={xScale}
            yScale={yScale}
          />
          {data.map((d, index) => {
            const key = `line-${index}`;
            return (
              <SVGLine key={key} d={path(d.values) as string} index={index} />
            );
          })}
          {showTooltip && (
            <line
              x1={lineLocation}
              x2={lineLocation}
              y1={0}
              y2={contentHeight}
              stroke="#333333"
            />
          )}
          <SVGXAxis xScale={xScale} yAxisZero={contentHeight} />
          <SVGYAxis yScale={yScale} xAxisZero={0} />
          <rect
            fill="none"
            height={contentHeight}
            width={contentWidth}
            onMouseEnter={onMouseEnter}
            onMouseLeave={onMouseLeave}
            onMouseMove={onMouseMove}
            onTouchStart={onMouseEnter}
            onTouchEnd={onMouseLeave}
            onTouchMove={onMouseMove}
            style={{
              pointerEvents: "all"
            }}
          />
        </>
      )}
    </SVGBase>
  );
};

LineChart.defaultProps = {
  ...defaultChartProps
};

export default LineChart;
