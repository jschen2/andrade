import React from "react";
import styled, { css } from "styled-components";
import { borderRadius, getColor, getShadow, transition } from "../../utils";
import { Color } from "../../types/enums";
import Flex from "../Flex";
import { StyledIcon } from "../Icon";
import Popover from "../Popover";

export interface NavbarButtonProps
  extends React.HTMLAttributes<HTMLDivElement> {
  active?: boolean;
}

const StyledNavbarButton = styled.div<NavbarButtonProps>`
  align-items: center;
  background-color: transparent;
  ${borderRadius};
  cursor: pointer;
  display: inline-flex;
  justify-content: center;
  margin: 0 0.25rem;
  position: relative;
  ${transition("background-color box-shadow")};

  &:hover {
    background-color: ${getColor(Color.gray, 125)};
    ${getShadow(0)};
  }

  ${props =>
    props.active &&
    css`
      background-color: ${getColor(Color.gray, 150)};
      ${getShadow(0)};
      position: relative;

      &:before {
        background-color: ${getColor(Color.purple, 300)};
        border-radius: 0 0 0.25rem 0.25rem;
        bottom: 0;
        content: "";
        height: 0.25rem;
        left: 0;
        position: absolute;
        width: 100%;
      }
    `};

  ${StyledIcon} {
    margin-right: -0.25rem;
  }
`;

export const NavbarButtonPopover = styled(Popover)`
  padding: 0;
`;

export const NavbarDropdownContent = styled(Flex)`
  flex-direction: column;
  width: 100%;
`;

NavbarDropdownContent.displayName = "NavbarDropdownContent";

export default StyledNavbarButton;
