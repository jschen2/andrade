import { ReactWrapper, ShallowWrapper } from "enzyme";
import React from "react";
import { act } from "react-dom/test-utils";
import { mountWithTheme, shallowComponent } from "../../utils/testUtils";

import { ToggleButton } from "./Toggle.styles";
import Toggle, { ToggleProps } from ".";

describe("Toggle", () => {
  let props: ToggleProps;
  let mountToggle: ReactWrapper | undefined;
  let shallowToggle: ShallowWrapper | undefined;

  const mountTestComponent = (): ReactWrapper => {
    if (!mountToggle) {
      mountToggle = mountWithTheme(
        <Toggle {...props}>
          <ToggleButton value="Option">Option</ToggleButton>
        </Toggle>
      );
    }

    return mountToggle;
  };

  const shallowTestComponent = (): ShallowWrapper => {
    if (!shallowToggle) {
      shallowToggle = shallowComponent(Toggle, props);
    }

    return shallowToggle;
  };

  beforeEach(() => {
    props = {
      onClick: jest.fn()
    };

    Element.prototype.getBoundingClientRect = jest.fn(
      () =>
        ({
          width: 70
        } as any)
    );

    mountToggle = undefined;
    shallowToggle = undefined;
  });

  it("should render correctly", () => {
    expect(shallowTestComponent()).toMatchSnapshot();
  });

  it("should mount correctly", () => {
    expect(mountTestComponent().find(Toggle)).toBeDefined();
  });

  it("should fire onClick when selecting option", () => {
    act(() => {
      (mountTestComponent()
        .find(Toggle)
        .find(ToggleButton)
        .at(0)
        .props() as any).onClick();
    });
    expect(props.onClick).toHaveBeenCalled();
  });
});
