import React from "react";
import { ReactWrapper, ShallowWrapper } from "enzyme";
import { Color } from "../../types/enums";
import { mountWithTheme, shallowComponent } from "../../utils/testUtils";

import Progress, { ProgressLine, ProgressProps } from ".";

describe("Progress", () => {
  let props: ProgressProps;
  let mountProgress: ReactWrapper | undefined;
  let shallowProgress: ShallowWrapper | undefined;

  const mountTestComponent = (): ReactWrapper => {
    if (!mountProgress) {
      mountProgress = mountWithTheme(
        <Progress>
          <ProgressLine {...props} />
        </Progress>
      );
    }

    return mountProgress;
  };

  const shallowTestComponent = (): ShallowWrapper => {
    if (!shallowProgress) {
      shallowProgress = shallowComponent(Progress, props);
    }

    return shallowProgress;
  };

  beforeEach(() => {
    props = {
      color: Color.blue,
      percentage: 25
    };

    mountProgress = undefined;
    shallowProgress = undefined;
  });

  it("should render correctly", () => {
    expect(shallowTestComponent()).toMatchSnapshot();
  });

  it("should mount correctly", () => {
    expect(mountTestComponent().find(Progress)).toBeDefined();
  });

  describe("if 'progressLabel' is true", () => {
    beforeEach(() => {
      props.progressLabel = true;
    });

    it("should render correctly", () => {
      expect(shallowTestComponent()).toMatchSnapshot();
    });

    it("should mount correctly", () => {
      expect(mountTestComponent().find(ProgressLine)).toBeDefined();
    });
  });
});
