import { boolean, select } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";
import React from "react";

import { ButtonSize, Color } from "../../types/enums";
import { getEnumValues } from "../../utils";
import ButtonReadme from "./README.md";
import Button from ".";

export default {
  component: Button,
  parameters: {
    info: {
      inline: true,
      text: ButtonReadme
    }
  },
  title: "Button"
};

export const BasicUse = (): React.ReactElement => (
  <Button
    color={select("Colors", getEnumValues(Color), Color.purple) as Color}
    disabled={boolean("Disabled?", false)}
    size={
      select("Size", getEnumValues(ButtonSize), ButtonSize.md) as ButtonSize
    }
    ghost={boolean("Ghost?", false)}
    outline={boolean("Outline?", false)}
    onClick={() => action("Clicked")()}
  >
    Click me
  </Button>
);
