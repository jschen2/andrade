import { ReactWrapper, ShallowWrapper } from "enzyme";
import * as React from "react";
import { mountWithTheme, shallowWithTheme } from "../../utils/testUtils";

import Field, { FieldProps } from ".";

describe("Field", () => {
  let props: FieldProps;
  let mountField: ReactWrapper | undefined;
  let shallowField: ShallowWrapper | undefined;

  const mountTestComponent = (): ReactWrapper => {
    if (!mountField) {
      mountField = mountWithTheme(<Field {...props}>Help text</Field>);
    }

    return mountField;
  };

  const shallowTestComponent = (): ShallowWrapper => {
    if (!shallowField) {
      shallowField = shallowWithTheme(<Field {...props}>Help text</Field>);
    }

    return shallowField;
  };

  beforeEach(() => {
    props = {
      icon: {
        className: "fa-times"
      },
      value: undefined
    };

    mountField = undefined;
    shallowField = undefined;
  });

  it("should render correctly", () => {
    expect(shallowTestComponent()).toMatchSnapshot();
  });

  it("should mount correctly", () => {
    expect(mountTestComponent().find(Field)).toBeDefined();
  });
});
