import { boolean, select } from "@storybook/addon-knobs";
import React from "react";

import { getEnumValues } from "../../utils";
import { Alignment, Color, Position } from "../../types/enums";
import Button from "../Button";
import DrawerReadme from "./README.md";
import DrawerHeader from "./components/DrawerHeader";
import { DrawerBody, DrawerFooter } from "./Drawer.styles";
import Drawer from ".";

export default {
  component: Drawer,
  parameters: {
    info: {
      inline: true,
      text: DrawerReadme
    }
  },
  title: "Drawer"
};

export const BasicUse = (): React.ReactElement => {
  const [isOpen, setIsOpen] = React.useState(false);

  const openDrawer = (): void => {
    setIsOpen(true);
  };

  const closeDrawer = (): void => {
    setIsOpen(false);
  };

  return (
    <>
      <Button color={Color.purple} onClick={openDrawer}>
        Click me
      </Button>
      <Drawer
        dimBackground={boolean("Dim background?", true)}
        isOpen={isOpen}
        position={
          select(
            "Position",
            getEnumValues(Position),
            Position.right
          ) as Position
        }
        toggle={closeDrawer}
      >
        <DrawerHeader toggle={closeDrawer}>Drawer</DrawerHeader>
        <DrawerBody>Hello</DrawerBody>
        <DrawerFooter justifyContent={Alignment.flexEnd}>
          <Button outline={true} color={Color.blue}>
            Cancel
          </Button>
          <Button color={Color.green}>Save</Button>
        </DrawerFooter>
      </Drawer>
    </>
  );
};
