import styled, { css } from "styled-components";
import Flex, { FlexProps } from "../Flex";
import { getColor } from "../../utils";
import { Color } from "../../types/enums";

export interface BreadcrumbProps {
  active?: boolean;
}

export const Breadcrumb = styled.div<BreadcrumbProps>`
  align-items: center;
  border: 1px solid transparent;
  color: ${getColor(Color.gray, 800)};
  cursor: pointer;
  display: flex;
  justify-content: center;
  padding: 0.5rem 1rem;
  position: relative;

  &:before {
    content: "\f054";
    font-family: "Font Awesome 5 Pro";
    position: absolute;
    left: -0.25rem;
    top: 50%;
    transform: translateY(-50%);
  }

  &:hover {
    color: ${getColor(Color.purple, 200)};
  }

  ${props =>
    props.active &&
    css`
      color: ${getColor(Color.purple, 300)};
      font-weight: bold;
    `};
`;

const Breadcrumbs = styled(Flex)<FlexProps>`
  padding: 0.5rem;
`;

export default Breadcrumbs;
