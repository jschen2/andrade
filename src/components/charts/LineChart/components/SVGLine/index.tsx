import React from "react";
import styled from "styled-components";
import { Color } from "../../../../../types/enums";
import { getColor, transition } from "../../../../../utils";

export interface SVGLineProps extends React.HTMLAttributes<SVGPathElement> {
  index?: number;
}

const SVGLine = styled.path<SVGLineProps>`
  fill: none;
  shape-rendering: geometricPrecision;
  stroke: ${props =>
    getColor(Object.keys(Color)[props.index as number] as Color, 500)(props)};
  stroke-width: 2px;
  ${transition("all")};
`;

export default SVGLine;
