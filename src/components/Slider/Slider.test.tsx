import { ReactWrapper, ShallowWrapper } from "enzyme";
import { act } from "react-dom/test-utils";
import { mountComponent, shallowComponent } from "../../utils/testUtils";
import { Color } from "../../types/enums";
import { Handle, StyledSlider } from "./Slider.styles";
import Slider, { SliderProps } from ".";

describe("Slider", () => {
  let props: SliderProps;
  let mountSlider: ReactWrapper | undefined;
  let shallowSlider: ShallowWrapper | undefined;

  const mountTestComponent = (): ReactWrapper => {
    if (!mountSlider) {
      mountSlider = mountComponent(Slider, props);
    }

    return mountSlider;
  };

  const shallowTestComponent = (): ShallowWrapper => {
    if (!shallowSlider) {
      shallowSlider = shallowComponent(Slider, props);
    }

    return shallowSlider;
  };

  beforeEach(() => {
    props = {
      color: Color.blue,
      max: 100,
      min: 0,
      onChange: jest.fn(),
      value: [28]
    };

    Element.prototype.getBoundingClientRect = jest.fn(() => ({
      width: 120,
      height: 120,
      top: 0,
      left: 0,
      bottom: 0,
      right: 0
    })) as any;

    mountSlider = undefined;
    shallowSlider = undefined;
  });

  it("should render correctly", () => {
    expect(shallowTestComponent()).toMatchSnapshot();
  });

  it("should mount correctly", () => {
    act(() => {
      mountTestComponent()
        .find(Handle)
        .props()
        .onMouseDown({
          preventDefault: jest.fn()
        });
    });

    mountTestComponent().update();

    act(() => {
      (mountTestComponent().find(StyledSlider) as any).props().onClick({
        clientX: 20,
        getBoundingClientRect: () => ({
          left: 0,
          width: 100
        }),
        preventDefault: jest.fn(),
        stopPropagation: jest.fn()
      } as any);
    });

    mountTestComponent().update();

    expect(mountTestComponent().find(Slider)).toBeDefined();
  });

  it("should handle touch event", () => {
    act(() => {
      (mountTestComponent().find(StyledSlider) as any).props().onClick({
        touches: [
          {
            clientX: 80
          }
        ],
        getBoundingClientRect: () => ({
          left: 0,
          width: 100
        }),
        preventDefault: jest.fn(),
        stopPropagation: jest.fn()
      } as any);
    });
    mountTestComponent().update();

    expect(mountTestComponent().find(Slider)).toBeDefined();
  });

  describe("if 'range' is true", () => {
    beforeEach(() => {
      props.range = true;
    });

    it("should render correctly", () => {
      expect(shallowTestComponent()).toMatchSnapshot();
    });

    it("should mount correctly", () => {
      mountTestComponent().update();

      expect(
        mountTestComponent()
          .find(Slider)
          .find(Handle).length
      ).toBe(2);
    });
  });

  describe("if 'disabled' is true", () => {
    beforeEach(() => {
      props.range = true;
      props.disabled = true;
    });

    it("should render correctly", () => {
      expect(shallowTestComponent()).toMatchSnapshot();
    });

    it("should mount correctly", () => {
      mountTestComponent().update();

      expect(
        mountTestComponent()
          .find(Slider)
          .find(Handle).length
      ).toBe(2);
    });
  });
});
