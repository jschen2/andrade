import React from "react";
import debounce from "lodash/debounce";
import uniqueId from "lodash/uniqueId";
import { isNullOrUndefined } from "../../../utils";
import Container from "../../Container";
import ChartTooltip from "../ChartTooltip";

import useCombinedRefs from "../../../utils/useCombinedRefs";
import SVGLegend from "../SVGLegend";
import StyledSVGBase, {
  AxisLabel,
  ChartTooltipWrapper
} from "./SVGBase.styles";

export interface DataValue {
  date: number;
  value: number;
}

export interface DefaultChartProps extends React.HTMLAttributes<SVGElement> {
  legend?: boolean;
  marginBottom?: number;
  marginLeft?: number;
  marginRight?: number;
  marginTop?: number;
  xAxisLabel?: string;
  yAxisLabel?: string;
}

export const defaultChartProps: DefaultChartProps = {
  legend: true,
  marginBottom: 32,
  marginLeft: 8,
  marginRight: 8,
  marginTop: 8
};

export interface SVGBaseProps extends DefaultChartProps {
  height: number;
  getSVGWidth(height: number, width: number): void;
  names?: string[];
  showTooltip?: boolean;
  tooltipData?: { name: string; value: number }[];
  width: number;
}

export const SVGBase = React.forwardRef(
  (
    {
      children,
      getSVGWidth,
      height,
      legend,
      marginLeft,
      marginTop,
      names,
      showTooltip,
      tooltipData,
      width,
      xAxisLabel,
      yAxisLabel,
      ...props
    }: SVGBaseProps,
    ref: React.RefObject<any>
  ): React.ReactElement => {
    const svgRef: React.RefObject<any> = React.useRef();
    const combinedRef = useCombinedRefs(svgRef, ref);

    const resizeEvent = (): void => {
      if (!isNullOrUndefined(svgRef.current)) {
        const { clientHeight, clientWidth } = svgRef.current;
        getSVGWidth(
          clientHeight - (yAxisLabel ? 24 : 0),
          clientWidth - (xAxisLabel ? 24 : 0)
        );
      }
    };

    React.useEffect(() => {
      if (!isNullOrUndefined(svgRef.current)) {
        resizeEvent();
        window.addEventListener("resize", debounce(resizeEvent, 50));

        return function cleanup() {
          window.removeEventListener("resize", debounce(resizeEvent, 50));
        };
      }

      return undefined;
    }, [svgRef.current]);

    return (
      <Container
        ref={combinedRef}
        style={{ height: "100%", position: "relative" }}
      >
        <StyledSVGBase
          {...props}
          preserveAspectRatio="xMinYMin meet"
          viewBox={`0 0 ${width} ${height}`}
        >
          <g
            transform={`translate(${marginLeft! +
              (xAxisLabel ? 8 : 0)}, ${marginTop})`}
          >
            {children}
          </g>
          {yAxisLabel && (
            <AxisLabel
              style={{ transform: "translate(16px, 50%) rotateZ(-90deg)" }}
            >
              {yAxisLabel}
            </AxisLabel>
          )}
          {xAxisLabel && (
            <AxisLabel style={{ transform: `translate(50%, ${height + 8}px)` }}>
              {xAxisLabel}
            </AxisLabel>
          )}
          {legend && names && <SVGLegend names={names} width={width} />}
        </StyledSVGBase>
        {showTooltip && (
          <ChartTooltip isOpen={showTooltip} target={uniqueId("svg-tooltip-")}>
            {tooltipData &&
              tooltipData.map(data => (
                <ChartTooltipWrapper key={data.name}>
                  <span className="name">{data.name}</span>
                  <span className="value">{data.value}</span>
                </ChartTooltipWrapper>
              ))}
          </ChartTooltip>
        )}
      </Container>
    );
  }
);

export default SVGBase;
