import styled from "styled-components";
import { transition } from "../../utils";

export const StyledSidebarContent = styled.div<{ expanded?: boolean }>`
  box-sizing: border-box;
  height: 100%;
  margin-left: ${props => (props.expanded ? 224 : 46)}px;
  padding: 1rem;
  ${transition("margin-left width")};
  width: calc(100% - ${props => (props.expanded ? 224 : 46)}px + 1rem);
`;

export default StyledSidebarContent;
