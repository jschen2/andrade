import moment, { Moment } from "moment";
import React from "react";

import Field, { FieldProps } from "../Field";
import Popover from "../Popover";
import { Placement } from "../../types/enums";
import { isNullOrUndefined, onEnter } from "../../utils";

import CalendarControls from "./components/CalendarControls";
import Calendar from "./components/Calendar";

export interface DateFieldProps
  extends Omit<FieldProps, "value" | "onChange" | "onClick"> {
  onChange?(e: React.ChangeEvent<HTMLDivElement>, value: Moment): void;
  onClick?(e: React.MouseEvent<HTMLDivElement>, value: Moment): void;
  value?: Moment;
}

export const DateField = ({
  id,
  onChange: onCompChange,
  onClick,
  value,
  ...props
}: DateFieldProps): React.ReactElement => {
  const dateId = !isNullOrUndefined(id) ? id : "date-field";
  const containerRef: React.RefObject<HTMLDivElement> = React.useRef(null);
  const pickerRef: React.RefObject<HTMLDivElement> = React.useRef(null);
  const [fieldText, setFieldText] = React.useState(
    (value || moment(new Date())).format("MM/DD/YYYY")
  );
  const [date, setDate] = React.useState(value || moment(new Date()));
  const [isDatePickerOpen, setIsDatePickerOpen] = React.useState(false);

  const formatDate = (newDate?: Moment): string =>
    (newDate || date).format("MM/DD/YYYY");

  const onDateChange = (e: React.ChangeEvent<HTMLInputElement>): void => {
    const { value: targetValue } = e.target;
    if (!isNullOrUndefined(targetValue) && targetValue !== "") {
      const newDate: Moment = moment(
        targetValue.split("/").join(" "),
        "MM DD YYYY"
      );
      if (newDate.isValid()) {
        setDate(newDate);
        setFieldText(formatDate(newDate));
      }

      if (onCompChange) {
        onCompChange(e, newDate);
      }
    }
  };

  const onOutsideClick = (e: any): void => {
    if (
      !isNullOrUndefined(containerRef.current) &&
      !isNullOrUndefined(pickerRef.current) &&
      !containerRef.current.contains(e.target) &&
      !pickerRef.current.contains(e.target)
    ) {
      setIsDatePickerOpen(false);
      onDateChange(e);
    }
  };

  React.useEffect(() => {
    if (value) {
      setDate(value);
      setFieldText(formatDate());
    }
  }, [value]);

  React.useEffect(() => {
    window.addEventListener("click", onOutsideClick);

    return function cleanup() {
      window.removeEventListener("click", onOutsideClick);
    };
  }, [containerRef]);

  const onChange = (e: React.ChangeEvent<HTMLInputElement>): void => {
    const { value: targetValue } = e.target;
    setFieldText(targetValue);
  };

  const onFocus = (): void => {
    setIsDatePickerOpen(true);
  };

  const modifyDate = (prop: string, type: string) => () => {
    if (!isNullOrUndefined(date)) {
      const newDate = (moment(date) as any)[prop](1, type);
      setDate(newDate);
      setFieldText(formatDate(newDate));
    }
  };

  const selectDay = (idx: number) => (
    e: React.MouseEvent<HTMLDivElement>
  ): void => {
    let positions;
    const day = Number(date?.format("D"));
    let newDate;
    if (day < idx) {
      positions = idx - day;
      newDate = moment(date).add(positions, "d");
    } else {
      positions = day - idx;
      newDate = moment(date).subtract(positions, "d");
    }
    setDate(newDate);
    setFieldText(formatDate(newDate));
    setIsDatePickerOpen(false);

    if (onClick) {
      onClick(e, newDate);
    }
  };

  return (
    <div ref={containerRef}>
      <Field
        {...props}
        id={dateId}
        onFocus={onFocus}
        onChange={onChange}
        onKeyDown={onEnter(onDateChange)}
        value={fieldText}
      />
      <Popover
        placement={Placement.bottomStart}
        isOpen={isDatePickerOpen}
        target={dateId}
        ref={pickerRef}
        showArrow={false}
      >
        <CalendarControls
          add={modifyDate("add", "y")}
          subtract={modifyDate("subtract", "y")}
        >
          <div>{date.year()}</div>
        </CalendarControls>
        <CalendarControls
          add={modifyDate("add", "M")}
          subtract={modifyDate("subtract", "M")}
        >
          <div>{moment(String(date.month() + 1), "M").format("MMMM")}</div>
        </CalendarControls>
        <Calendar date={date} selectDay={selectDay} />
      </Popover>
    </div>
  );
};

export default DateField;
