import debounce from "lodash/debounce";
import React from "react";

import { Color } from "../../types/enums";
import {
  getDocument,
  isNullOrUndefined,
  percentToValue,
  roundValueToStep
} from "../../utils";
import {
  Handle,
  Rail,
  SliderPopover,
  Step,
  StyledSlider
} from "./Slider.styles";

export interface SliderProps
  extends Omit<React.HTMLAttributes<HTMLElement>, "onChange"> {
  color: Color;
  disabled?: boolean;
  max: number;
  min: number;
  onChange?(e: React.MouseEvent, value: number[]): void;
  range?: boolean;
  step?: number;
  value: number[];
}

export const Slider = ({
  color,
  disabled,
  max,
  min,
  onChange,
  range,
  step,
  value: propVal,
  ...props
}: SliderProps): React.ReactElement => {
  const sliderRef = React.useRef<HTMLDivElement>();
  const [active, setActive] = React.useState(-1);
  const [minVal, setMinVal] = React.useState(range ? propVal[0] : min);
  const [maxVal, setMaxVal] = React.useState(
    range && propVal[1] ? propVal[1] : propVal[0] || max
  );

  const setPositionValue = (
    e: MouseEvent | React.MouseEvent | React.TouchEvent | TouchEvent
  ): void => {
    e.preventDefault();
    e.stopPropagation();
    if (!isNullOrUndefined(sliderRef.current)) {
      const {
        left,
        width
      } = (sliderRef.current as any).getBoundingClientRect();
      const idx = (sliderRef.current as any).getAttribute("data-index");
      let { clientX } = e as MouseEvent;
      if (!clientX) {
        clientX = (e as TouchEvent).touches[0].clientX;
      }
      const position = clientX - left;
      if (position >= 0 && position <= width) {
        let newVal = Math.round(percentToValue(position / width, min, max));
        if (step) {
          newVal = roundValueToStep(newVal, step, min);
        }
        if (idx === "0" && newVal <= maxVal) {
          setMinVal(newVal);
          if (onChange) {
            onChange(e as any, [newVal, maxVal]);
          }
        } else if (idx === "1" && newVal >= minVal) {
          setMaxVal(newVal);
          if (onChange) {
            onChange(e as any, range ? [minVal, newVal] : [newVal]);
          }
        }
      }
    }
  };

  const onMouseUp = (): void => {
    setActive(-1);
    const doc = getDocument(sliderRef.current);
    doc.removeEventListener("mousemove", debounce(setPositionValue, 5));
    doc.removeEventListener("touchmove", debounce(setPositionValue, 5));
    doc.removeEventListener("mouseup", onMouseUp);
    doc.removeEventListener("touchend", onMouseUp);
  };

  const onMouseDown = (num: number) => (
    e: React.MouseEvent | React.TouchEvent
  ): void => {
    e.preventDefault();
    setActive(num);
    const doc = getDocument(sliderRef.current);
    doc.addEventListener("mousemove", debounce(setPositionValue, 5));
    doc.addEventListener("touchmove", debounce(setPositionValue, 5));
    doc.addEventListener("mouseup", onMouseUp);
    doc.addEventListener("touchend", onMouseUp);
  };

  return (
    <StyledSlider
      {...props}
      disabled={disabled}
      ref={sliderRef as any}
      onClick={setPositionValue}
      data-index={active}
    >
      <Rail
        color={color}
        left={minVal}
        value={((maxVal - minVal - min) / (max - min)) * 100}
      />
      {step &&
        new Array(Math.round(max / step) + 1)
          .fill(undefined)
          .map((val, idx) => {
            const key = `step-${idx}`;
            const value = step * idx;
            return (
              <Step
                color={color}
                key={key}
                left={value}
                onRail={value > minVal && value < maxVal}
              />
            );
          })}
      {range && (
        <>
          <input
            type="hidden"
            name="slider-min-range"
            value={minVal}
            readOnly={true}
          />
          <Handle
            active={active === 0}
            color={color}
            disabled={disabled}
            id="slider-handle-min-val"
            onMouseDown={!disabled ? onMouseDown(0) : undefined}
            onTouchStart={!disabled ? onMouseDown(0) : undefined}
            style={{ left: `calc(${minVal}% - 0.25rem)` }}
          >
            <SliderPopover
              color={color}
              isOpen={active === 0}
              target="slider-handle-min-val"
            >
              {minVal}
            </SliderPopover>
          </Handle>
        </>
      )}
      <input
        type="hidden"
        name="slider-max-range"
        value={maxVal}
        readOnly={true}
      />
      <Handle
        active={active === 1}
        color={color}
        disabled={disabled}
        id="slider-handle-max-val"
        onMouseDown={!disabled ? onMouseDown(1) : undefined}
        onTouchStart={!disabled ? onMouseDown(1) : undefined}
        style={{ left: `calc(${maxVal}% - 0.25rem)` }}
      >
        <SliderPopover
          color={color}
          isOpen={active === 1}
          target="slider-handle-max-val"
        >
          {maxVal}
        </SliderPopover>
      </Handle>
    </StyledSlider>
  );
};

Slider.defaultProps = {
  color: Color.purple,
  max: 100,
  min: 0
};

export default Slider;
