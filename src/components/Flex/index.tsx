import styled from "styled-components";
import { Alignment } from "../../types/enums";
import Container, { ContainerProps } from "../Container";

export interface FlexProps extends ContainerProps {
  /* vertical alignment of children */
  alignItems?: Alignment;
  /* horizontal alignment children */
  justifyContent?: Alignment;
}

export const Flex = styled(Container)<FlexProps>`
  align-items: ${props => props.alignItems};
  justify-content: ${props => props.justifyContent};
  display: flex;
`;

Flex.displayName = "Flex";

export default Flex;
