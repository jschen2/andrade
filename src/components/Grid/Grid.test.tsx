import { ReactWrapper, ShallowWrapper } from "enzyme";
import { mountComponent, shallowComponent } from "../../utils/testUtils";

import Grid, { GridProps } from ".";

describe("Grid", () => {
  let props: GridProps;
  let mountGrid: ReactWrapper | undefined;
  let shallowGrid: ShallowWrapper | undefined;

  const mountTestComponent = (): ReactWrapper => {
    if (!mountGrid) {
      mountGrid = mountComponent(Grid, props);
    }

    return mountGrid;
  };

  const shallowTestComponent = (): ShallowWrapper => {
    if (!shallowGrid) {
      shallowGrid = shallowComponent(Grid, props);
    }

    return shallowGrid;
  };

  beforeEach(() => {
    props = {
      cols: undefined,
      gap: "1em",
      rows: undefined
    };

    mountGrid = undefined;
    shallowGrid = undefined;
  });

  it("should render correctly", () => {
    expect(shallowTestComponent()).toMatchSnapshot();
  });

  it("should mount correctly", () => {
    expect(mountTestComponent().find(Grid)).toBeDefined();
  });

  describe("if 'cols' and 'rows' are strings", () => {
    beforeEach(() => {
      props.cols = "1fr";
      props.rows = "1fr";
    });

    it("should mount correctly", () => {
      expect(mountTestComponent().find(Grid)).toBeDefined();
    });
  });
});
