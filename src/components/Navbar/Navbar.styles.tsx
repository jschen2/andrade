import styled from "styled-components";
import { getColor, getShadow } from "../../utils";
import { Color } from "../../types/enums";
import Flex from "../Flex";

export const NavbarHeader = styled.div`
  align-items: center;
  display: flex;
  font-weight: bold;
  justify-content: center;
  max-width: 224px;
  overflow: hidden;
  white-space: nowrap;
`;

export const NavbarLeftContent = styled(Flex)`
  justify-content: flex-start;
`;

export const NavbarRightContent = styled(Flex)`
  justify-content: flex-end;
`;

const StyledNavbar = styled(Flex)`
  background-color: ${getColor(Color.gray, 0)};
  ${getShadow(1)};
  height: 54px;
  left: 0;
  padding: 0.5rem;
  position: fixed;
  top: 0;
  width: 100%;
  z-index: 1031;
`;

export default StyledNavbar;
