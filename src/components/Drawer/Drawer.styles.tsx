import React from "react";
import styled, { css } from "styled-components";
import Flex from "../Flex";
import Button from "../Button";
import { borderRadius, getColor, getShadow, transition } from "../../utils";
import { Color, Position } from "../../types/enums";

export interface DrawerProps extends React.HTMLAttributes<HTMLDivElement> {
  dimBackground?: boolean;
  isOpen: boolean;
  position?: Position;
  toggle(): void;
}

export const DrawerBody = styled.div`
  display: flex;
  flex: 1 1 auto;
`;

export const DrawerFooter = styled(Flex)`
  padding: 0.5rem;

  ${Button} {
    margin: 0.25rem;
  }
`;

export const StyledDrawer = styled.div<Omit<DrawerProps, "toggle">>`
  background-color: ${getColor(Color.gray, 0)};
  ${borderRadius};
  display: flex;
  flex-direction: column;
  ${getShadow(2)};
  height: 100%;
  max-width: 100%;
  position: fixed;
  ${transition("transform")};
  width: 375px;
  z-index: 2;

  ${props =>
    props.position &&
    (() => {
      switch (String(props.position)) {
        case "bottom":
          return css`
            bottom: 0;
            box-shadow: -1px -2px 4px rgba(51, 51, 51, 0.35);
            left: 0;
            transform: translateY(${!props.isOpen ? "101%" : "0"});
            height: 375px;
            width: 100%;
          `;
        case "top":
          return css`
            top: 0;
            left: 0;
            height: 375px;
            transform: translateX(${!props.isOpen ? "101%" : "0"});
            width: 100%;
          `;
        case "left":
          return css`
            left: 0;
            top: 0;
            transform: translateY(${!props.isOpen ? "101%" : "0"});
          `;
        case "right":
        default:
          return css`
            box-shadow: -1px -2px 4px rgba(51, 51, 51, 0.35);
            right: 0;
            top: 0;
            transform: translateX(${!props.isOpen ? "101%" : "0"});
          `;
      }
    })()};
`;
