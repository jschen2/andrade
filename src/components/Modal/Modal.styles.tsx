import styled, { css } from "styled-components";
import { borderRadius, getColor, getShadow, hexToRgb } from "../../utils";
import { Color } from "../../types/enums";
import Flex from "../Flex";
import Container from "../Container";
import Button from "../Button";

const animateIn = css`
  animation: animateIn 332ms ease forwards;
  opacity: 0;

  @keyframes animateIn {
    from {
      opacity: 0;
    }
    to {
      opacity: 1;
    }
  }
`;

export const ModalOverlay = styled(Container)`
  background-color: rgba(
    ${props => hexToRgb(getColor(Color.gray, 900)(props))},
    0.32
  );
  height: 100vh;
  left: 0;
  position: fixed;
  top: 0;
  width: 100vw;
  z-index: 1;
  ${animateIn};
`;

export const ModalFooter = styled(Flex)`
  flex: 0 0 38px;
  padding: 0.5rem;

  ${Button} {
    margin: 0 0.25rem;
  }
`;

export const ModalBody = styled.div`
  display: flex;
  flex: 1 1 auto;
  height: 100%;
  width: 100%;
`;

export const StyledModal = styled(Flex)`
  background-color: ${getColor(Color.gray, 0)};
  ${borderRadius};
  flex-direction: column;
  ${getShadow(2)};
  height: max-content;
  left: 50%;
  max-height: 90vh;
  min-height: 100px;
  max-width: 90vw;
  min-width: 375px;
  padding: 0.5rem;
  position: fixed;
  top: 50%;
  transform: translate(-50%, -50%);
  width: max-content;
  z-index: 1;
  ${animateIn};
`;

export default StyledModal;
