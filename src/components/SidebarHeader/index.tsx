import React from "react";
import { SidebarContext } from "../SidebarContext";
import StyledSidebarHeader from "./SidebarHeader.styles";

export type SidebarHeaderProps = React.HTMLAttributes<HTMLDivElement>;

const SidebarHeader = ({
  children,
  ...props
}: SidebarHeaderProps): React.ReactElement => {
  const { expanded } = React.useContext(SidebarContext);
  const [expandedChild, collapsedChild] = React.Children.toArray(children);
  return (
    <StyledSidebarHeader {...props}>
      {expanded && expandedChild}
      {!expanded && collapsedChild}
    </StyledSidebarHeader>
  );
};

export default SidebarHeader;
