import React from "react";
import {
  FieldContainer,
  FieldGroup,
  FieldHelpText,
  FieldLabel
} from "../Field/Field.styles";

import StyledTextArea, { TextAreaProps } from "./TextArea.styles";

export const TextArea = ({
  children,
  defaultValue,
  disabled,
  id,
  innerRef,
  label,
  value,
  ...props
}: TextAreaProps): React.ReactElement => (
  <FieldGroup disabled={disabled} label={label} value={value || defaultValue}>
    <FieldContainer>
      <StyledTextArea
        id={id}
        ref={innerRef}
        value={value}
        defaultValue={defaultValue}
        disabled={disabled}
        rows={1}
        {...props}
      />
      {label && (
        <FieldLabel disabled={disabled} htmlFor={id}>
          {label}
        </FieldLabel>
      )}
    </FieldContainer>
    {children && <FieldHelpText>{children}</FieldHelpText>}
  </FieldGroup>
);

export default TextArea;
