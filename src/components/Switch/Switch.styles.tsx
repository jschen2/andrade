import React from "react";
import styled, { css } from "styled-components";
import { getColor, getShadow, transition } from "../../utils";
import { Color } from "../../types/enums";

export interface SwitchProps extends React.HTMLAttributes<HTMLInputElement> {
  checked: boolean;
  disabled?: boolean;
}

const StyledSwitch = styled.div<SwitchProps>`
  background-color: ${getColor(Color.gray, 200)};
  border-radius: 1.75rem;
  ${getShadow(0)};
  height: 1.75rem;
  position: relative;
  ${transition("background-color")};
  width: 3rem;

  & > input {
    display: none;
    left: -100%;
    position: absolute;
    top: -100%;
  }

  &:after {
    background-color: ${getColor(Color.gray, 100)};
    border-radius: 100%;
    content: "";
    height: 1.25rem;
    left: 0.25rem;
    ${getShadow(1)};
    position: absolute;
    top: 0.25rem;
    ${transition("transform")};
    width: 1.25rem;
  }

  ${props =>
    props.checked &&
    css`
      background-color: ${getColor(Color.green, 300)};

      &:after {
        transform: translateX(100%);
      }
    `}

  ${props =>
    props.disabled &&
    css`
      cursor: not-allowed;
      opacity: 0.5;
    `};
`;

export default StyledSwitch;
