import { action } from "@storybook/addon-actions";
import { boolean } from "@storybook/addon-knobs";
import moment from "moment";
import React from "react";

import DateReadme from "./README.md";
import DateField from ".";

export default {
  component: DateField,
  parameters: {
    info: {
      inline: true,
      text: DateReadme
    }
  },
  title: "DateField"
};

export const BasicUse = (): React.ReactElement => (
  <DateField
    disabled={boolean("Disabled?", false)}
    label="Date"
    value={moment()}
    onChange={action("Change")}
  />
);
