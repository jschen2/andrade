import styled from "styled-components";
import Container, { ContainerProps } from "../../../Container";
import { focusOutline, getColor } from "../../../../utils";
import { Color } from "../../../../types/enums";
import { StyledIcon } from "../../../Icon";

const CalendarControl = styled(Container)<ContainerProps>`
  display: flex;
  justify-content: space-between;
  width: 224px;

  & > div {
    line-height: 1;
  }

  & > ${Container} {
    width: auto;

    ${focusOutline(Color.blue)};

    ${StyledIcon} {
      color: ${getColor(Color.blue, 500)};
    }
  }
`;

export default CalendarControl;
