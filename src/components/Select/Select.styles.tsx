import styled, { css } from "styled-components";
import React from "react";

import Input from "../Input";
import Icon, { IconProps, StyledIcon } from "../Icon";
import {
  borderRadius,
  combineClasses,
  getColor,
  hexToRgb,
  transition
} from "../../utils";
import { Color } from "../../types/enums";
import Container from "../Container";

export interface SelectLabelProps extends React.HTMLAttributes<HTMLElement> {
  removeOption(): void;
}

type FieldIconProps = IconProps & { iconTop: number };

export const FieldIcon = styled(({ iconTop, ...props }) => <Icon {...props} />)<
  FieldIconProps
>`
  height: calc(100% - 6px);
  position: absolute;
  right: 0.5rem;
  top: ${props => props.iconTop + 3}px;
  
  &:before {
    ${transition("text-shadow transform")};
  }

  &:hover {
    &:before {
      text-shadow: 0px 0px 3px
          rgba(${props => hexToRgb(getColor(Color.gray, 900)(props))}, 0.5)
  }
`;

export const MenuIcon = styled(({ className, ...props }) => (
  <FieldIcon
    className={combineClasses(className, "fa-chevron-down")}
    {...props}
  />
))<FieldIconProps>`
  &:after {
    background-color: ${getColor(Color.gray, 200)};
    content: "";
    height: 80%;
    left: -2px;
    position: absolute;
    top: 10%;
    width: 2px;
  }
`;

export const CloseIcon = styled(({ className, ...props }) => (
  <FieldIcon className={combineClasses(className, "fa-times")} {...props} />
))<FieldIconProps>`
  right: 2.5rem;

  &:hover {
    color: ${getColor(Color.blue, 500)};
  }
`;

export const StyledSelectLabel = styled.span<
  Omit<SelectLabelProps, "removeOption">
>`
  align-items: center;
  ${borderRadius};
  background-color: rgba(
    ${props => hexToRgb(getColor(Color.purple, 400)(props))},
    0.75
  );
  border: 2px solid ${getColor(Color.purple, 500)};
  color: ${getColor(Color.gray, 100)};
  display: inline-flex;
  font-size: 0.85rem;
  font-weight: bold;
  justify-content: center;
  line-height: 1.1;
  margin: 0.25rem 0;
  padding: 0.125rem 0.25rem;

  ${Container} {
    color: inherit;
  }

  ${StyledIcon} {
    align-items: center;
    ${borderRadius};
    display: flex;
    height: 100%;
    justify-content: center;
    overflow: hidden;
    ${transition("margin padding")};
    margin: 0;
    padding: 0;
    width: 0;

    &:hover {
      background-color: ${getColor(Color.purple, 600)};
    }
  }

  &:hover {
    ${StyledIcon} {
      padding: 0.5rem 0.75rem;
      margin: -0.5rem 0 -0.5rem 0.25rem;
    }
  }

  &:not(:last-child) {
    margin-right: 0.5em;
  }
`;

export const StyledSelect = styled(Container)<{
  disabled?: boolean;
  isMenuOpen?: boolean;
}>`
  ${Input} {
    padding-right: 2em;
  }

  ${props =>
    props.isMenuOpen &&
    css`
      ${MenuIcon} {
        &:before {
          transform: rotate(180deg);
        }
      }
    `};

  ${props =>
    props.disabled &&
    css`
      ${MenuIcon} {
        cursor: not-allowed;
        opacity: 0.5;
      }
    `};
`;
