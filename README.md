# Andrade UI

This is a React-based, TypeScript-as-a-first-class-citizen UI library that has an emphasis in minimizing external
dependencies on those that are absolutely necessary. Styled-components have been used to allow for easy theming
of the components.

[![andrade](https://img.shields.io/npm/v/andrade/latest?style=for-the-badge)](https://www.npmjs.com/package/andrade)
<a href="https://gitlab.com/jschen2/andrade"><img src="https://about.gitlab.com/images/press/logo/svg/gitlab-logo-gray-rgb.svg" height="28" /></a>
<a href="https://jschen2.gitlab.io/andrade/"><img src="https://raw.githubusercontent.com/storybookjs/brand/master/badge/badge-storybook.svg?sanitize=true" height="28" /></a>
[![codecov](https://img.shields.io/codecov/c/gl/jschen2/andrade?style=for-the-badge)](https://codecov.io/gl/jschen2/andrade)

## Installation

```
// with npm
npm install andrade

// with yarn
yarn install andrade
```

## Usage

Wrapping your application with the `AndradeTheme` component is absolutely necessary for passing the theme file down to
the rest of the components. Using the `theme` prop allows you to replace the theme variables with your own.

```
import { AndradeTheme } from "andrade";

import App from "App";

ReactDOM.render(
    <AndradeTheme>
        <App />
    </AndradeTheme>,
    document.querySelector('#root')
);
```
