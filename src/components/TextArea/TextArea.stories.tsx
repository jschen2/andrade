import { action } from "@storybook/addon-actions";
import { boolean, text } from "@storybook/addon-knobs";
import React from "react";

import TextAreaReadme from "./README.md";
import TextArea from ".";

export default {
  component: TextArea,
  parameters: {
    info: {
      inline: true,
      text: TextAreaReadme
    }
  },
  title: "TextArea"
};

export const BasicUse = (): React.ReactElement => {
  const [value, setValue] = React.useState("");
  const onTextAreaChange = (
    e: React.ChangeEvent<HTMLTextAreaElement>
  ): void => {
    setValue(e.target.value);
    action("Value")(e.target.value, e);
  };
  return (
    <TextArea
      disabled={boolean("Disabled?", false)}
      label={text("Label", "Name")}
      id={text("ID", "Name")}
      invalid={boolean("Invalid?", false)}
      value={value}
      onChange={onTextAreaChange}
    >
      {text("Help text", "Add text here to help the user")}
    </TextArea>
  );
};
