import { ReactWrapper, ShallowWrapper } from "enzyme";
import { mountComponent, shallowComponent } from "../../utils/testUtils";

import { SwitchProps } from "./Switch.styles";

import Switch from ".";

describe("Switch", () => {
  let props: SwitchProps;
  let mountSwitch: ReactWrapper | undefined;
  let shallowSwitch: ShallowWrapper | undefined;

  const mountTestComponent = (): ReactWrapper => {
    if (!mountSwitch) {
      mountSwitch = mountComponent(Switch, props);
    }

    return mountSwitch;
  };

  const shallowTestComponent = (): ShallowWrapper => {
    if (!shallowSwitch) {
      shallowSwitch = shallowComponent(Switch, props);
    }

    return shallowSwitch;
  };

  beforeEach(() => {
    props = {
      checked: false,
      onClick: undefined
    };

    mountSwitch = undefined;
    shallowSwitch = undefined;
  });

  it("should render correctly", () => {
    expect(shallowTestComponent()).toMatchSnapshot();
  });

  it("should mount correctly", () => {
    expect(mountTestComponent().find(Switch)).toBeDefined();
  });

  it("should not fire 'onClick'", () => {
    mountTestComponent()
      .find("input")
      .simulate("change");

    expect(jest.fn()).not.toHaveBeenCalled();
  });

  describe("if 'onClick' is defined", () => {
    beforeEach(() => {
      props.checked = true;
      props.onClick = jest.fn();
    });

    it("should fire 'onClick'", () => {
      mountTestComponent()
        .find("input")
        .simulate("change");
      expect(props.onClick).toHaveBeenCalled();
    });
  });
});
