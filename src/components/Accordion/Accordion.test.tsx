import { ReactWrapper, ShallowWrapper } from "enzyme";
import { mountComponent, shallowComponent } from "../../utils/testUtils";

import { AccordionContent, AccordionProps } from "./Accordion.styles";
import Accordion from ".";

describe("Accordion", () => {
  let props: AccordionProps;
  let mountAccordion: ReactWrapper | undefined;
  let shallowAccordion: ShallowWrapper | undefined;

  const mountTestComponent = (): ReactWrapper => {
    if (!mountAccordion) {
      mountAccordion = mountComponent(Accordion, props);
    }

    return mountAccordion;
  };

  const shallowTestComponent = (): ShallowWrapper => {
    if (!shallowAccordion) {
      shallowAccordion = shallowComponent(Accordion, props);
    }

    return shallowAccordion;
  };

  beforeEach(() => {
    props = {
      title: "Accordion",
      children: "AccordionContent",
      isOpen: false
    };

    mountAccordion = undefined;
    shallowAccordion = undefined;
  });

  it("should render correctly", () => {
    expect(shallowTestComponent()).toMatchSnapshot();
  });

  it("should mount correctly", () => {
    expect(mountTestComponent().find(Accordion)).toBeDefined();
  });

  describe("if 'isOpen' is true", () => {
    beforeEach(() => {
      props.isOpen = true;
    });

    it("should render correctly", () => {
      expect(shallowTestComponent()).toMatchSnapshot();
    });

    it("should mount 'AccordionContent' correctly", () => {
      expect(mountTestComponent().find(AccordionContent)).toBeDefined();
    });
  });
});
