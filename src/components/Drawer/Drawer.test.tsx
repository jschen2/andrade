import { ReactWrapper, ShallowWrapper } from "enzyme";
import * as React from "react";

import { Position } from "../../types/enums";
import { getEnumValues } from "../../utils";
import { mountWithTheme, shallowWithTheme } from "../../utils/testUtils";

import { DrawerProps } from "./Drawer.styles";
import Drawer from ".";

describe("Drawer", () => {
  let props: DrawerProps;
  let mountDrawer: ReactWrapper | undefined;
  let shallowDrawer: ShallowWrapper | undefined;

  const mountTestComponent = (position: Position): ReactWrapper => {
    if (!mountDrawer) {
      mountDrawer = mountWithTheme(<Drawer position={position} {...props} />);
    }

    return mountDrawer;
  };

  const shallowTestComponent = (position: Position): ShallowWrapper => {
    if (!shallowDrawer) {
      shallowDrawer = shallowWithTheme(
        <Drawer position={position} {...props} />
      );
    }

    return shallowDrawer;
  };

  beforeEach(() => {
    props = {
      isOpen: true,
      toggle: jest.fn()
    };

    mountDrawer = undefined;
    shallowDrawer = undefined;
  });

  test.each(getEnumValues(Position))(
    "if position is equal to %p, it should render correctly",
    firstArg => {
      expect(shallowTestComponent(firstArg as Position)).toMatchSnapshot();
    }
  );

  test.each(getEnumValues(Position))(
    "if position is equal to %p, it should mount correctly",
    firstArg => {
      expect(
        mountTestComponent(firstArg as Position).find(Drawer)
      ).toBeDefined();
    }
  );

  describe("if 'isOpen' is false", () => {
    beforeEach(() => {
      props.isOpen = false;
    });

    test.each(getEnumValues(Position))(
      "if position is equal to %p, it should render correctly",
      firstArg => {
        expect(shallowTestComponent(firstArg as Position)).toMatchSnapshot();
      }
    );

    test.each(getEnumValues(Position))(
      "if position is equal to %p, it should mount correctly",
      firstArg => {
        expect(
          mountTestComponent(firstArg as Position).find(Drawer)
        ).toBeDefined();
      }
    );
  });
});
