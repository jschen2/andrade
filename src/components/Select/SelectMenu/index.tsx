import React from "react";
import styled, { css } from "styled-components";
import {
  borderRadiusNumber,
  getColor,
  hexToRgb,
  isNullOrUndefined
} from "../../../utils";
import { Color } from "../../../types/enums";
import {
  StyledPopover,
  StyledPopoverProps
} from "../../Popover/Popover.styles";
import Portal from "../../Portal";

export interface OptionProps {
  selected?: boolean;
}

export interface Option {
  label: string;
  value: string;
}

export const MenuOption = styled.div<OptionProps>`
  border: 1px solid transparent;
  width: 100%;

  &:first-child {
    border-radius: ${borderRadiusNumber}rem ${borderRadiusNumber}rem 0 0;
  }

  &:last-child {
    border-radius: 0 0 ${borderRadiusNumber}rem ${borderRadiusNumber}rem;
  }

  ${props =>
    props.selected &&
    css`
      border: 1px solid ${getColor(Color.purple, 100)};
      background-color: rgba(
        ${hexToRgb(getColor(Color.purple, 100)(props))},
        0.5
      );
    `};

  &:hover {
    border: 1px solid ${getColor(Color.gray, 200)};
    background-color: rgba(
      ${props => hexToRgb(getColor(Color.gray, 200)(props))},
      0.5
    );
  }
`;

export interface SelectMenuProps
  extends Omit<StyledPopoverProps, "left" | "top"> {
  node?: HTMLElement;
  options: Option[];
  selectOption(option: Option): (e: React.MouseEvent) => void;
  selectedOption?: Option;
}

const SelectMenu = ({
  node,
  options,
  selectOption,
  selectedOption,
  ...props
}: SelectMenuProps): React.ReactElement => {
  const [{ menuLeft, menuTop, menuWidth }, setMenuDimensions] = React.useState({
    menuLeft: 0,
    menuTop: 0,
    menuWidth: 0
  });

  const updateDimensions = (): void => {
    if (!isNullOrUndefined(node)) {
      const {
        height: menuHeight,
        left,
        top,
        width
      } = node.getBoundingClientRect();
      setMenuDimensions({
        menuLeft: left,
        menuTop: menuHeight + top,
        menuWidth: width
      });
    }
  };

  React.useEffect(() => {
    if (!isNullOrUndefined(node)) {
      updateDimensions();

      window.addEventListener("scroll", updateDimensions);

      return function cleanup() {
        window.removeEventListener("scroll", updateDimensions);
      };
    }

    return undefined;
  }, [node]);
  return (
    <Portal node={node}>
      <StyledPopover
        left={`calc(${menuLeft}px + 0.5rem)`}
        top={`${menuTop}px`}
        width={`calc(${menuWidth}px - 1rem)`}
        showArrow={false}
        style={{ position: "fixed", zIndex: 1 }}
        {...props}
      >
        {options.length > 0 &&
          options.map((option, idx) => {
            const key = `${option.label}-${idx}`;
            return (
              <MenuOption
                key={key}
                onClick={selectOption(option)}
                selected={
                  !isNullOrUndefined(selectedOption) &&
                  option.value === selectedOption.value
                }
              >
                {option.label}
              </MenuOption>
            );
          })}
        {options.length === 0 && (
          <div style={{ color: "#CFCFCF", textAlign: "center" }}>
            No more options
          </div>
        )}
      </StyledPopover>
    </Portal>
  );
};

export default SelectMenu;
