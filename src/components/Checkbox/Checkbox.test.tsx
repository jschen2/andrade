import { ReactWrapper, ShallowWrapper } from "enzyme";
import { mountComponent, shallowComponent } from "../../utils/testUtils";

import { CheckboxProps, StyledCheckbox } from "./Checkbox.styles";

import Checkbox from ".";

describe("Checkbox", () => {
  let props: CheckboxProps;
  let mountCheckbox: ReactWrapper | undefined;
  let shallowCheckbox: ShallowWrapper | undefined;

  const mountTestComponent = (): ReactWrapper => {
    if (!mountCheckbox) {
      mountCheckbox = mountComponent(Checkbox, props);
    }

    return mountCheckbox;
  };

  const shallowTestComponent = (): ShallowWrapper => {
    if (!shallowCheckbox) {
      shallowCheckbox = shallowComponent(Checkbox, props);
    }

    return shallowCheckbox;
  };

  beforeEach(() => {
    props = {
      checked: false,
      indeterminate: false,
      label: undefined,
      onClick: undefined,
      type: "checkbox"
    };

    mountCheckbox = undefined;
    shallowCheckbox = undefined;
  });

  it("renders correctly", () => {
    expect(shallowTestComponent()).toMatchSnapshot();
  });

  it("should render a StyledCheckbox", () => {
    expect(mountTestComponent().find(StyledCheckbox).length).toBe(1);
  });

  it("should not fire 'onClick'", () => {
    mountTestComponent()
      .find("input")
      .simulate("change");

    expect(jest.fn()).not.toHaveBeenCalled();
  });

  describe("if `disabled` is true", () => {
    beforeEach(() => {
      props.disabled = true;
      props.label = "Label";
    });

    it("renders correctly", () => {
      expect(shallowTestComponent()).toMatchSnapshot();
    });

    it("should render a StyledCheckbox", () => {
      expect(mountTestComponent().find(StyledCheckbox).length).toBe(1);
    });
  });

  describe("if 'label' is defined", () => {
    beforeEach(() => {
      props.indeterminate = true;
      props.label = "Checkbox";
    });

    it("should render a 'label'", () => {
      expect(mountTestComponent().find("label").length).toBe(1);
    });
  });

  describe("if 'onClick' is defined", () => {
    beforeEach(() => {
      props.checked = true;
      props.onClick = jest.fn();
    });

    it("should fire 'onClick'", () => {
      mountTestComponent()
        .find("input")
        .simulate("change");
      expect(props.onClick).toHaveBeenCalled();
    });
  });

  describe("if 'type' is defined as 'radio'", () => {
    beforeEach(() => {
      props.checked = true;
      props.type = "radio";
    });

    it("renders correctly", () => {
      expect(shallowTestComponent()).toMatchSnapshot();
    });

    it("should render a input[type='radio']", () => {
      expect(
        mountTestComponent()
          .find("input")
          .props().type
      ).toBe("radio");
    });
  });
});
