import React from "react";
import SidebarBody from "../SidebarBody";
import SidebarButton from "../SidebarButton";
import SidebarContent from "../SidebarContent";
import SidebarProvider, { SidebarContext } from "../SidebarContext";
import SidebarHeader from "../SidebarHeader";
import SidebarExpandCollapse from "./components/SidebarExpandCollapse";
import StyledSidebar, { SidebarProps } from "./Sidebar.styles";

export const Sidebar = ({
  children,
  ...props
}: SidebarProps): React.ReactElement => {
  const context = React.useContext(SidebarContext);
  return (
    <StyledSidebar {...context} {...props}>
      {children}
      <SidebarExpandCollapse />
    </StyledSidebar>
  );
};

Sidebar.Body = SidebarBody;
Sidebar.Button = SidebarButton;
Sidebar.Content = SidebarContent;
Sidebar.Context = SidebarContext;
Sidebar.Header = SidebarHeader;
Sidebar.Provider = SidebarProvider;

export default Sidebar;
