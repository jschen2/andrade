## [2.7.1](https://gitlab.com/jschen2/andrade/compare/v2.7.0...v2.7.1) (2020-12-24)


### Bug Fixes

* **`Table`:** make header fixed while scrolling ([4cc8b19](https://gitlab.com/jschen2/andrade/commit/4cc8b19da460c529fc3778314e305bd3a5fed58a))

# [2.7.0](https://gitlab.com/jschen2/andrade/compare/v2.6.8...v2.7.0) (2020-12-23)


### Bug Fixes

* **`Sidebar`:** adjust arrow click ([db55002](https://gitlab.com/jschen2/andrade/commit/db550022b71d662379fdadabe95f1df8d16232bd))


### Features

* **`SidebarContent`:** add component ([1077cc2](https://gitlab.com/jschen2/andrade/commit/1077cc2f81e106b6b9d49556fe4cf43badd7cb47))

## [2.6.8](https://gitlab.com/jschen2/andrade/compare/v2.6.7...v2.6.8) (2020-12-23)


### Bug Fixes

* **`Alert`:** update style to highlight better ([676d037](https://gitlab.com/jschen2/andrade/commit/676d0379041668ea221ace7c613f6882629331c1))

## [2.6.7](https://gitlab.com/jschen2/andrade/compare/v2.6.6...v2.6.7) (2020-11-26)


### Bug Fixes

* **`Table`:** change how selected rows are set in table body ([22aa2b1](https://gitlab.com/jschen2/andrade/commit/22aa2b16818d8963397755621198cafb0a85f7dd))

## [2.6.6](https://gitlab.com/jschen2/andrade/compare/v2.6.5...v2.6.6) (2020-11-10)


### Bug Fixes

* update styles ([6bf0db7](https://gitlab.com/jschen2/andrade/commit/6bf0db709a264c2b505666c8580a31a45e5ccc81))

## [2.6.5](https://gitlab.com/jschen2/andrade/compare/v2.6.4...v2.6.5) (2020-11-08)


### Bug Fixes

* **`Label`:** get label section aligned ([9dbc16a](https://gitlab.com/jschen2/andrade/commit/9dbc16ab0668b9a0cbcbd1a822c74c6ab02a4ba8))

## [2.6.4](https://gitlab.com/jschen2/andrade/compare/v2.6.3...v2.6.4) (2020-11-08)


### Bug Fixes

* align navbar popover and align input for multi select ([7c52f1d](https://gitlab.com/jschen2/andrade/commit/7c52f1dfffabc3f65242bdd0b5ee89b7c843d260))

## [2.6.3](https://gitlab.com/jschen2/andrade/compare/v2.6.2...v2.6.3) (2020-11-08)


### Bug Fixes

* **`Popover`:** actually match up to target ([ca31e20](https://gitlab.com/jschen2/andrade/commit/ca31e203c2bc68fe32fab0d7009ccaecb9ee68b8))

## [2.6.2](https://gitlab.com/jschen2/andrade/compare/v2.6.1...v2.6.2) (2020-10-15)


### Bug Fixes

* **`SVGLegend`:** complete legend component for LineChart ([f9fd442](https://gitlab.com/jschen2/andrade/commit/f9fd442f4ecbf649a1813165ef4e3a4f3e627629))

## [2.6.1](https://gitlab.com/jschen2/andrade/compare/v2.6.0...v2.6.1) (2020-07-28)


### Bug Fixes

* **`LineChart`:** add touch events ([7db176f](https://gitlab.com/jschen2/andrade/commit/7db176f73fc04b5927a85fbff6d273532f5317c1))

# [2.6.0](https://gitlab.com/jschen2/andrade/compare/v2.5.1...v2.6.0) (2020-07-10)


### Features

* **`charts`:** add tooltip ([1f755b8](https://gitlab.com/jschen2/andrade/commit/1f755b8cd856a33940cce4e1db3b6ba26ef6bed1))
* **charts:** introduce tooltip ([bb5b1d2](https://gitlab.com/jschen2/andrade/commit/bb5b1d2a1538182c37676ec3a802bcc124c8e6cf))

## [2.5.1](https://gitlab.com/jschen2/andrade/compare/v2.5.0...v2.5.1) (2020-03-20)


### Bug Fixes

* **`LineChart`:** add labels ([412cb9a](https://gitlab.com/jschen2/andrade/commit/412cb9a8ffceb5fe3c05a8d48d8d6f0ddea5c35b))

# [2.5.0](https://gitlab.com/jschen2/andrade/compare/v2.4.1...v2.5.0) (2020-03-19)


### Features

* **`LineChart`:** introduce component ([cf67bb5](https://gitlab.com/jschen2/andrade/commit/cf67bb5667fd2892bac741bf695337560ba7c627))

## [2.4.1](https://gitlab.com/jschen2/andrade/compare/v2.4.0...v2.4.1) (2020-03-18)


### Bug Fixes

* **`Table`:** add formatting types and handle separation in cells ([d2589cf](https://gitlab.com/jschen2/andrade/commit/d2589cfcbc5f23bbe2a44e1c58cbdaf6eabffb55))

# [2.4.0](https://gitlab.com/jschen2/andrade/compare/v2.3.0...v2.4.0) (2020-03-17)


### Features

* **`TreeView`:** introduce component ([b8beec0](https://gitlab.com/jschen2/andrade/commit/b8beec0a6d1b251a4a0a49cc93a1916be505999c))

# [2.3.0](https://gitlab.com/jschen2/andrade/compare/v2.2.0...v2.3.0) (2020-03-14)


### Features

* **`Table`:** add pagination ([c9482d5](https://gitlab.com/jschen2/andrade/commit/c9482d50e96491495aa81b62e64df386bc8659b2))

# [2.2.0](https://gitlab.com/jschen2/andrade/compare/v2.1.0...v2.2.0) (2020-03-11)


### Features

* **`TextArea`:** introduce component ([347190f](https://gitlab.com/jschen2/andrade/commit/347190f28bab968b6bd6f7bbf8e8f8d907de5083))

# [2.1.0](https://gitlab.com/jschen2/andrade/compare/v2.0.0...v2.1.0) (2020-03-11)


### Features

* **`Slider`:** add steps ([fde4cef](https://gitlab.com/jschen2/andrade/commit/fde4cefb2971c1c368cb8226214578d32dcabebd))

# [2.0.0](https://gitlab.com/jschen2/andrade/compare/v1.17.0...v2.0.0) (2020-03-11)


### Features

* **`Tabs`:** allow changing tab positioning around content ([1040e99](https://gitlab.com/jschen2/andrade/commit/1040e996125c60e82d22d83baea9f75687dafa18))


### BREAKING CHANGES

* **`Tabs`:** `Sidebar` and `Tabs` have removed their child exports in favor of using a compound component format. Example: `<Tabs.Tab />` and `<Sidebar.Button />`

# [1.17.0](https://gitlab.com/jschen2/andrade/compare/v1.16.1...v1.17.0) (2020-03-11)


### Features

* **`Link`:** introduce component ([8b0d5e2](https://gitlab.com/jschen2/andrade/commit/8b0d5e22e41a192aeb9c555d47a1cf0ed3ffaed8))

## [1.16.1](https://gitlab.com/jschen2/andrade/compare/v1.16.0...v1.16.1) (2020-03-11)


### Bug Fixes

* **`Select`:** clean up styles ([64fd8b6](https://gitlab.com/jschen2/andrade/commit/64fd8b63c4a56d665f710b3a42b1e9b58bae4fe8))

# [1.16.0](https://gitlab.com/jschen2/andrade/compare/v1.15.0...v1.16.0) (2020-03-10)


### Bug Fixes

* **`Table`:** fix show/hide columns disappearing on selection ([e23fbf8](https://gitlab.com/jschen2/andrade/commit/e23fbf8b57bb4d2f6a3e20c459914ec41b5a3ce2))


### Features

* **`Table`:** introduce drag to select ([82b3dc2](https://gitlab.com/jschen2/andrade/commit/82b3dc2d33944cb08cf635f67189e67a90c4dcfb))

# [1.15.0](https://gitlab.com/jschen2/andrade/compare/v1.14.0...v1.15.0) (2020-03-09)


### Features

* **`Indicator`:** introduce component ([c1d57cc](https://gitlab.com/jschen2/andrade/commit/c1d57cca1f96cef21deb81584914f622cd743d1d))

# [1.14.0](https://gitlab.com/jschen2/andrade/compare/v1.13.4...v1.14.0) (2020-03-09)


### Features

* **`Label`:** add icons and sections ([fb244a9](https://gitlab.com/jschen2/andrade/commit/fb244a9028562d05f282fd610c3b0b0c51da2770))

## [1.13.4](https://gitlab.com/jschen2/andrade/compare/v1.13.3...v1.13.4) (2020-03-08)


### Bug Fixes

* **`List`:** add boolean to list for header ([9768d3d](https://gitlab.com/jschen2/andrade/commit/9768d3dc61883a7d7963c6cde588baa0d066ff93))

## [1.13.3](https://gitlab.com/jschen2/andrade/compare/v1.13.2...v1.13.3) (2020-03-08)


### Bug Fixes

* add disabled states ([87f9c98](https://gitlab.com/jschen2/andrade/commit/87f9c981030cc6a66d8660c940e0b6eb3813fb29))
* add disabled states ([147dc90](https://gitlab.com/jschen2/andrade/commit/147dc909bb11148357fcd5892560938aa0f901c3))

## [1.13.2](https://gitlab.com/jschen2/andrade/compare/v1.13.1...v1.13.2) (2020-03-06)


### Bug Fixes

* **`Icon`:** add displayName ([13cea97](https://gitlab.com/jschen2/andrade/commit/13cea975262cff83532494ffb2311905cfef65f0))
* **`SectionTitle`:** change string to ReactNode ([673e0d2](https://gitlab.com/jschen2/andrade/commit/673e0d2120d50fec1858eb0bebd080262b06bb3d))

## [1.13.1](https://gitlab.com/jschen2/andrade/compare/v1.13.0...v1.13.1) (2020-03-05)


### Bug Fixes

* **`Icon`:** add size prop for larger icons ([d78b9e2](https://gitlab.com/jschen2/andrade/commit/d78b9e2aae09f4e1c3f15648856e7361d70336fd))

# [1.13.0](https://gitlab.com/jschen2/andrade/compare/v1.12.1...v1.13.0) (2020-03-05)


### Bug Fixes

* **`ListItem`:** search for Avatar ([404ccd4](https://gitlab.com/jschen2/andrade/commit/404ccd4b63b0f63a41d2b35630898f5dc2a13b15))


### Features

* **`Alert`:** introduce component ([8a73967](https://gitlab.com/jschen2/andrade/commit/8a7396785afc8b462759e2bb1ac8263d60ec8345))

## [1.12.1](https://gitlab.com/jschen2/andrade/compare/v1.12.0...v1.12.1) (2020-03-05)


### Bug Fixes

* **`Avatar`:** move ListItemImage to be Avatar component ([5ccacf3](https://gitlab.com/jschen2/andrade/commit/5ccacf32bb234076468199144b3653932b74243c))

# [1.12.0](https://gitlab.com/jschen2/andrade/compare/v1.11.0...v1.12.0) (2020-03-05)


### Features

* **`Navbar`:** introduce component ([91d9438](https://gitlab.com/jschen2/andrade/commit/91d9438619d74dd6aae833a358143120a2dc566f))
* **`NavbarDropdownButton`:** introduce component ([0490d02](https://gitlab.com/jschen2/andrade/commit/0490d02e1458df76621d86ff93e6a9973d430abc))

# [1.11.0](https://gitlab.com/jschen2/andrade/compare/v1.10.1...v1.11.0) (2020-03-01)


### Features

* **`SidebarButton`:** add toggle to expanded mode ([631640a](https://gitlab.com/jschen2/andrade/commit/631640a37ebc303532e360ebd73302191c3359f3))

## [1.10.1](https://gitlab.com/jschen2/andrade/compare/v1.10.0...v1.10.1) (2020-02-29)


### Bug Fixes

* clean up animations ([39711e4](https://gitlab.com/jschen2/andrade/commit/39711e4b18e26fe32106ed2bb469dfeae51e7a2b))

# [1.10.0](https://gitlab.com/jschen2/andrade/compare/v1.9.1...v1.10.0) (2020-02-29)


### Features

* **`NotificationHub`:** introduce component ([6241580](https://gitlab.com/jschen2/andrade/commit/6241580b183d780ef86cb1e0d1bb32df284325da))

## [1.9.1](https://gitlab.com/jschen2/andrade/compare/v1.9.0...v1.9.1) (2020-02-28)


### Bug Fixes

* **`Accordion`:** fix animation and add more styling ([73d38a4](https://gitlab.com/jschen2/andrade/commit/73d38a442ea7046c5150b7753d4b42c0e2f2939e))

# [1.9.0](https://gitlab.com/jschen2/andrade/compare/v1.8.0...v1.9.0) (2020-02-28)


### Features

* **`Accordion`:** introduce component ([805be4e](https://gitlab.com/jschen2/andrade/commit/805be4e2747991ed40f9cc0373f7753cdbf968f8))

# [1.8.0](https://gitlab.com/jschen2/andrade/compare/v1.7.1...v1.8.0) (2020-02-27)


### Features

* **`Progress`:** introduce component ([cf82111](https://gitlab.com/jschen2/andrade/commit/cf82111389a2d437c0f916748ea07bd9dc096b2e))

## [1.7.1](https://gitlab.com/jschen2/andrade/compare/v1.7.0...v1.7.1) (2020-02-27)


### Bug Fixes

* further update styles ([7871583](https://gitlab.com/jschen2/andrade/commit/7871583828a91128c56da052d8bb6d7980d6fdbf))
* update styles ([7107777](https://gitlab.com/jschen2/andrade/commit/71077776ab45171c3a1cf1412fa7ef00894e81be))

# [1.7.0](https://gitlab.com/jschen2/andrade/compare/v1.6.0...v1.7.0) (2020-02-27)


### Features

* **`Tabs`:** introduce components ([b3f1421](https://gitlab.com/jschen2/andrade/commit/b3f1421941ddb0f49f6d26b744e347e34bb71736))

# [1.6.0](https://gitlab.com/jschen2/andrade/compare/v1.5.1...v1.6.0) (2020-02-27)


### Bug Fixes

* **`storybook`:** change padding for code ([7c4d596](https://gitlab.com/jschen2/andrade/commit/7c4d596653c8541869741bcc728d9278206a35ee))


### Features

* **`Labels`:** introduce component ([a69d0b0](https://gitlab.com/jschen2/andrade/commit/a69d0b0c85c3ac3ff3be5ac4dbfc0db60245a97c))

## [1.5.1](https://gitlab.com/jschen2/andrade/compare/v1.5.0...v1.5.1) (2020-02-26)


### Bug Fixes

* **`Button`:** change button line-height to 1 and `CircleButton` padding to circular ([43f0490](https://gitlab.com/jschen2/andrade/commit/43f0490ae5d794fbb18319d89be6fb2912534a2a))

# [1.5.0](https://gitlab.com/jschen2/andrade/compare/v1.4.0...v1.5.0) (2020-02-26)


### Features

* **`Breadcrumbs`:** introduce component ([01143dd](https://gitlab.com/jschen2/andrade/commit/01143dd383628af30c3c009952575128f14b0385))

# [1.4.0](https://gitlab.com/jschen2/andrade/compare/v1.3.1...v1.4.0) (2020-02-26)


### Features

* **`Switch`:** introduce component ([083c14d](https://gitlab.com/jschen2/andrade/commit/083c14d81e8ac48dc95da00a923d5279348d5efe))

## [1.3.1](https://gitlab.com/jschen2/andrade/compare/v1.3.0...v1.3.1) (2020-02-25)


### Bug Fixes

* copy over font files in build ([f9bb2d0](https://gitlab.com/jschen2/andrade/commit/f9bb2d060903d6adffbaf8e7af692d3a99a4d7b8))
