import styled from "styled-components";
import { Color } from "../../../types/enums";
import { getColor, transition } from "../../../utils";

export const ChartTooltipWrapper = styled.div`
  padding: 0.25rem;
`;

export const AxisLabel = styled.text`
  fill: ${getColor(Color.gray, 600)};
  font-size: 0.75rem;
`;

export const StyledSVGBase = styled.svg`
  height: 100%;
  padding: 0;
  pointer-events: all;
  ${transition("all")};
  width: 100%;
`;

export default StyledSVGBase;
