import { ReactWrapper, ShallowWrapper } from "enzyme";
import { mountComponent, shallowComponent } from "../../utils/testUtils";

import Section, { SectionProps } from ".";

describe("Section", () => {
  let props: SectionProps;
  let mountSection: ReactWrapper | undefined;
  let shallowSection: ShallowWrapper | undefined;

  const mountTestComponent = (): ReactWrapper => {
    if (!mountSection) {
      mountSection = mountComponent(Section, props);
    }

    return mountSection;
  };

  const shallowTestComponent = (): ShallowWrapper => {
    if (!shallowSection) {
      shallowSection = shallowComponent(Section, props);
    }

    return shallowSection;
  };

  beforeEach(() => {
    props = {
      full: undefined,
      shadow: undefined
    };

    mountSection = undefined;
    shallowSection = undefined;
  });

  it("should render correctly", () => {
    expect(shallowTestComponent()).toMatchSnapshot();
  });

  it("should mount correctly", () => {
    expect(mountTestComponent().find(Section)).toBeDefined();
  });
});
