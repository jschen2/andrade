import { mount, ReactWrapper, shallow, ShallowWrapper } from "enzyme";
import React from "react";
import AndradeTheme from "../components/AndradeTheme";

export function mountComponent(
  Tag: React.ComponentType,
  props: any
): ReactWrapper {
  return mount(
    <AndradeTheme>
      <Tag {...props} />
    </AndradeTheme>
  );
}

export function shallowComponent(
  Tag: React.ComponentType,
  props: any
): ShallowWrapper {
  return shallow(
    <AndradeTheme>
      <Tag {...props} />
    </AndradeTheme>
  );
}

export function mountWithTheme(children: React.ReactElement): ReactWrapper {
  const div = document.createElement("div");
  div.id = "root";
  div.style.width = "720px";
  div.style.height = "1280px";
  document.body.appendChild(div);
  return mount(<AndradeTheme>{children}</AndradeTheme>, {
    attachTo: div
  });
}

export function shallowWithTheme(children: React.ReactElement): ShallowWrapper {
  const div = document.createElement("div");
  div.id = "root";
  div.style.width = "720px";
  div.style.height = "1280px";
  document.body.appendChild(div);
  return shallow(<AndradeTheme>{children}</AndradeTheme>, {
    attachTo: div
  });
}
