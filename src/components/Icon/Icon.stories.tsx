import { select, text } from "@storybook/addon-knobs";
import React from "react";

import IconReadme from "./README.md";
import Icon from ".";

export default {
  component: Icon,
  parameters: {
    info: {
      inline: true,
      text: IconReadme
    }
  },
  title: "Icon"
};

export const BasicUse = (): React.ReactElement => (
  <Icon
    className={text("Icon Name", "fa-archive")}
    size={select("Size", [1, 2, 3, 4, 5], 1)}
  />
);
