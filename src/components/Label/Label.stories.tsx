import { select } from "@storybook/addon-knobs";
import React from "react";

import { Alignment, Color } from "../../types/enums";
import { getEnumValues } from "../../utils";
import CloseButton from "../CloseButton";
import Flex from "../Flex";
import LabelReadme from "./README.md";
import Label from ".";

export default {
  component: Label,
  parameters: {
    info: {
      inline: true,
      text: LabelReadme
    }
  },
  title: "Label"
};

export const BasicUse = (): React.ReactElement => (
  <Flex alignItems={Alignment.center} justifyContent={Alignment.spaceBetween}>
    <Label
      color={
        select("Color", getEnumValues(Color), getEnumValues(Color)[0]) as Color
      }
    >
      Label
    </Label>
  </Flex>
);

export const WithIcons = (): React.ReactElement => (
  <Flex alignItems={Alignment.center} justifyContent={Alignment.spaceBetween}>
    <Label
      color={
        select("Color", getEnumValues(Color), getEnumValues(Color)[0]) as Color
      }
    >
      <Label.Icon className="fa-user" />
      Label
      <CloseButton />
    </Label>
  </Flex>
);

export const WithIconAndSection = (): React.ReactElement => (
  <Flex alignItems={Alignment.center} justifyContent={Alignment.spaceBetween}>
    <Label
      color={
        select("Color", getEnumValues(Color), getEnumValues(Color)[0]) as Color
      }
    >
      <Label.Icon className="fa-user" />
      Label
      <Label.Section>Sub-Label</Label.Section>
    </Label>
  </Flex>
);
