import React from "react";

import { Alignment } from "../../types/enums";

import StyledAccordion, {
  AccordionArrow,
  AccordionContent,
  AccordionProps,
  AccordionTitle
} from "./Accordion.styles";

export const Accordion = ({
  children,
  isOpen,
  title,
  ...props
}: AccordionProps): React.ReactElement => (
  <>
    <StyledAccordion
      alignItems={Alignment.center}
      justifyContent={Alignment.spaceBetween}
      {...props}
    >
      <AccordionTitle>{title}</AccordionTitle>
      <AccordionArrow isOpen={isOpen} />
    </StyledAccordion>
    {isOpen && <AccordionContent>{children}</AccordionContent>}
  </>
);

export default Accordion;
