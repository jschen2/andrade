import React from "react";
import moment, { Moment } from "moment";
import StyledCalendar, { CalendarDay } from "../../DateField.styles";

export interface CalendarProps {
  date: Moment;
  selectDay(idx: number): (e: React.MouseEvent<HTMLDivElement>) => void;
}

export const Calendar = ({
  date,
  selectDay,
  ...props
}: CalendarProps): React.ReactElement => {
  const start = moment(date)
    .startOf("month")
    .weekday();
  const daysInMonth = date?.daysInMonth();
  const selected = date?.format("D");
  const days = [];
  if (start !== 0) {
    for (let i = 0; i < start; i++) {
      days.push("");
    }
  }
  for (let i = 0; i < daysInMonth; i++) {
    days.push(String(i + 1));
  }
  const lastCol = (start + daysInMonth - 1) % 7;
  if (lastCol !== 6) {
    for (let i = lastCol; i < 6; i++) {
      days.push("");
    }
  }
  let row = 0;

  return (
    <StyledCalendar
      {...props}
      height={Math.round(date.daysInMonth() / 7 + 1) * 32}
    >
      {days.map((str, idx) => {
        const key = `day-${idx}`;
        const col = idx % 7;
        if (col === 0 && idx !== 0) {
          row += 1;
        }

        const isWeekend = col === 0 || col === 6;
        const isSelected = String(idx - start + 1) === selected;

        return (
          <CalendarDay
            onClick={str !== "" ? selectDay(idx - start + 1) : undefined}
            key={key}
            isSelected={isSelected}
            isWeekend={isWeekend}
            left={col * 32}
            top={row * 32}
          >
            {str}
          </CalendarDay>
        );
      })}
    </StyledCalendar>
  );
};

export default Calendar;
