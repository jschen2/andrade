import React, { Dispatch, SetStateAction } from "react";
import Portal from "../../../Portal";
import StyledTableBody, { DragSquare } from "./TableBody.styles";

export interface TableBodyProps
  extends React.HTMLAttributes<HTMLTableSectionElement> {
  dragSelect?: boolean;
  selectedRows: number[];
  setSelectedRows: Dispatch<SetStateAction<number[]>>;
}

const TableBody = ({
  children,
  dragSelect,
  selectedRows,
  setSelectedRows,
  ...props
}: TableBodyProps): React.ReactElement => {
  const [{ x, y }, setSquareStart] = React.useState({
    x: 0,
    y: 0
  });
  const [{ height, width }, setSquareDimensions] = React.useState({
    height: 0,
    width: 0
  });

  const onClick = (idx: number) => () => {
    const newSelectedRows = [...selectedRows];
    const index = newSelectedRows.findIndex(row => row === idx);
    if (index === -1) {
      newSelectedRows.push(idx);
    } else {
      newSelectedRows.splice(index, 1);
    }
    setSelectedRows(newSelectedRows);
  };

  const onDragStart = (idx: number) => (e: React.DragEvent): void => {
    const img = document.createElement("img");
    img.src =
      "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7";
    e.dataTransfer.setDragImage(img, -5000, -5000);
    setSquareStart({
      x: e.clientX,
      y: e.clientY
    });
    const newSelectedRows = [...selectedRows];
    const index = newSelectedRows.findIndex(row => row === idx);
    if (index === -1) {
      newSelectedRows.push(idx);
    }
    setSelectedRows(newSelectedRows);
  };

  const onDragOver = (idx: number) => (): void => {
    const newSelectedRows = [...selectedRows];
    const index = newSelectedRows.findIndex(row => row === idx);
    if (index === -1) {
      newSelectedRows.push(idx);
    }
    setSelectedRows(newSelectedRows);
  };

  const onDrag = (e: React.DragEvent): void => {
    setSquareDimensions({
      height: e.clientY - y,
      width: e.clientX - x
    });
  };

  const onDragEnd = (): void => {
    setSquareStart({
      x: 0,
      y: 0
    });
    setSquareDimensions({
      height: 0,
      width: 0
    });
  };

  return (
    <StyledTableBody {...props} onDragOver={onDrag}>
      <Portal>
        <DragSquare
          style={{
            height: Math.abs(height),
            transform: `translate(${x + (width < 0 ? width : 0)}px, ${y +
              (height < 0 ? height : 0)}px)`,
            width: Math.abs(width),
            visibility: x > 0 ? "visible" : "hidden"
          }}
        />
      </Portal>
      {React.Children.toArray(children).map(child => {
        if (React.isValidElement(child) && dragSelect) {
          return React.cloneElement(child, {
            draggable: "true",
            onClick: onClick(child.props.rowId),
            onDragEnd,
            onDragOver: onDragOver(child.props.rowId),
            onDragStart: onDragStart(child.props.rowId)
          });
        }

        return child;
      })}
    </StyledTableBody>
  );
};

export default TableBody;
