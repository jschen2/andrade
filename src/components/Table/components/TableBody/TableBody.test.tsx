import { ReactWrapper, ShallowWrapper } from "enzyme";
import React from "react";
import { mountWithTheme, shallowComponent } from "../../../../utils/testUtils";

import TableBody, { TableBodyProps } from ".";

describe("TableBody", () => {
  let props: TableBodyProps;
  let mountTableBody: ReactWrapper | undefined;
  let shallowTableBody: ShallowWrapper | undefined;

  const mountTestComponent = (): ReactWrapper => {
    if (!mountTableBody) {
      mountTableBody = mountWithTheme(
        <table>
          <TableBody {...props} />
        </table>
      );
    }

    return mountTableBody;
  };

  const shallowTestComponent = (): ShallowWrapper => {
    if (!shallowTableBody) {
      shallowTableBody = shallowComponent(TableBody, props);
    }

    return shallowTableBody;
  };

  beforeEach(() => {
    props = {
      setSelectedRows: jest.fn(),
      selectedRows: []
    };

    mountTableBody = undefined;
    shallowTableBody = undefined;
  });

  it("should render correctly", () => {
    expect(shallowTestComponent()).toMatchSnapshot();
  });

  it("should mount correctly", () => {
    expect(mountTestComponent().find(TableBody)).toBeDefined();
  });
});
