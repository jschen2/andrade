import React from "react";

import LinkReadme from "./README.md";
import Link from ".";

export default {
  component: Link,
  parameters: {
    info: {
      inline: true,
      text: LinkReadme
    }
  },
  title: "Link"
};

export const BasicUse = (): React.ReactElement => (
  <span>
    {"This is a "}
    <Link href="./">link</Link>
  </span>
);
