import uniqueId from "lodash/uniqueId";
import React from "react";
import TreeNode from "./components/TreeNode";

import StyledTreeView from "./TreeView.styles";

export interface NodeProp
  extends Omit<React.HTMLAttributes<HTMLDivElement>, "title"> {
  title: React.ReactNode;
  children?: (NodeProp | null | undefined)[];
}

export interface TreeViewProps extends React.HTMLAttributes<HTMLDivElement> {
  tree: NodeProp[];
}

const buildTree = (node?: NodeProp | null): any => {
  const childNodes = [];
  if (node) {
    const { children, ...props } = node;
    childNodes.push(
      <TreeNode key={uniqueId("tree-node-")} {...props}>
        {children && children.map(child => buildTree(child))}
      </TreeNode>
    );
  }
  return childNodes;
};

export const TreeView = ({
  tree,
  ...props
}: TreeViewProps): React.ReactElement => (
  <StyledTreeView {...props}>
    {tree.map(({ children, ...node }) => (
      <TreeNode key={uniqueId("tree-node-")} {...node}>
        {children && children.map(buildTree)}
      </TreeNode>
    ))}
  </StyledTreeView>
);

export default TreeView;
