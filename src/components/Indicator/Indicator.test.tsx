import { ReactWrapper, ShallowWrapper } from "enzyme";
import * as React from "react";
import { ButtonSize, Color } from "../../types/enums";
import { getEnumValues } from "../../utils";
import { mountWithTheme, shallowWithTheme } from "../../utils/testUtils";

import { IndicatorProps } from "./Indicator.styles";
import Indicator from ".";

describe("Indicator", () => {
  let props: IndicatorProps;
  let mountIndicator: ReactWrapper | undefined;
  let shallowIndicator: ShallowWrapper | undefined;

  const mountTestComponent = (size?: ButtonSize): ReactWrapper => {
    if (!mountIndicator) {
      mountIndicator = mountWithTheme(<Indicator size={size} {...props} />);
    }

    return mountIndicator;
  };

  const shallowTestComponent = (size?: ButtonSize): ShallowWrapper => {
    if (!shallowIndicator) {
      shallowIndicator = shallowWithTheme(<Indicator size={size} {...props} />);
    }

    return shallowIndicator;
  };

  beforeEach(() => {
    props = {
      color: Color.blue
    };

    mountIndicator = undefined;
    shallowIndicator = undefined;
  });

  it("should render correctly", () => {
    expect(shallowTestComponent()).toMatchSnapshot();
  });

  it("should mount correctly", () => {
    expect(mountTestComponent().find(Indicator)).toBeDefined();
  });

  test.each(getEnumValues(ButtonSize))("if size is %p", firstArg => {
    expect(shallowTestComponent(firstArg as ButtonSize)).toMatchSnapshot();
  });

  test.each(getEnumValues(ButtonSize))("if size is %p", firstArg => {
    expect(
      mountTestComponent(firstArg as ButtonSize).find(Indicator)
    ).toBeDefined();
  });

  describe("if `intensity` is not defined", () => {
    beforeEach(() => {
      props.intensity = null as any;
    });

    it("should render correctly", () => {
      expect(shallowTestComponent()).toMatchSnapshot();
    });

    it("should mount correctly", () => {
      expect(mountTestComponent().find(Indicator)).toBeDefined();
    });
  });
});
