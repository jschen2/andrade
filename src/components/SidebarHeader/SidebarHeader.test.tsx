import { ReactWrapper, ShallowWrapper } from "enzyme";
import { mountComponent, shallowComponent } from "../../utils/testUtils";

import SidebarHeader, { SidebarHeaderProps } from ".";

describe("SidebarHeader", () => {
  let props: SidebarHeaderProps;
  let mountSidebarHeader: ReactWrapper | undefined;
  let shallowSidebarHeader: ShallowWrapper | undefined;

  const mountTestComponent = (): ReactWrapper => {
    if (!mountSidebarHeader) {
      mountSidebarHeader = mountComponent(SidebarHeader, props);
    }

    return mountSidebarHeader;
  };

  const shallowTestComponent = (): ShallowWrapper => {
    if (!shallowSidebarHeader) {
      shallowSidebarHeader = shallowComponent(SidebarHeader, props);
    }

    return shallowSidebarHeader;
  };

  beforeEach(() => {
    props = {};

    mountSidebarHeader = undefined;
    shallowSidebarHeader = undefined;
  });

  it("should render correctly", () => {
    expect(shallowTestComponent()).toMatchSnapshot();
  });

  it("should mount correctly", () => {
    expect(mountTestComponent().find(SidebarHeader)).toBeDefined();
  });
});
