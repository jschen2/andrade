import React from "react";
import { onEnter } from "../../../../utils";
import Container from "../../../Container";
import Icon from "../../../Icon";
import CalendarControl from "./CalendarControls.styles";

export interface CalendarControlsProps
  extends React.HTMLAttributes<HTMLDivElement> {
  add(): void;
  subtract(): void;
}

export const CalendarControls = ({
  add,
  children,
  subtract,
  ...props
}: CalendarControlsProps): React.ReactElement => (
  <CalendarControl {...props}>
    <Container
      onClick={subtract}
      role="button"
      tabIndex={0}
      onKeyDown={onEnter(subtract)}
    >
      <Icon className="fa-angle-double-left" />
    </Container>
    {children}
    <Container
      onClick={add}
      role="button"
      tabIndex={0}
      onKeyDown={onEnter(add)}
    >
      <Icon className="fa-angle-double-right" />
    </Container>
  </CalendarControl>
);

export default CalendarControls;
