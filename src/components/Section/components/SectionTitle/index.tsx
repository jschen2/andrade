import React from "react";
import { ThemeContext } from "styled-components";
import { getColor, isNullOrUndefined, onOutsideClick } from "../../../../utils";
import { Alignment, Color, Placement } from "../../../../types/enums";
import Header from "../../../Header";
import Icon from "../../../Icon";
import Popover from "../../../Popover";
import Flex from "../../../Flex";

export interface SectionTitleProps
  extends Omit<React.HTMLAttributes<HTMLDivElement>, "title"> {
  buttons?: React.HTMLAttributes<HTMLDivElement>[];
  title: React.ReactNode;
}

const SectionTitle = ({
  buttons,
  id,
  title,
  children,
  ...props
}: SectionTitleProps): React.ReactElement => {
  const theme = React.useContext(ThemeContext);
  const sectionId = id || "section";
  const popoverId = `${sectionId}-popover-overflow-menu`;
  const popoverRef: React.RefObject<any> = React.useRef();
  const [isOpen, setIsOpen] = React.useState(false);

  React.useEffect(() => {
    window.addEventListener(
      "click",
      onOutsideClick(popoverRef.current, setIsOpen)
    );

    return function cleanup() {
      window.removeEventListener(
        "click",
        onOutsideClick(popoverRef.current, setIsOpen)
      );
    };
  }, [popoverRef.current]);

  const onClick = (): void => {
    setIsOpen(true);
  };

  return (
    <Flex
      alignItems={Alignment.center}
      justifyContent={Alignment.spaceBetween}
      id={id}
      style={{ flex: "0 0 40px" }}
      {...props}
    >
      <Header size={3}>{title}</Header>
      {isNullOrUndefined(buttons) && <div>{children}</div>}
      {!isNullOrUndefined(buttons) && (
        <div
          id={popoverId}
          style={{ color: getColor(Color.purple, 500)({ theme }) }}
        >
          <Icon iconSetClassName="fa-ellipsis-h" onClick={onClick} />
          <Popover
            ref={popoverRef}
            isOpen={isOpen}
            placement={Placement.bottom}
            target={popoverId}
          >
            {buttons.map((button, idx) => {
              const key = `section-title-button-${idx}`;
              return <div key={key}>{button.children}</div>;
            })}
          </Popover>
        </div>
      )}
    </Flex>
  );
};

export default SectionTitle;
