import React from "react";
import styled, { css } from "styled-components";
import Button from "../Button";
import { borderRadius, getColor, getShadow, transition } from "../../utils";
import { Color } from "../../types/enums";

export const ToggleButton = styled(({ active, ...props }) => (
  <Button {...props} />
))<{ value?: string }>`
  color: ${getColor(Color.gray, 700)};
  position: relative;
  z-index: 1;

  &:hover {
    background-color: transparent;
    color: ${getColor(Color.gray, 600)};
    ${getShadow(0)}
  }

  &:after {
    content: none;
  }

  &:last-child {
    border-right: none;
  }
`;

const StyledToggle = styled.div<{
  disabled?: boolean;
  left: number;
  width: number;
}>`
  background-color: ${getColor(Color.gray, 125)};
  ${borderRadius};
  ${getShadow(0)};
  height: max-content;
  position: relative;
  width: max-content;

  &:before {
    background-color: ${getColor(Color.gray, 0)};
    ${borderRadius};
    content: "";
    display: block;
    ${getShadow(1)};
    height: calc(100% - 1rem);
    left: 0.5rem;
    transform: translateX(${props => props.left}px);
    position: absolute;
    top: 0.5rem;
    ${transition("transform")};
    width: ${props => props.width}px;
  }

  ${props =>
    props.disabled &&
    css`
      cursor: not-allowed;
      opacity: 0.5;
    `};
`;

export default StyledToggle;
