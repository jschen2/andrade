import React from "react";
import {
  CheckboxLabel,
  CheckboxProps,
  StyledCheckbox
} from "./Checkbox.styles";

export const Checkbox = ({
  checked,
  disabled,
  id,
  indeterminate,
  label,
  onClick,
  type,
  ...props
}: CheckboxProps): React.ReactElement => {
  const onChange = (e: React.ChangeEvent<HTMLInputElement>): void => {
    if (onClick) {
      onClick(e as any);
    }
  };
  return (
    <StyledCheckbox
      onClick={!disabled ? onClick : undefined}
      checked={checked}
      disabled={disabled}
      indeterminate={indeterminate}
      type={type}
    >
      <input
        id={id}
        type={type}
        checked={checked}
        disabled={disabled}
        onChange={!disabled ? onChange : undefined}
        {...props}
      />
      <div />
      {label && (
        <CheckboxLabel disabled={disabled} htmlFor={id}>
          {label}
        </CheckboxLabel>
      )}
    </StyledCheckbox>
  );
};

Checkbox.defaultProps = {
  checked: false,
  indeterminate: false,
  type: "checkbox"
};

export default Checkbox;
