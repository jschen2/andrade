import React from "react";
import { css, FlattenSimpleInterpolation } from "styled-components";
import { Color, Font } from "../types/enums";

export type ColorIntensity =
  | 0
  | 100
  | 125
  | 150
  | 200
  | 300
  | 400
  | 500
  | 600
  | 700
  | 800
  | 900;

export interface ColorProps {
  [key: string]: string;
}

export interface Props {
  theme: {
    animation: {
      easeOutElastic: string;
    };
    colors: {
      blue: ColorProps;
      red: ColorProps;
      green: ColorProps;
      orange: ColorProps;
      yellow: ColorProps;
      purple: ColorProps;
      gray: ColorProps;
    };
    fonts: {
      body: string;
      code: string;
      header: string;
    };
    shadows: string[];
  };
}

export const borderRadiusNumber = 0.25;
export const borderRadius = `border-radius: ${borderRadiusNumber}rem`;

const setIntensity = (intensity: ColorIntensity): string => {
  if (intensity < 10) {
    return `$00${intensity}`;
  }

  if (intensity < 100) {
    return `$0${intensity}`;
  }

  return `$${intensity}`;
};

export const getColor = (color: Color, intensity: ColorIntensity) => (
  props: Props
) => {
  const { colors } = props.theme;
  return colors[color][setIntensity(intensity)];
};

export const getShadow = (index: number, inset = false) => (props: Props) => {
  const { shadows } = props.theme;
  return `box-shadow: ${inset ? "inset " : ""}${shadows[index]};`;
};

export function hexToRgb(hex: string): string {
  const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(
    hex
  ) as RegExpExecArray;
  return `${parseInt(result[1], 16)},${parseInt(result[2], 16)},${parseInt(
    result[3],
    16
  )}`;
}

export function transition(types: string): string {
  return `transition: ${types
    .split(" ")
    .map(type => `${type} 332ms ease`)
    .join(", ")}`;
}

export const focusOutline = (color: Color) => (
  props: Props
): FlattenSimpleInterpolation => css`
  position: relative;
  &:after {
    border: 2px solid transparent;
    ${borderRadius};
    content: "";
    height: calc(100% - 4px);
    left: 0;
    position: absolute;
    top: 0;
    ${transition("border-color")};
    width: calc(100% - 4px);
  }

  &:focus {
    outline: none;

    &:after {
      border: 2px solid rgba(${hexToRgb(getColor(color, 500)(props))}, 0.35);
    }
  }
`;

export function combineClasses(...classNames: (string | undefined)[]): string {
  return classNames.filter(c => c !== undefined).join(" ");
}

export function isNullOrUndefined(value?: any): value is null | undefined {
  return value === undefined || value === null;
}

export function getItemWidth(str: string): number {
  const span = document.createElement("span");
  span.innerText = str;
  document.body.appendChild(span);
  const width = span.offsetWidth;
  document.body.removeChild(span);
  return width;
}

export const easeOutElastic = (props: Props): string =>
  props.theme.animation.easeOutElastic;

export function getEnumValues(someEnum: any): string[] {
  const enumValues: string[] = [];

  Object.keys(someEnum).forEach(value => {
    if (typeof someEnum[value] === "string") {
      enumValues.push(someEnum[value]);
    }
  });

  return enumValues;
}

export const getFont = (prop: Font) => (props: Props): string =>
  props.theme.fonts[prop];

export const onEnter = (callback: (e: any) => void) => (
  e: React.KeyboardEvent
): void => {
  if (e.keyCode === 13) {
    callback(e);
  }
};

export function getDocument(node?: Element): Document {
  return (node && node.ownerDocument) || document;
}

export function percentToValue(
  percent: number,
  min?: number,
  max?: number
): number {
  if (!isNullOrUndefined(min) && !isNullOrUndefined(max)) {
    return (max - min) * percent + min;
  }

  return percent;
}

export function roundValueToStep(
  value: number,
  step: number,
  min: number
): number {
  return Math.round((value - min) / step) * step + min;
}

export const onOutsideClick = (
  node: Element,
  setIsOpen: (isOpen: boolean) => void
) => (e: any) => {
  if (!isNullOrUndefined(node) && !node.contains(e.target)) {
    setIsOpen(false);
  }
};

export const generateRandom = (start: number, end: number): number =>
  Math.floor(Math.random() * end) + start;

export function chunk(array: any[], size: number): any[] {
  if (array.length === size) {
    return [array];
  }

  const chunkedArr: any[] = [];
  let index = 0;
  while (index < array.length) {
    chunkedArr.push(array.slice(index, size + index));
    index += size;
  }
  return chunkedArr;
}
