export enum Status {
  error = "error",
  info = "info",
  warning = "warning",
  success = "success"
}

export enum ButtonSize {
  sm = "sm",
  md = "md",
  lg = "lg"
}

export enum Color {
  blue = "blue",
  gray = "gray",
  green = "green",
  orange = "orange",
  purple = "purple",
  red = "red",
  yellow = "yellow"
}

export enum Font {
  body = "body",
  code = "code",
  header = "header"
}

export enum Position {
  bottom = "bottom",
  left = "left",
  right = "right",
  top = "top"
}

export enum Placement {
  bottom = "bottom",
  bottomStart = "bottomStart",
  bottomEnd = "bottomEnd",
  left = "left",
  leftEnd = "leftEnd",
  leftStart = "leftStart",
  right = "right",
  rightEnd = "rightEnd",
  rightStart = "rightStart",
  top = "top",
  topStart = "topStart",
  topEnd = "topEnd"
}

export enum Alignment {
  baseline = "baseline",
  center = "center",
  end = "end",
  flexEnd = "flex-end",
  flexStart = "flex-start",
  normal = "normal",
  spaceAround = "space-around",
  spaceBetween = "space-between",
  start = "start",
  stretch = "stretch"
}

export enum TabPosition {
  bottom = "bottom",
  left = "left",
  right = "right",
  top = "top"
}
