import styled, { css, FlattenSimpleInterpolation } from "styled-components";

import { getColor, getShadow, transition } from "../../utils";
import { Color, TabPosition } from "../../types/enums";
import Flex from "../Flex";

export interface TabProps {
  active: boolean;
}

const setBorderRadius = (
  tabPosition?: TabPosition
): FlattenSimpleInterpolation => {
  switch (tabPosition) {
    case TabPosition.bottom:
      return css`
        border-radius: 0.25rem 0.25rem 0.25rem 0;
      `;
    case TabPosition.left:
      return css`
        border-radius: 0 0.25rem 0.25rem 0.25rem;
      `;
    case TabPosition.right:
      return css`
        border-radius: 0.25rem 0 0.25rem 0.25rem;
      `;
    case TabPosition.top:
    default:
      return css`
        border-radius: 0 0.25rem 0.25rem 0.25rem;
      `;
  }
};

const setTabBorderRadius = (
  tabPosition?: TabPosition
): FlattenSimpleInterpolation => {
  switch (tabPosition) {
    case TabPosition.bottom:
      return css`
        border-radius: 0 0 0.25rem 0.25rem;
      `;
    case TabPosition.left:
      return css`
        border-radius: 0.25rem 0 0 0.25rem;
      `;
    case TabPosition.right:
      return css`
        border-radius: 0 0.25rem 0.25rem 0;
      `;
    case TabPosition.top:
    default:
      return css`
        border-radius: 0.25rem 0.25rem 0 0;
      `;
  }
};

const setTabActivePosition = (
  tabPosition?: TabPosition
): FlattenSimpleInterpolation => {
  switch (tabPosition) {
    case TabPosition.bottom:
      return css`
        height: 2px;
        left: 0;
        top: -1px;
        width: 100%;
      `;
    case TabPosition.left:
      return css`
        bottom: 0;
        height: 100%;
        right: -1px;
        width: 2px;
      `;
    case TabPosition.right:
      return css`
        bottom: 0;
        height: 100%;
        left: -1px;
        width: 2px;
      `;
    case TabPosition.top:
    default:
      return css`
        bottom: -1px;
        height: 2px;
        left: 0;
        width: 100%;
      `;
  }
};

const setTabsPosition = (
  tabPosition?: TabPosition
): FlattenSimpleInterpolation => {
  switch (tabPosition) {
    case TabPosition.left:
    case TabPosition.right:
      return css`
        flex-direction: column;
      `;
    case TabPosition.bottom:
    case TabPosition.top:
    default:
      return css`
        flex-direction: row;
      `;
  }
};

export const TabContent = styled.div<{ tabPosition?: TabPosition }>`
  display: inline-flex;
  ${props => setBorderRadius(props.tabPosition)};
  ${getShadow(0)};
  flex: 1 1 100%;
`;

TabContent.displayName = "TabContent";

export const Tab = styled.div<TabProps & { tabPosition?: TabPosition }>`
  ${props => setTabBorderRadius(props.tabPosition)};
  cursor: pointer;
  line-height: 1;
  padding: 0.5rem 0.75rem;
  position: relative;
  ${transition("background-color")};

  ${props =>
    !props.active &&
    css`
      background-color: ${getColor(Color.gray, 125)};
      ${getShadow(0, true)};
      z-index: 0;
    `};
  ${props =>
    props.active &&
    css`
      background-color: ${getColor(Color.gray, 0)};
      ${getShadow(0)};
      z-index: 1;

      &:after {
        ${setTabActivePosition(props.tabPosition)};
        background-color: ${getColor(Color.gray, 0)};
        content: "";
        position: absolute;
      }
    `};
`;

const StyledTabs = styled(Flex)<{ tabPosition?: TabPosition }>`
  display: inline-flex;
  ${props => setTabsPosition(props.tabPosition)};
  flex: 0 0 max-content;
`;

export default StyledTabs;
