import React from "react";
import styled from "styled-components";

import { PopoverButton } from "../../../Popover/Popover.styles";
import Flex from "../../../Flex";
import Checkbox from "../../../Checkbox";
import Container from "../../../Container";

export interface ColumnButton extends React.HTMLAttributes<HTMLButtonElement> {
  column: any;
  hidden: boolean;
  showHideColumn(): void;
}

export const StyledColumnButton = styled(PopoverButton)`
  &.dragging {
    opacity: 0.5;
  }
`;

export const ColumnButton = ({
  children,
  column,
  hidden,
  showHideColumn,
  ...props
}: ColumnButton): React.ReactElement => (
  <StyledColumnButton
    onClick={showHideColumn}
    style={{ justifyContent: "space-between" }}
    {...props}
  >
    <Flex>
      <Checkbox checked={!hidden} />
      <Container>{column.header}</Container>
    </Flex>
    <Flex style={{ marginLeft: "0.5rem", opacity: "0.5" }}>{children}</Flex>
  </StyledColumnButton>
);

export default ColumnButton;
