import { axisLeft } from "d3-axis";
import { select } from "d3-selection";
import React from "react";
import { isNullOrUndefined } from "../../../utils";
import StyledSVGYAxis from "./SVGYAxis.styles";

export interface XAxisProps {
  yScale: any;
  xAxisZero?: number;
}

const SVGYAxis = ({
  yScale,
  xAxisZero,
  ...props
}: XAxisProps): React.ReactElement => {
  const axisRef: React.RefObject<any> = React.useRef();

  React.useEffect(() => {
    if (!isNullOrUndefined(axisRef.current)) {
      select(axisRef.current).call(g =>
        g.call(
          axisLeft(yScale)
            .ticks(yScale.range()[0] / 70)
            .tickSizeOuter(0)
        )
      );
    }
  }, [axisRef.current, yScale, xAxisZero]);

  return (
    <StyledSVGYAxis
      ref={axisRef}
      transform={`translate(${xAxisZero}, 0)`}
      {...props}
    />
  );
};

export default SVGYAxis;
