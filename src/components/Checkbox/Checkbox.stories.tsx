import { action } from "@storybook/addon-actions";
import { boolean } from "@storybook/addon-knobs";
import React from "react";

import CheckboxReadme from "./README.md";
import Checkbox from ".";

export default {
  component: Checkbox,
  parameters: {
    info: {
      inline: true,
      text: CheckboxReadme
    }
  },
  title: "Checkbox"
};

export const AsCheckboxGroup = (): React.ReactElement => {
  const [checked, setChecked] = React.useState<string[]>([]);

  const onClick = (value: string) => (): void => {
    const temp: string[] = [...checked];
    if (temp.includes(value)) {
      const index = temp.findIndex(check => check === value);
      temp.splice(index, 1);
    } else {
      temp.push(value);
    }
    action("Checked")(temp);
    setChecked(temp);
  };

  return (
    <>
      <Checkbox
        checked={checked.includes("Label 1")}
        disabled={boolean("Disabled?", false)}
        onClick={onClick("Label 1")}
        label="Label 1"
      />
      <Checkbox
        checked={checked.includes("Label 2")}
        disabled={boolean("Disabled?", false)}
        onClick={onClick("Label 2")}
        label="Label 2"
      />
    </>
  );
};

export const AsRadioGroup = (): React.ReactElement => {
  const [checked, setChecked] = React.useState("");

  const onClick = (value: string) => (): void => {
    action("Checked")(value);
    setChecked(value);
  };

  return (
    <>
      <Checkbox
        checked={checked.includes("Label 1")}
        onClick={onClick("Label 1")}
        label="Label 1"
        type="radio"
      />
      <Checkbox
        checked={checked.includes("Label 2")}
        onClick={onClick("Label 2")}
        label="Label 2"
        type="radio"
      />
    </>
  );
};
