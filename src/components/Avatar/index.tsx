import React from "react";
import styled, { css } from "styled-components";
import { getColor, getShadow } from "../../utils";
import { Color } from "../../types/enums";

export interface AvatarProps extends React.HTMLAttributes<HTMLDivElement> {
  src?: string;
}

export const Avatar = styled(({ src, ...props }) => <div {...props} />)<
  AvatarProps
>`
  align-items: center;
  background-color: ${getColor(Color.gray, 300)};
  border-radius: 100%;
  color: ${getColor(Color.purple, 500)};
  display: flex;
  font-size: 1.5rem;
  font-weight: bold;
  ${getShadow(0)};
  justify-content: center;
  flex: 0 0 2rem;
  height: 3rem;
  padding: 0;
  width: 3rem;

  ${props =>
    props.src &&
    css`
      background-image: url("${props.src}");
      background-size: cover;
      background-position: 50% 50%;
    `};
`;

export default Avatar;
