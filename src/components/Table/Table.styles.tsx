import React from "react";
import styled, { css } from "styled-components";
import { Color, Font } from "../../types/enums";
import {
  borderRadius,
  focusOutline,
  getColor,
  getFont,
  getShadow,
  hexToRgb,
  transition
} from "../../utils";
import Icon, { StyledIcon } from "../Icon";

export enum ColumnType {
  data = "data",
  string = "string",
  number = "number"
}

export interface TableColumn {
  accessor: string;
  header: string;
  type?: ColumnType;
}

export interface TableProps extends React.HTMLAttributes<HTMLTableElement> {
  columns: TableColumn[];
  data: any[];
  dragSelect?: boolean;
  showHideColumns?: boolean;
  multiSelect?: boolean;
  onRowsSelected?(selectedRows: number[]): void;
  pageSize?: number;
  pagination?: boolean;
}

export const sharedTableStyles = css`
  display: inline-block;
  padding: 0.25rem;
`;

export const PageButton = styled.span<{ active?: boolean }>`
  color: ${getColor(Color.purple, 500)};
  cursor: pointer;

  ${focusOutline(Color.purple)};

  ${props =>
    props.active &&
    css`
      font-weight: bold;
      text-decoration: underline;
    `};
`;

export const TableHeader = styled.thead`
  ${sharedTableStyles};
  display: block;
  min-width: 100%;
  padding: 0;
  width: max-content;
`;

export const TableHeaderRow = styled.tr`
  ${sharedTableStyles};
  display: block;
  background-color: ${getColor(Color.gray, 125)};
  ${borderRadius};
  ${getShadow(0)};
  white-space: nowrap;
  width: 100%;
`;

export const TableHeaderGrip = styled(Icon)`
  color: ${getColor(Color.gray, 200)};
  cursor: col-resize;
  visibility: hidden;
`;

export const TableHeaderCell = styled.th<{ type?: ColumnType }>`
  ${sharedTableStyles};
  cursor: pointer;
  display: inline-flex;
  font-weight: bold;
  justify-content: space-between;
  min-width: 100px;
  ${props =>
    props.type === "data" &&
    css`
      justify-content: center;
    `};
  ${props =>
    props.type === "number" &&
    css`
      justify-content: flex-end;
    `};
  ${props =>
    props.type === "string" &&
    css`
      justify-content: flex-start;
    `};

  &.dragging {
    opacity: 0.5;
  }

  span {
    padding: 0.25rem;
  }

  ${StyledIcon} {
    height: 1rem;
    width: 1rem;
  }

  &:hover {
    ${TableHeaderGrip} {
      visibility: visible;
    }
  }
`;

export const TableRow = styled(({ rowId, ...props }) => <tr {...props} />)<{
  rowId: boolean;
  selected?: boolean;
}>`
  ${sharedTableStyles};
  ${props =>
    props.selected &&
    css`
      background-color: rgba(
        ${hexToRgb(getColor(Color.purple, 500)(props))},
        0.15
      );
    `};
  display: block;
  ${transition("background-color")};
  ${props =>
    props.type === "data" &&
    css`
      text-align: center;
    `};
  ${props =>
    props.type === "number" &&
    css`
      text-align: right;
    `};
  ${props =>
    props.type === "string" &&
    css`
      text-align: left;
    `};
  white-space: nowrap;
  width: 100%;
`;

export const TableCell = styled.td<{ type?: ColumnType }>`
  ${sharedTableStyles};
  display: inline-flex;
  margin-right: 1.75rem;
  min-width: calc(100px - 1.75rem);
  overflow: hidden;
  text-overflow: ellipsis;
  ${props =>
    props.type === "data" &&
    css`
      justify-content: center;
    `};
  ${props =>
    props.type === "number" &&
    css`
      justify-content: flex-end;
    `};
  ${props =>
    props.type === "string" &&
    css`
      justify-content: flex-start;
    `};
  white-space: nowrap;
`;

export const TableDragImage = styled.span`
  background-color: ${getColor(Color.gray, 100)};
  border: 1px solid
    rgba(${props => hexToRgb(getColor(Color.gray, 900)(props))}, 0.32);
  border-left: 0.25rem solid ${getColor(Color.blue, 500)};
  font-weight: bold;
  ${getShadow(1)};
  left: -100%;
  opacity: 0;
  padding: 0.75rem;
  pointer-events: none;
  position: absolute;
  top: -100%;
  ${transition("opacity")};
  z-index: 1;
`;

export const TableDragLine = styled.div`
  background-color: ${getColor(Color.blue, 500)};
  ${getShadow(1)};
  height: calc(100% - 2rem);
  opacity: 0;
  padding: 0;
  position: absolute;
  top: 2rem;
  ${transition("opacity left")};
  width: 0.25rem;
`;

export const StyledTable = styled.table`
  display: block;
  font-family: ${getFont(Font.body)};
  height: 100%;
  overflow: hidden;
`;

export default StyledTable;
