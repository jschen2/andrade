import { action } from "@storybook/addon-actions";
import { boolean, select } from "@storybook/addon-knobs";
import React from "react";

import { getEnumValues } from "../../utils";
import { ButtonSize, Color } from "../../types/enums";
import Icon from "../Icon";

import CircleButtonReadme from "./README.md";
import CircleButton from ".";

export default {
  component: CircleButton,
  parameters: {
    info: {
      inline: true,
      text: CircleButtonReadme
    }
  },
  title: "CircleButton"
};

export const BasicUse = (): React.ReactElement => (
  <CircleButton
    color={select("Colors", getEnumValues(Color), Color.purple) as Color}
    disabled={boolean("Disabled?", false)}
    size={
      select("Size", getEnumValues(ButtonSize), ButtonSize.md) as ButtonSize
    }
    ghost={boolean("Ghost?", false)}
    outline={boolean("Outline?", false)}
    onClick={() => action("Clicked")()}
  >
    <Icon className="fa-plus" />
  </CircleButton>
);
