import { select } from "@storybook/addon-knobs";
import React from "react";

import Button, { ButtonContainer } from "../Button";
import { Alignment, Color } from "../../types/enums";
import SectionReadme from "./README.md";
import img from "./example.jpg";
import Section from ".";

export default {
  component: Section,
  parameters: {
    info: {
      inline: true,
      text: SectionReadme
    }
  },
  title: "Section"
};

export const BasicUse = (): React.ReactElement => (
  <Section shadow={select("Shadow", [0, 1, 2, 3, 4], 0)}>
    <div style={{ height: "200px", width: "200px" }} />
  </Section>
);

export const WithAllComponents = (): React.ReactElement => (
  <Section shadow={select("Shadow", [0, 1, 2, 3, 4], 0)}>
    <Section.Title
      title="Section"
      buttons={[{ children: "Hello" }, { children: "Hello" }]}
    />
    <Section.Body>
      <div
        style={{ border: "1px solid black", height: "200px", width: "200px" }}
      />
    </Section.Body>
    <Section.Footer>
      <ButtonContainer justifyContent={Alignment.flexEnd}>
        <Button outline={true} color={Color.blue}>
          Cancel
        </Button>
        <Button color={Color.blue}>Save</Button>
      </ButtonContainer>
    </Section.Footer>
  </Section>
);

export const WithImage = (): React.ReactElement => (
  <Section
    full={true}
    shadow={select("Shadow", [0, 1, 2, 3, 4], 0)}
    style={{ height: "200px", width: "200px" }}
  >
    <img src={img} alt="example" style={{ height: "100%", width: "100%" }} />
  </Section>
);
