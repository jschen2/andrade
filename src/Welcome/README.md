# Andrade UI Library

This is a React-based, TypeScript-as-a-first-class-citizen UI library that has an emphasis in minimizing external
dependencies on those that are absolutely necessary. Styled-components have been used to allow for easy theming of the
components.
