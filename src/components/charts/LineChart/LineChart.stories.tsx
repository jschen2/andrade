import { text } from "@storybook/addon-knobs";
import React from "react";
import { generateRandom } from "../../../utils";

import LineChartReadme from "./README.md";
import LineChart from ".";

export default {
  component: LineChart,
  parameters: {
    info: {
      inline: true,
      text: LineChartReadme
    }
  },
  title: "Charts/LineChart"
};

const day = Date.now();
const lineData = [
  {
    date: day,
    value: generateRandom(5, 60)
  }
];
const lineData2 = [
  {
    date: day,
    value: generateRandom(5, 60)
  }
];

for (let i = 1; i < 60; i++) {
  lineData.push({
    date: lineData[i - 1].date + 860000,
    value: generateRandom(5, 60)
  });
  lineData2.push({
    date: lineData2[i - 1].date + 860000,
    value: generateRandom(5, 60)
  });
}

export const BasicUse = (): React.ReactElement => (
  <div style={{ height: "400px", width: "100%" }}>
    <LineChart
      data={[
        {
          name: "Line One",
          values: lineData
        },
        {
          name: "Line Two",
          values: lineData2
        }
      ]}
      xAxisLabel={text("X axis label", "X Axis")}
      yAxisLabel={text("Y axis label", "Y Axis")}
    />
  </div>
);
