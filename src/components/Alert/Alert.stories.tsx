import { select } from "@storybook/addon-knobs";
import React from "react";

import { Status } from "../../types/enums";
import { getEnumValues } from "../../utils";

import AlertReadme from "./README.md";
import Alert from ".";

export default {
  component: Alert,
  parameters: {
    info: {
      inline: true,
      text: AlertReadme
    }
  },
  title: "Alert"
};

export const BasicUse = (): React.ReactElement => (
  <Alert type={select("Type", ["", ...getEnumValues(Status)], "") as Status}>
    This is an alert to highlight what alerts look like. They can pretty much
    hold anything.
  </Alert>
);
