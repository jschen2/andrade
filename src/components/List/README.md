A full featured list component that includes sorting and searching through the
items that you've passed into the list.