import React from "react";
import Input, { InputProps } from "../Input";
import Icon, { IconProps } from "../Icon";
import {
  FieldContainer,
  FieldGroup,
  FieldHelpText,
  FieldLabel
} from "./Field.styles";

export interface FieldProps extends InputProps {
  /** FontAwesome icon name */
  icon?: IconProps;
  /** Input Ref Object */
  innerRef?: React.RefObject<HTMLInputElement>;
  /** Form input label */
  label?: string;
}

export const Field = ({
  children,
  defaultValue,
  disabled,
  icon,
  id,
  innerRef,
  label,
  value,
  ...props
}: FieldProps): React.ReactElement => (
  <FieldGroup disabled={disabled} label={label} value={value || defaultValue}>
    <FieldContainer>
      <Input
        id={id}
        ref={innerRef}
        value={value}
        defaultValue={defaultValue}
        disabled={disabled}
        {...props}
      />
      {label && (
        <FieldLabel disabled={disabled} htmlFor={id}>
          {label}
        </FieldLabel>
      )}
      {icon && <Icon {...icon} />}
    </FieldContainer>
    {children && <FieldHelpText>{children}</FieldHelpText>}
  </FieldGroup>
);

Field.defaultProps = {
  invalid: false
};

export default Field;
