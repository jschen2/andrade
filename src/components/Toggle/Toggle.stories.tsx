import { boolean } from "@storybook/addon-knobs";
import React from "react";

import ToggleReadme from "./README.md";
import { ToggleButton } from "./Toggle.styles";
import Toggle from ".";

export default {
  component: Toggle,
  parameters: {
    info: {
      inline: true,
      text: ToggleReadme
    }
  },
  title: "Toggle"
};

export const BasicUse = (): React.ReactElement => (
  <Toggle
    disabled={boolean("Disabled?", false)}
    onClick={e => console.log("Click", (e.target as any).value)}
  >
    <ToggleButton value="Option 1">Scan</ToggleButton>
    <ToggleButton value="Option 2">Trade</ToggleButton>
    <ToggleButton value="Option 3">Abort</ToggleButton>
  </Toggle>
);
