import React from "react";
import styled, { css } from "styled-components";
import { getColor, getShadow, transition } from "../../utils";
import { Color } from "../../types/enums";
import Flex from "../Flex";

export interface ProgressProps extends React.HTMLAttributes<HTMLDivElement> {
  color: Color;
  percentage: number;
  progressLabel?: boolean;
}

export const ProgressLine = styled.div<ProgressProps>`
  background-color: ${props => getColor(props.color, 500)(props)};
  flex: 0 0 ${props => props.percentage}%;
  ${getShadow(0)};
  position: relative;
  ${transition("flex")};

  &:first-child:last-child {
    border-radius: 1em;
  }

  &:first-child:not(:last-child) {
    border-radius: 1em 0 0 1em;
  }

  &:last-child:not(:first-child) {
    border-radius: 0 1em 1em 0;
  }

  ${props =>
    props.progressLabel &&
    css`
      &:after {
        content: "${props.percentage}%";
        color: ${getColor(Color.gray, 0)};
        font-size: 0.75rem;
        font-weight: bold;
        right: 0.5rem;
        position: absolute;
        top: 50%;
        transform: translateY(-50%);
      }
    `};
`;

export const Progress = styled(Flex)`
  border-radius: 1em;
  background-color: ${getColor(Color.gray, 100)};
  ${getShadow(0, true)};
  height: 1em;
`;

export default Progress;
