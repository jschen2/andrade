import styled from "styled-components";
import Container from "../../Container";
import Popover from "../../Popover";

export const MouseTarget = styled(Container)`
  height: 0;
  left: 0;
  position: fixed;
  top: 0;
  width: 0;
`;

export const StyledChartTooltip = styled(Popover)`
  font-size: 0.75rem;
  font-weight: bold;
  padding: 0.25rem;
`;

export default StyledChartTooltip;
