import { ReactWrapper, ShallowWrapper } from "enzyme";
import React from "react";
import { act } from "react-dom/test-utils";
import { mountWithTheme, shallowComponent } from "../../utils/testUtils";

import CircleButton from "../CircleButton";
import Icon from "../Icon";
import ListItem from "./components/ListItem";
import List, { ListProps } from ".";

const listItems = [
  {
    title: "List Item 1",
    content: "This is a standard list item."
  },
  {
    title: "List Item 2",
    content: "This is a second standard list item."
  }
];

describe("List", () => {
  let props: ListProps;
  let mountList: ReactWrapper | undefined;
  let shallowList: ShallowWrapper | undefined;

  const mountTestComponent = (): ReactWrapper => {
    if (!mountList) {
      mountList = mountWithTheme(
        <List {...props}>
          {listItems.map((item: any) => (
            <ListItem key={item.title} title={item.title}>
              {item.content}
            </ListItem>
          ))}
        </List>
      );
    }

    return mountList;
  };

  const shallowTestComponent = (): ShallowWrapper => {
    if (!shallowList) {
      shallowList = shallowComponent(List, props);
    }

    return shallowList;
  };

  beforeEach(() => {
    props = {
      title: "List Title"
    };

    mountList = undefined;
    shallowList = undefined;
  });

  it("should render correctly", () => {
    expect(shallowTestComponent()).toMatchSnapshot();
  });

  it("should mount correctly", () => {
    expect(mountTestComponent().find(List)).toBeDefined();
  });

  it("should change sort on click", () => {
    act(() => {
      mountTestComponent()
        .find(CircleButton)
        .props()
        .onClick({});
    });
    mountTestComponent().update();
    expect(
      mountTestComponent()
        .find(Icon)
        .props().className
    ).toBe("fa-sort-up");
    act(() => {
      mountTestComponent()
        .find(CircleButton)
        .props()
        .onClick({});
    });
    mountTestComponent().update();
    expect(
      mountTestComponent()
        .find(Icon)
        .props().className
    ).toBe("fa-sort-down");
    act(() => {
      mountTestComponent()
        .find(CircleButton)
        .props()
        .onClick({});
    });
    mountTestComponent().update();
    expect(
      mountTestComponent()
        .find(Icon)
        .props().className
    ).toBe("fa-sort");
  });

  describe("if 'title' is undefined", () => {
    beforeEach(() => {
      props.title = undefined;
    });

    it("should not render 'h4", () => {
      expect(mountTestComponent().find("h4").length).toBe(0);
    });
  });
});
