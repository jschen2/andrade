import styled from "styled-components";

import { Color, Status } from "../../types/enums";
import { borderRadius, getColor, getShadow } from "../../utils";
import Flex from "../Flex";
import { StyledIcon } from "../Icon";
import CloseButton from "../CloseButton";

const getNotificationColor = (type: Status): Color => {
  switch (type) {
    case Status.error:
      return Color.red;
    case Status.info:
      return Color.blue;
    case Status.warning:
      return Color.yellow;
    case Status.success:
      return Color.green;
    default:
      return Color.gray;
  }
};

export const NotificationTitle = styled.div<{ type: Status }>`
  align-items: center;
  color: ${props => getColor(getNotificationColor(props.type), 900)(props)};
  display: flex;
  font-weight: bold;
  justify-content: space-between;
  padding-bottom: 0.25rem;

  ${StyledIcon} {
    margin-right: 0.25rem;
  }

  ${CloseButton} {
    margin: 0;
  }
`;

export const NotificationContent = styled.div<{ type: Status }>`
  color: ${props => getColor(getNotificationColor(props.type), 900)(props)};
  padding-top: 0.25rem;
`;

export const Notification = styled.div<{ type: Status }>`
  animation: slideIn 332ms ease forwards;
  background-color: ${props =>
    getColor(getNotificationColor(props.type), 100)(props)};
  border: 0.125rem solid
    ${props => getColor(getNotificationColor(props.type), 500)(props)};
  ${borderRadius};
  ${getShadow(2)};
  position: relative;
  margin: 0.5rem 0;
  width: calc(100% - 1rem);

  @keyframes slideIn {
    from {
      transform: translateX(100%);
    }

    to {
      transform: translateX(0);
    }
  }
`;

const StyledNotificationHub = styled(Flex)`
  flex-direction: column;
  position: fixed;
  right: 0;
  top: 0;
  width: 375px;

  &:focus {
    border: none;
    outline: none;
  }

  @media (max-width: 375px) {
    width: 100vw;
  }
`;

export default StyledNotificationHub;
