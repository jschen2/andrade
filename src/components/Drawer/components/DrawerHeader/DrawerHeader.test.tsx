import { ReactWrapper, ShallowWrapper } from "enzyme";
import { mountComponent, shallowComponent } from "../../../../utils/testUtils";
import { Alignment } from "../../../../types/enums";

import DrawerHeader, { DrawerHeaderProps } from ".";

describe("DrawerHeader", () => {
  let props: DrawerHeaderProps;
  let mountDrawerHeader: ReactWrapper | undefined;
  let shallowDrawerHeader: ShallowWrapper | undefined;

  const mountTestComponent = (): ReactWrapper => {
    if (!mountDrawerHeader) {
      mountDrawerHeader = mountComponent(DrawerHeader, props);
    }

    return mountDrawerHeader;
  };

  const shallowTestComponent = (): ShallowWrapper => {
    if (!shallowDrawerHeader) {
      shallowDrawerHeader = shallowComponent(DrawerHeader, props);
    }

    return shallowDrawerHeader;
  };

  beforeEach(() => {
    props = {
      alignItems: Alignment.center,
      justifyContent: Alignment.center,
      toggle: jest.fn()
    };

    mountDrawerHeader = undefined;
    shallowDrawerHeader = undefined;
  });

  it("should render correctly", () => {
    expect(shallowTestComponent()).toMatchSnapshot();
  });

  it("should mount correctly", () => {
    expect(mountTestComponent().find(DrawerHeader)).toBeDefined();
  });
});
