import React from "react";

export enum TableActions {
  SetPageNum = "SetPageNum",
  SetSelectedPageSize = "SetSelectedPageSize",
  SetStartingLeft = "SetStartingLeft",
  SetReplacedColumn = "SetReplacedColumn",
  SetActive = "SetActive",
  SetSelectedRows = "SetSelectedRows",
  SetSortingKey = "SetSortingKey",
  SetFlexBasis = "SetFlexBasis",
  SetTableColumns = "SetTableColumns"
}

interface SortingKey {
  accessor: string;
  direction: string;
}

interface SetPageNum {
  type: TableActions.SetPageNum;
  payload: number;
}

interface SetSelectedPageSize {
  type: TableActions.SetSelectedPageSize;
  payload: number;
}

interface SetStartingLeft {
  type: TableActions.SetStartingLeft;
  payload: number;
}

interface SetReplacedColumn {
  type: TableActions.SetReplacedColumn;
  payload: number;
}

interface SetActive {
  type: TableActions.SetActive;
  payload: number;
}

interface SetSelectedRows {
  type: TableActions.SetSelectedRows;
  payload: number[];
}

interface SetSortingKey {
  type: TableActions.SetSortingKey;
  payload: SortingKey;
}

interface SetFlexBasis {
  type: TableActions.SetFlexBasis;
  payload: (number | undefined)[];
}

interface SetTableColumns {
  type: TableActions.SetTableColumns;
  payload: { hidden: boolean; column: any }[];
}

export type TableReducerAction =
  | SetPageNum
  | SetSelectedPageSize
  | SetStartingLeft
  | SetReplacedColumn
  | SetActive
  | SetSelectedRows
  | SetSortingKey
  | SetFlexBasis
  | SetTableColumns;

interface TableReducerState {
  active: number;
  flexBasis: (number | undefined)[];
  pageNum: number;
  replacedColumn: number;
  selectedPageSize: number;
  selectedRows: number[];
  sortingKey: SortingKey;
  startingLeft: number;
  tableColumns: { hidden: boolean; column: any }[];
}

export const tableReducer: React.Reducer<
  TableReducerState,
  TableReducerAction
> = (state: TableReducerState, action: TableReducerAction) => {
  switch (action.type) {
    case TableActions.SetActive:
      return {
        ...state,
        active: action.payload
      };
    case TableActions.SetFlexBasis:
      return {
        ...state,
        flexBasis: action.payload
      };
    case TableActions.SetPageNum:
      return {
        ...state,
        pageNum: action.payload
      };
    case TableActions.SetReplacedColumn:
      return {
        ...state,
        replacedColumn: action.payload
      };
    case TableActions.SetSelectedPageSize:
      return {
        ...state,
        selectedPageSize: action.payload
      };
    case TableActions.SetSelectedRows:
      return {
        ...state,
        selectedRows: action.payload
      };
    case TableActions.SetSortingKey:
      return {
        ...state,
        sortingKey: action.payload
      };
    case TableActions.SetStartingLeft:
      return {
        ...state,
        startingLeft: action.payload
      };
    case TableActions.SetTableColumns:
      return {
        ...state,
        tableColumns: action.payload
      };
    default:
      return state;
  }
};
