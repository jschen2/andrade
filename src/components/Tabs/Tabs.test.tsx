import { ReactWrapper, ShallowWrapper } from "enzyme";
import React from "react";
import { TabPosition } from "../../types/enums";
import { getEnumValues } from "../../utils";
import {
  mountWithTheme,
  shallowComponent,
  shallowWithTheme
} from "../../utils/testUtils";
import { TabProps } from "./Tabs.styles";

import Tabs from ".";

describe("Tabs", () => {
  let props: TabProps;
  let mountTabs: ReactWrapper | undefined;
  let shallowTabs: ShallowWrapper | undefined;

  const mountTestComponent = (position?: TabPosition): ReactWrapper => {
    if (!mountTabs) {
      mountTabs = mountWithTheme(
        <Tabs tabPosition={position}>
          <Tabs.Tab {...props}>Tab</Tabs.Tab>
          <Tabs.Content>Tab</Tabs.Content>
        </Tabs>
      );
    }

    return mountTabs;
  };

  const shallowTestComponent = (position?: TabPosition): ShallowWrapper => {
    if (!shallowTabs) {
      shallowTabs = shallowWithTheme(
        <Tabs tabPosition={position}>
          <Tabs.Tab {...props}>Tab</Tabs.Tab>
          <Tabs.Content>Tab</Tabs.Content>
        </Tabs>
      );
    }

    return shallowTabs;
  };

  beforeEach(() => {
    props = {
      active: false
    };

    mountTabs = undefined;
    shallowTabs = undefined;
  });

  it("should render correctly", () => {
    expect(shallowTestComponent()).toMatchSnapshot();
  });

  it("Tab should render correctly", () => {
    expect(shallowComponent(Tabs.Tab, { active: true })).toMatchSnapshot();
  });

  it("Tab should render correctly", () => {
    expect(shallowComponent(Tabs.Content, {})).toMatchSnapshot();
  });

  it("should mount correctly", () => {
    expect(mountTestComponent().find(Tabs)).toBeDefined();
  });

  test.each(getEnumValues(TabPosition))(
    "if `position` is defined as %p",
    firstArg => {
      expect(shallowTestComponent(firstArg as TabPosition)).toMatchSnapshot();
    }
  );

  test.each(getEnumValues(TabPosition))(
    "if `position` is defined as %p",
    firstArg => {
      expect(
        mountTestComponent(firstArg as TabPosition).find(Tabs)
      ).toBeDefined();
    }
  );

  describe("if 'active' is true", () => {
    beforeEach(() => {
      props.active = true;
    });

    it("should mount correctly", () => {
      expect(mountTestComponent().find(Tabs)).toBeDefined();
    });
  });
});
