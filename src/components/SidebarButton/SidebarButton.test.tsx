import { ReactWrapper, ShallowWrapper } from "enzyme";
import React from "react";
import { act } from "react-dom/test-utils";
import { mountWithTheme, shallowWithTheme } from "../../utils/testUtils";
import Container from "../Container";
import Icon from "../Icon";
import StyledPopover from "../Popover/Popover.styles";

import StyledSidebarButton, {
  SidebarButtonProps,
  SidebarButtonToggle,
  SidebarSubButtonGroup
} from "./SidebarButton.styles";
import SidebarButton from ".";

describe("SidebarButton", () => {
  let props: SidebarButtonProps;
  let mountSidebarButton: ReactWrapper | undefined;
  let shallowSidebarButton: ShallowWrapper | undefined;

  const mountTestComponent = (): ReactWrapper => {
    if (!mountSidebarButton) {
      mountSidebarButton = mountWithTheme(
        <SidebarButton
          {...props}
          subButtons={[
            {
              children: "First Sub Child"
            },
            {
              children: "Second Sub Child"
            }
          ]}
        >
          <Icon className="fa-ad" />
          <Container>First Child</Container>
        </SidebarButton>
      );
    }

    return mountSidebarButton;
  };

  const shallowTestComponent = (): ShallowWrapper => {
    if (!shallowSidebarButton) {
      shallowSidebarButton = shallowWithTheme(
        <SidebarButton
          {...props}
          subButtons={[
            {
              children: "First Sub Child"
            },
            {
              children: "Second Sub Child"
            }
          ]}
        >
          <Icon className="fa-ad" />
          <Container>First Child</Container>
        </SidebarButton>
      );
    }

    return shallowSidebarButton;
  };

  beforeEach(() => {
    props = {};

    mountSidebarButton = undefined;
    shallowSidebarButton = undefined;
  });

  it("should render correctly", () => {
    expect(shallowTestComponent()).toMatchSnapshot();
  });

  it("should mount correctly", () => {
    expect(mountTestComponent().find(SidebarButton)).toBeDefined();
  });

  it("should mount correctly", () => {
    expect(mountTestComponent().find(SidebarButton)).toBeDefined();
  });

  it("should show popover buttons", () => {
    act(() => {
      (mountTestComponent()
        .find(StyledSidebarButton)
        .props() as any).onMouseOver();
    });
    mountTestComponent().update();
    expect(mountTestComponent().find(StyledPopover)).toBeDefined();
    act(() => {
      (mountTestComponent()
        .find(StyledSidebarButton)
        .props() as any).onMouseOut({
        relatedTarget: null
      } as any);
    });
    mountTestComponent().update();
    expect(mountTestComponent().find(StyledPopover)).toBeDefined();
  });

  describe("if `expanded` is true", () => {
    beforeEach(() => {
      props.active = true;
      props.expanded = true;
    });

    it("should render correctly", () => {
      expect(shallowTestComponent()).toMatchSnapshot();
    });

    it("should mount correctly", () => {
      expect(mountTestComponent().find(SidebarButton)).toBeDefined();
    });

    it("should show children on arrow click", () => {
      act(() => {
        mountTestComponent()
          .find(SidebarButtonToggle)
          .props()
          .onClick({
            preventDefault: jest.fn(),
            stopPropagation: jest.fn()
          });
      });
      expect(mountTestComponent().find(SidebarSubButtonGroup)).toBeDefined();
    });
  });
});
