import { ReactWrapper, ShallowWrapper } from "enzyme";
import { act } from "react-dom/test-utils";
import { mountComponent, shallowComponent } from "../../utils/testUtils";
import { Alignment, Status } from "../../types/enums";
import { getEnumValues } from "../../utils";
import { FlexProps } from "../Flex";

import { Notification } from "./NotificationHub.styles";
import NotificationHub, { Notify } from ".";

describe("NotificationHub", () => {
  let props: FlexProps;
  let mountNotificationHub: ReactWrapper | undefined;
  let shallowNotificationHub: ShallowWrapper | undefined;

  const mountTestComponent = (): ReactWrapper => {
    if (!mountNotificationHub) {
      mountNotificationHub = mountComponent(NotificationHub, props);
    }

    return mountNotificationHub;
  };

  const shallowTestComponent = (): ShallowWrapper => {
    if (!shallowNotificationHub) {
      shallowNotificationHub = shallowComponent(NotificationHub, props);
    }

    return shallowNotificationHub;
  };

  beforeEach(() => {
    props = {
      alignItems: Alignment.center,
      justifyContent: Alignment.center
    };

    mountNotificationHub = undefined;
    shallowNotificationHub = undefined;
  });

  it("should render correctly", () => {
    expect(shallowTestComponent()).toMatchSnapshot();
  });

  it("should mount correctly", () => {
    expect(mountTestComponent().find(NotificationHub)).toBeDefined();
  });

  describe("if notification is sent", () => {
    it("should render correctly", () => {
      act(() => {
        Notify({
          title: "title",
          content: "content"
        });
      });
      expect(shallowTestComponent()).toMatchSnapshot();
    });

    it("should mount correctly", () => {
      act(() => {
        Notify({
          title: "title",
          content: "content"
        });
      });
      expect(mountTestComponent().find(NotificationHub)).toBeDefined();
    });
  });

  describe.each(["", ...getEnumValues(Status)])(
    "if 'type' is set to %p",
    type => {
      it("should render correctly", () => {
        act(() => {
          Notify({
            title: "title",
            content: "content",
            type: type as Status
          });
        });
        expect(shallowTestComponent()).toMatchSnapshot();
      });

      it("should mount correctly", () => {
        act(() => {
          Notify({
            title: "title",
            content: "content",
            type: type as Status
          });
        });
        expect(
          mountTestComponent()
            .find(NotificationHub)
            .find(Notification)
        ).toBeDefined();
      });
    }
  );
});
