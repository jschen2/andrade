import React from "react";

import { Color, Status } from "../../types/enums";
import Container from "../Container";
import CloseButton from "../CloseButton";
import Icon from "../Icon";
import { FlexProps } from "../Flex";
import Portal from "../Portal";
import StyledNotificationHub, {
  Notification,
  NotificationContent,
  NotificationTitle
} from "./NotificationHub.styles";

export interface Notification {
  content: string;
  title: string;
  type?: Status;
}

// eslint-disable-next-line
export let Notify: (notification: Notification) => void;

export const NotificationHub = (
  props: FlexProps
): React.ReactElement | null => {
  const [notifications, setNotification] = React.useState<any[]>([]);

  Notify = (notification: Notification) => {
    setNotification(prevNotifications => [...prevNotifications, notification]);
  };

  const closeNotification = (idx: number) => () => {
    setNotification(prevNotifications => {
      const newNotifications = [...prevNotifications];
      newNotifications.splice(idx, 1);
      return newNotifications;
    });
  };

  const getNotificationIcon = (type: Status): string => {
    switch (type) {
      case Status.error:
        return "fa-do-not-enter";
      case Status.info:
        return "fa-exclamation-circle";
      case Status.warning:
        return "fa-radiation-alt";
      case Status.success:
        return "fa-check-circle";
      default:
        return Color.gray;
    }
  };

  if (notifications.length > 0) {
    return (
      <Portal>
        <StyledNotificationHub {...props} tabIndex={-1}>
          {notifications.map(({ content, title, type }, idx) => {
            const key = `notification-${idx}`;
            return (
              <Notification type={type} key={key}>
                <NotificationTitle type={type}>
                  <Container>
                    {type && <Icon className={getNotificationIcon(type)} />}
                    {title}
                  </Container>
                  <CloseButton onClick={closeNotification(idx)} />
                </NotificationTitle>
                <NotificationContent type={type}>{content}</NotificationContent>
              </Notification>
            );
          })}
        </StyledNotificationHub>
      </Portal>
    );
  }

  return null;
};

export default NotificationHub;
