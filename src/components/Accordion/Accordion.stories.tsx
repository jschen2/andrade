import React from "react";

import AccordionReadme from "./README.md";
import Accordion from ".";

export default {
  component: Accordion,
  parameters: {
    info: {
      inline: true,
      text: AccordionReadme
    }
  },
  title: "Accordion"
};

export const BasicUse = (): React.ReactElement => {
  const [isOpen, setIsOpen] = React.useState(false);
  const onClick = (): void => setIsOpen(prevIsOpen => !prevIsOpen);
  return (
    <Accordion isOpen={isOpen} onClick={onClick} title="Accordion">
      This is some cool accordion content that is ready to be seen.
    </Accordion>
  );
};
