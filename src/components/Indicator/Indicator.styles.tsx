import styled, { css } from "styled-components";
import { ButtonSize, Color } from "../../types/enums";
import { ColorIntensity, getColor, getShadow } from "../../utils";
import Container from "../Container";

export interface IndicatorProps {
  color: Color;
  intensity?: ColorIntensity;
  size?: ButtonSize;
}

const setSize = (size?: ButtonSize): any => {
  switch (size) {
    case ButtonSize.sm:
      return css`
        height: 0.75rem;
        width: 0.75rem;
      `;
    case ButtonSize.lg:
      return css`
        height: 1.55rem;
        width: 1.5rem;
      `;
    case ButtonSize.md:
    default:
      return css`
        height: 1.125rem;
        width: 1.125rem;
      `;
  }
};

export const StyledIndicator = styled(Container)<IndicatorProps>`
  background-color: ${props => getColor(props.color, props.intensity || 500)};
  border: 1px solid ${props => getColor(props.color, 600)};
  border-radius: 100%;
  ${getShadow(0)};
  ${props => setSize(props.size)};
`;

export default StyledIndicator;
