import React from "react";
import styled from "styled-components";
import Icon from "../Icon";
import { combineClasses } from "../../utils";

const CloseButton = styled(({ className, ...props }) => (
  <Icon
    className={combineClasses(className, "fa-times")}
    noPadding={true}
    {...props}
  />
))`
  cursor: pointer;
`;

export default CloseButton;
