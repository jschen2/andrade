import { ReactWrapper, ShallowWrapper } from "enzyme";

import { mountComponent, shallowComponent } from "../../utils/testUtils";
import { ButtonSize, Color } from "../../types/enums";
import Button, { ButtonProps } from ".";

describe("Button", () => {
  let props: ButtonProps;
  let shallowButton: ShallowWrapper | undefined;
  let mountButton: ReactWrapper | undefined;

  const mountTestComponent = (): ReactWrapper => {
    if (!mountButton) {
      mountButton = mountComponent(Button, props);
    }

    return mountButton;
  };

  const shallowTestComponent = (): ShallowWrapper => {
    if (!shallowButton) {
      shallowButton = shallowComponent(Button, props);
    }

    return shallowButton;
  };

  beforeEach(() => {
    props = {
      color: Color.blue,
      outline: false,
      size: ButtonSize.md
    };

    shallowButton = undefined;
    mountButton = undefined;
  });

  it("should render correctly", () => {
    expect(shallowTestComponent()).toMatchSnapshot();
  });

  describe("if 'outline' is true", () => {
    beforeEach(() => {
      props.outline = true;
    });

    it("should render outline button", () => {
      expect(mountTestComponent().find(Button).length).toBe(1);
    });
  });

  describe("if 'size' is sm", () => {
    beforeEach(() => {
      props.size = ButtonSize.sm;
    });

    it("should render small button", () => {
      expect(mountTestComponent().find(Button).length).toBe(1);
    });
  });

  describe("if 'size' is lg", () => {
    beforeEach(() => {
      props.size = ButtonSize.lg;
    });

    it("should render large button", () => {
      expect(mountTestComponent().find(Button).length).toBe(1);
    });
  });
});
