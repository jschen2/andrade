import styled, { css } from "styled-components";
import { applyFieldTheme } from "../Input";
import Container from "../Container";
import { FieldProps } from "../Field";
import { borderRadius, easeOutElastic, getColor, getShadow } from "../../utils";
import { Color } from "../../types/enums";

export interface CheckboxProps extends FieldProps {
  /** Set checked state */
  checked?: boolean;
  /** Set indeterminate state */
  indeterminate?: boolean;
  /** Set type to checkbox or radio */
  type?: "checkbox" | "radio";
}

export const CheckboxLabel = styled.label<{ disabled?: boolean }>`
  ${props =>
    props.disabled &&
    css`
      cursor: not-allowed;
      opacity: 0.5;
    `};
`;

export const StyledCheckbox = styled(Container)<CheckboxProps>`
  align-items: center;
  cursor: default;
  display: flex;
  position: relative;

  ${props =>
    props.disabled &&
    css`
      cursor: not-allowed;
      opacity: 0.5;
    `};

  > div {
    ${applyFieldTheme};
    ${borderRadius};
    ${getShadow(0)};
    height: 1em;
    position: relative;
    width: 1em;

    ${props =>
      props.type === "radio" &&
      css`
        border-radius: 100%;
      `};
  }

  > span {
    padding-left: 0.5em;
  }

  ${props =>
    props.indeterminate &&
    !props.checked &&
    css`
      div {
        background: ${getColor(Color.gray, 150)};
        &:after {
          background: ${getColor(Color.purple, 300)};
          ${borderRadius};
          content: "";
          height: 2px;
          left: 0.25rem;
          position: absolute;
          top: 0.46rem;
          width: 0.5rem;
        }
      }
    `};

  ${props =>
    props.checked &&
    props.type !== "checkbox" &&
    css`
      > input[type="radio"]:checked + div {
        background: ${getColor(Color.gray, 150)};
        ${getShadow(1)};
        &:after {
          animation: fillRadio 332ms ${easeOutElastic(props)} forwards;
          background: ${getColor(Color.purple, 300)};
          border-radius: 100%;
          content: "";
          height: 0;
          left: 50%;
          position: absolute;
          top: 50%;
          width: 0;
        }
      }

      @keyframes fillRadio {
        from {
          height: 0;
          left: 50%;
          top: 50%;
          width: 0;
        }
        to {
          height: 65%;
          left: 18%;
          top: 18%;
          width: 65%;
        }
      }
    `};

  ${props =>
    props.checked &&
    props.type === "checkbox" &&
    css`
      > input[type="checkbox"]:checked + div {
        background: ${getColor(Color.gray, 150)};
        ${getShadow(1)};
        &:before,
        &:after {
          animation: fillCheck 332ms ${easeOutElastic} forwards;
          background: ${getColor(Color.purple, 300)};
          ${borderRadius};
          content: "";
          position: absolute;
          width: 0;
        }
        &:before {
          left: 0.55rem;
          height: 0.65rem;
          top: 0.2rem;
          transform: rotate(35deg);
        }
        &:after {
          height: 0.35rem;
          left: 0.3rem;
          top: 0.45rem;
          transform: rotate(-45deg);
        }
      }

      @keyframes fillCheck {
        from {
          width: 0;
        }
        to {
          width: 2px;
        }
      }
    `};

  input {
    display: none;
    left: -100%;
    position: absolute;
    top: -100%;
  }
`;
