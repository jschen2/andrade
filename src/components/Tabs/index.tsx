import React, { CSSProperties } from "react";
import { TabPosition } from "../../types/enums";
import Flex, { FlexProps } from "../Flex";
import StyledTabs, { Tab, TabContent } from "./Tabs.styles";

export interface TabsProps extends FlexProps {
  tabPosition?: TabPosition;
}

const setPosition = (position: TabPosition): CSSProperties => {
  switch (position) {
    case TabPosition.bottom:
      return {
        flexDirection: "column-reverse"
      };
    case TabPosition.left:
      return {
        flexDirection: "row"
      };
    case TabPosition.right:
      return {
        flexDirection: "row-reverse"
      };
    case TabPosition.top:
    default:
      return {
        flexDirection: "column"
      };
  }
};

const Tabs = ({
  children,
  tabPosition,
  ...props
}: TabsProps): React.ReactElement => {
  const tabChildren = React.Children.toArray(children);
  const tabContent = tabChildren.splice(
    tabChildren.findIndex(
      child =>
        React.isValidElement(child) &&
        (child.type as React.ComponentType).displayName === "TabContent"
    ),
    1
  );
  return (
    <Flex style={{ ...setPosition(tabPosition!) }}>
      <StyledTabs tabPosition={tabPosition} {...props}>
        {tabChildren.map(child => {
          if (React.isValidElement(child)) {
            return React.cloneElement(child, {
              tabPosition
            });
          }

          return child;
        })}
      </StyledTabs>
      {tabContent.map(child => {
        if (React.isValidElement(child)) {
          return React.cloneElement(child, {
            tabPosition
          });
        }

        return child;
      })}
    </Flex>
  );
};

Tabs.defaultProps = {
  tabPosition: TabPosition.top
};

Tabs.Tab = Tab;
Tabs.Content = TabContent;

export default Tabs;
