import React from "react";
import styled, { css } from "styled-components";
import { getColor, transition } from "../../utils";
import Flex from "../Flex";
import Input from "../Input";
import { StyledIcon } from "../Icon";
import { Color } from "../../types/enums";
import StyledTextArea from "../TextArea/TextArea.styles";

export const FieldContainer = styled(Flex)`
  flex-direction: column;
`;

export const FieldLabel = styled.label<{ disabled?: boolean }>`
  font-size: 1.15rem;
  font-weight: bold;
  left: 0.5rem;
  padding: 0;
  position: absolute;
  top: 0;
  transform: translateY(calc(50% + 0.505rem));
  ${transition("transform")};
  width: fit-content;
  z-index: 1;

  ${props =>
    props.disabled &&
    css`
      cursor: not-allowed;
    `};
`;

export const FieldGroup = styled(({ label, value, ...props }) => (
  <div {...props} />
))<{ disabled?: boolean; label?: string; value?: string }>`
  display: flex;
  flex-direction: column;
  position: relative;

  ${props =>
    props.disabled &&
    css`
      cursor: not-allowed;
      opacity: 0.5;
      pointer-events: all;
    `};

  ${props =>
    props.value &&
    css`
      ${FieldLabel} {
        font-size: 0.85rem;
        transform: translateY(0.25rem);
      }
    `};

  ${Input}:focus + ${FieldLabel}, ${StyledTextArea}:focus + ${FieldLabel} {
    font-size: 0.85rem;
    transform: translateY(0.25rem);
  }

  ${Input}, ${StyledTextArea} {
    padding-top: ${props => (props.label ? "2rem" : "0.5rem")};
  }

  ${StyledIcon} {
    align-items: center;
    display: flex;
    height: calc(2.25rem - 2px);
    justify-content: center;
    padding-right: 2px;
    position: absolute;
    right: 0.5rem;
    top: calc(50% + 2px);
    width: 2rem;
  }
`;

export const FieldHelpText = styled.span`
  color: ${getColor(Color.gray, 600)};
  font-size: 0.7rem;
  padding-top: 0.25rem;
`;
