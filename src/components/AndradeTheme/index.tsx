import React from "react";
import { createGlobalStyle, ThemeProvider } from "styled-components";
import { getColor, getFont, Props } from "../../utils";

import regular from "../../fonts/css/regular.min.css";
import domine from "../../fonts/Domine-Regular.ttf";
import domineBold from "../../fonts/Domine-Bold.ttf";
import roboto from "../../fonts/Roboto-Regular.ttf";
import robotoBold from "../../fonts/Roboto-Bold.ttf";
import firaCode from "../../fonts/FiraCode-Regular.ttf";
import { Color, Font } from "../../types/enums";
import andradeTheme from "./theme.json";

export interface AndradeThemeProps {
  children?: React.ReactElement;
  theme?: Props;
}

const GlobalStyle = createGlobalStyle`
  @font-face {
    font-family: "FontAwesome";
    src: url("${regular}");  
  }
  
  @font-face {
    font-family: "Domine";
    src: url("${domine}"); 
    font-weight: normal;
    font-style: normal;
  }
  
  @font-face {
    font-family: "Domine";
    src: url("${domineBold}"); 
    font-weight: bold;
    font-style: normal;
  }
  
  @font-face {
    font-family: "Roboto";
    src: url("${roboto}"); 
    font-weight: normal;
    font-style: normal;
  }
  
  @font-face {
    font-family: "Roboto";
    src: url("${robotoBold}"); 
    font-weight: bold;
    font-style: normal;
  }
  
  @font-face {
    font-family: "FiraCode";
    src: url("${firaCode}"); 
  }
  
  html {
    font-size: 16px;
    line-height: 1.375rem;
  }
  
  body {
    * {
      box-sizing: border-box;
      color: ${getColor(Color.gray, 900)};
      font-family: ${getFont(Font.body)};
      padding: 0.5rem;
    }
    
    img,
    circle,
    rect,
    svg,
    line,
    path,
    text {
      padding: 0;
    }
    
    p {
      margin: 0 0 0 0.5rem;
      padding: 0.25rem;
    }
  }
`;

const AndradeTheme: React.FC<AndradeThemeProps> = ({
  theme,
  children
}: AndradeThemeProps) => (
  <ThemeProvider theme={{ ...andradeTheme, ...theme }}>
    <GlobalStyle />
    {children}
  </ThemeProvider>
);

export default AndradeTheme;
