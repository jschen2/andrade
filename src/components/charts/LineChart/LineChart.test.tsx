import { ReactWrapper, ShallowWrapper } from "enzyme";
import { act } from "react-dom/test-utils";

import { mountComponent, shallowComponent } from "../../../utils/testUtils";
import SVGBase from "../SVGBase";
import LineChart, { LineChartProps } from ".";

const lineData = [
  {
    date: 1584648845435,
    value: 50
  }
];

for (let i = 1; i < 60; i++) {
  lineData.push({
    date: lineData[i - 1].date + 860000,
    value: 50
  });
}

describe("LineChart", () => {
  let props: LineChartProps;
  let mountLineChart: ReactWrapper | undefined;
  let shallowLineChart: ShallowWrapper | undefined;

  const mountTestComponent = (): ReactWrapper => {
    if (!mountLineChart) {
      mountLineChart = mountComponent(LineChart, props);
    }

    return mountLineChart;
  };

  const shallowTestComponent = (): ShallowWrapper => {
    if (!shallowLineChart) {
      shallowLineChart = shallowComponent(LineChart, props);
    }

    return shallowLineChart;
  };

  beforeEach(() => {
    props = {
      data: [
        {
          name: "Line One",
          values: lineData
        }
      ],
      xAxisLabel: "Label",
      yAxisLabel: "Label"
    };

    mountLineChart = undefined;
    shallowLineChart = undefined;
  });

  it("should render correctly", () => {
    expect(shallowTestComponent()).toMatchSnapshot();
  });

  it("should mount correctly", () => {
    mountTestComponent();
    act(() => {
      mountTestComponent()
        .find(LineChart)
        .find(SVGBase)
        .props()
        .getSVGWidth(720, 1280);
    });
    mountTestComponent().update();
    expect(mountTestComponent().find(LineChart)).toBeDefined();
  });

  describe("if labels are undefined", () => {
    beforeEach(() => {
      props.xAxisLabel = undefined;
      props.yAxisLabel = undefined;
    });

    it("should render correctly", () => {
      expect(shallowTestComponent()).toMatchSnapshot();
    });

    it("should mount correctly", () => {
      mountTestComponent();
      act(() => {
        mountTestComponent()
          .find(LineChart)
          .find(SVGBase)
          .props()
          .getSVGWidth(720, 1280);
      });
      mountTestComponent().update();
      expect(mountTestComponent().find(LineChart)).toBeDefined();
    });
  });
});
