When `Button` is too large for your use case, `CircleButton` is a good
alternative.