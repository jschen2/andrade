`Checkbox` is great for use in forms when needing to let a user select multiple
options. Set `type` to `radio` if you wish to use as a radio component;