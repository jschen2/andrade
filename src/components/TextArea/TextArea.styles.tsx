import React from "react";
import styled from "styled-components";
import { applyFieldTheme } from "../Input";

export interface TextAreaProps
  extends React.HTMLAttributes<HTMLTextAreaElement> {
  /** Input Ref Object */
  innerRef?: React.RefObject<HTMLTextAreaElement>;
  /** Form input label */
  label?: string;
  /** Set disabled input state */
  disabled?: boolean;
  /** Set invalid input state */
  invalid?: boolean;
  /** Input value */
  value?: string;
}

export const StyledTextArea = styled.textarea<TextAreaProps>`
  ${applyFieldTheme};
`;

export default StyledTextArea;
