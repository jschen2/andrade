import { ReactWrapper, ShallowWrapper } from "enzyme";
import { Status } from "../../types/enums";
import { getEnumValues } from "../../utils";
import { mountComponent, shallowComponent } from "../../utils/testUtils";

import Alert, { AlertProps } from ".";

describe("Alert", () => {
  let props: AlertProps;
  let mountAlert: ReactWrapper | undefined;
  let shallowAlert: ShallowWrapper | undefined;

  const mountTestComponent = (type?: Status): ReactWrapper => {
    if (!mountAlert) {
      mountAlert = mountComponent(Alert, {
        ...props,
        type
      });
    }

    return mountAlert;
  };

  const shallowTestComponent = (type?: Status): ShallowWrapper => {
    if (!shallowAlert) {
      shallowAlert = shallowComponent(Alert, {
        ...props,
        type
      });
    }

    return shallowAlert;
  };

  beforeEach(() => {
    props = {};

    mountAlert = undefined;
    shallowAlert = undefined;
  });

  it("should render correctly", () => {
    expect(shallowTestComponent()).toMatchSnapshot();
  });

  it("should mount correctly", () => {
    expect(mountTestComponent().find(Alert)).toBeDefined();
  });

  test.each(getEnumValues(Status))(
    "if type is `%p`, it should render correctly",
    firstArg => {
      expect(shallowTestComponent(firstArg as Status)).toMatchSnapshot();
    }
  );

  test.each(getEnumValues(Status))(
    "if type is `%p`, it should mount correctly",
    firstArg => {
      expect(mountTestComponent(firstArg as Status)).toMatchSnapshot();
    }
  );
});
