import React from "react";
import styled, { css } from "styled-components";
import {
  borderRadius,
  easeOutElastic,
  getColor,
  getShadow,
  transition
} from "../../utils";
import { Color, Placement } from "../../types/enums";
import { StyledCheckbox } from "../Checkbox/Checkbox.styles";
import Container from "../Container";

export interface StyledPopoverProps
  extends React.HTMLAttributes<HTMLDivElement> {
  left: number | string;
  placement?: Placement;
  showArrow?: boolean;
  top: number | string;
  width?: number | string;
}

const innerFunction = React.forwardRef<HTMLDivElement, StyledPopoverProps>(
  (
    { left, placement, showArrow, top, width, ...props }: StyledPopoverProps,
    ref
  ) => <div ref={ref} {...props} />
);

export const StyledPopover = styled(innerFunction)<StyledPopoverProps>`
  animation: openPopover 332ms ${easeOutElastic} forwards;
  background-color: ${getColor(Color.gray, 0)};
  ${borderRadius};
  ${getShadow(1)};
  height: max-content;
  ${props => css`
    left: ${props.left && typeof props.left === "string"
      ? props.left
      : `${props.left}px`};
    top: ${props.top && typeof props.top === "string"
      ? props.top
      : `${props.top}px`};
  `};
  overflow: hidden;
  position: absolute;
  width: ${props =>
    !props.width
      ? "max-content"
      : typeof props.width === "string"
      ? props.width
      : `${props.width}px`};
  ${props =>
    props.showArrow &&
    css`
      &:after {
        background-color: inherit;
        ${() => {
          const { placement } = props;

          switch (placement) {
            case Placement.bottom:
              return css`
                box-shadow: -1px -1px 1px rgba(51, 51, 51, 0.35);
                left: 50%;
                top: -0.25rem;
                transform: translateX(-50%) rotate(45deg);
              `;
            case Placement.bottomStart:
              return css`
                box-shadow: -1px -1px 1px rgba(51, 51, 51, 0.35);
                right: 0.25rem;
                top: -0.25rem;
                transform: rotate(45deg);
              `;
            case Placement.bottomEnd:
              return css`
                box-shadow: -1px -1px 1px rgba(51, 51, 51, 0.35);
                left: 0.25rem;
                top: -0.25rem;
                transform: rotate(45deg);
              `;
            case Placement.left:
              return css`
                box-shadow: 1px -1px 1px rgba(51, 51, 51, 0.35);
                right: -0.25rem;
                top: 50%;
                transform: translateY(-50%) rotate(45deg);
              `;
            case Placement.leftStart:
              return css`
                box-shadow: 1px -1px 1px rgba(51, 51, 51, 0.35);
                right: -0.25rem;
                top: 0.25rem;
                transform: rotate(45deg);
              `;
            case Placement.leftEnd:
              return css`
                box-shadow: 1px -1px 1px rgba(51, 51, 51, 0.35);
                right: -0.25rem;
                bottom: 0.25rem;
                transform: rotate(45deg);
              `;
            case Placement.right:
              return css`
                box-shadow: -1px 1px 1px rgba(51, 51, 51, 0.35);
                left: -0.25rem;
                top: 50%;
                transform: translateY(-50%) rotate(45deg);
              `;
            case Placement.rightStart:
              return css`
                box-shadow: -1px 1px 1px rgba(51, 51, 51, 0.35);
                left: -0.25rem;
                top: 0.25rem;
                transform: rotate(45deg);
              `;
            case Placement.rightEnd:
              return css`
                box-shadow: -1px 1px 1px rgba(51, 51, 51, 0.35);
                left: -0.25rem;
                bottom: 0.25rem;
                transform: rotate(45deg);
              `;
            case Placement.top:
              return css`
                box-shadow: 2px 2px 2px rgba(51, 51, 51, 0.35);
                left: 50%;
                bottom: -0.25rem;
                transform: translateX(-50%) rotate(45deg);
              `;
            case Placement.topStart:
              return css`
                box-shadow: 2px 2px 2px rgba(51, 51, 51, 0.35);
                right: 0.25rem;
                bottom: -0.25rem;
                transform: rotate(45deg);
              `;
            case Placement.topEnd:
              return css`
                box-shadow: 2px 2px 2px rgba(51, 51, 51, 0.35);
                left: 0.25rem;
                bottom: -0.25rem;
                transform: rotate(45deg);
              `;
            default:
              return css`
                box-shadow: 2px 2px 2px rgba(51, 51, 51, 0.35);
                left: 50%;
                bottom: -0.25rem;
                transform: translateX(-50%) rotate(45deg);
              `;
          }
        }};
        content: "";
        position: absolute;
        width: 0.5rem;
        height: 0.5rem;
      }
    `};

  @keyframes openPopover {
    from {
      opacity: 0;
    }

    to {
      opacity: 1;
    }
  }
`;

export const PopoverButton = styled.button`
  background-color: ${getColor(Color.gray, 0)};
  border: none;
  display: flex;
  text-align: left;
  ${transition("background-color color")};
  width: 100%;

  &:hover {
    background-color: ${getColor(Color.gray, 150)};
    color: ${getColor(Color.gray, 900)};
    ${getShadow(0)};
  }

  &:active,
  &:focus {
    background-color: ${getColor(Color.gray, 200)};
    outline: none;
  }

  ${StyledCheckbox} {
    margin-right: 0.5rem;
    width: auto;
  }

  ${Container} {
    width: auto;
  }
`;

export default StyledPopover;
