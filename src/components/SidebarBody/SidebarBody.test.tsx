import { ReactWrapper, ShallowWrapper } from "enzyme";
import * as React from "react";
import { mountComponent, shallowComponent } from "../../utils/testUtils";
import { Alignment } from "../../types/enums";

import SidebarButton from "../SidebarButton";
import SidebarBody, { SidebarBodyProps } from ".";

describe("SidebarBody", () => {
  let props: SidebarBodyProps;
  let mountSidebarBody: ReactWrapper | undefined;
  let shallowSidebarBody: ShallowWrapper | undefined;

  const mountTestComponent = (): ReactWrapper => {
    if (!mountSidebarBody) {
      mountSidebarBody = mountComponent(SidebarBody, props);
    }

    return mountSidebarBody;
  };

  const shallowTestComponent = (): ShallowWrapper => {
    if (!shallowSidebarBody) {
      shallowSidebarBody = shallowComponent(SidebarBody, props);
    }

    return shallowSidebarBody;
  };

  beforeEach(() => {
    props = {
      alignItems: Alignment.center,
      children: <SidebarButton />,
      justifyContent: Alignment.center
    };

    mountSidebarBody = undefined;
    shallowSidebarBody = undefined;
  });

  it("should render correctly", () => {
    expect(shallowTestComponent()).toMatchSnapshot();
  });

  it("should mount correctly", () => {
    expect(mountTestComponent().find(SidebarBody)).toBeDefined();
  });
});
