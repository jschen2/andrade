import styled from "styled-components";

const SectionBody = styled.div`
  flex: 1 1 auto;
  overflow: auto;
`;

export default SectionBody;
