import React from "react";
import styled from "styled-components";
import Flex from "../../../Flex";
import Icon from "../../../Icon";
import { combineClasses } from "../../../../utils";

export const CloseButton = styled(({ className, ...props }) => (
  <Icon className={combineClasses(className, "fa-times")} {...props} />
))`
  cursor: pointer;
  height: 2rem;
  padding: 0.5rem;
  text-align: center;
  width: 2rem;
`;

const StyledModalHeader = styled(Flex)`
  align-items: center;
  display: flex;
  flex: 0 0 38px;
`;

export default StyledModalHeader;
