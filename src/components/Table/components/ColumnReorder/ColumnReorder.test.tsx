import { ReactWrapper, ShallowWrapper } from "enzyme";
import { mountComponent, shallowComponent } from "../../../../utils/testUtils";

import ColumnReorder, { ColumnReorderProps } from ".";

describe("ColumnReorder", () => {
  let props: ColumnReorderProps;
  let mountColumnReorder: ReactWrapper | undefined;
  let shallowColumnReorder: ShallowWrapper | undefined;

  const mountTestComponent = (): ReactWrapper => {
    if (!mountColumnReorder) {
      mountColumnReorder = mountComponent(ColumnReorder as any, props);
    }

    return mountColumnReorder;
  };

  const shallowTestComponent = (): ShallowWrapper => {
    if (!shallowColumnReorder) {
      shallowColumnReorder = shallowComponent(ColumnReorder as any, props);
    }

    return shallowColumnReorder;
  };

  beforeEach(() => {
    props = {
      columns: [],
      id: "column-reorder",
      setTableColumns: jest.fn(),
      showHideColumn: jest.fn()
    };

    mountColumnReorder = undefined;
    shallowColumnReorder = undefined;
  });

  it("should render correctly", () => {
    expect(shallowTestComponent()).toMatchSnapshot();
  });

  it("should mount correctly", () => {
    expect(mountTestComponent().find(ColumnReorder)).toBeDefined();
  });
});
