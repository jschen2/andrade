import React from "react";
import { addDecorator, addParameters } from '@storybook/react';
import { withInfo } from "@storybook/addon-info";
import { withKnobs } from "@storybook/addon-knobs";
import "@storybook/addon-console";
import { createGlobalStyle } from "styled-components";

import theme from "../src/components/AndradeTheme/theme.json";
import AndradeTheme from "../src/components/AndradeTheme";
import { Color, Font } from "../src/types/enums";
import { borderRadius, getColor, getFont } from "../src/utils";
import myTheme from "./myTheme";

const StorybookStyle = createGlobalStyle`
  #story-root {
    padding: 20px 20px;
  }
  
  div[class*="simplebar-"] {
    font-family: ${getFont(Font.code)({ theme })};
    padding: 0;
  
    .token {
      font-family: ${getFont(Font.code)({ theme })};
      padding: 1px;
    }
  }
  
  code {
    background-color: ${getColor(Color.gray, 150)};
    font-family: ${getFont(Font.code)({ theme })};
    ${borderRadius};
    padding: 0 2px;
  }
`;

addDecorator(withKnobs);
addDecorator(withInfo({
  inline: true,
  styles: stylesheet => ({
    ...stylesheet,
    header: {
      ...stylesheet.header,
      h1: {
        ...stylesheet.header.h1,
        fontFamily: getFont(Font.header)({ theme })
      },
      h2: {
        ...stylesheet.header.h2,
        fontFamily: getFont(Font.header)({ theme })
      }
    },
    infoBody: {
      ...stylesheet.infoBody,
      fontFamily: getFont(Font.body)({ theme }),
      padding: "1em"
    },
    infoStory: {
      ...stylesheet.infoStory,
      h1: {
        ...stylesheet.infoStory.h1,
        fontFamily: getFont(Font.header)({ theme })
      }
    },
    source: {
      ...stylesheet.source,
      h1: {
        ...stylesheet.source.h1,
        fontFamily: getFont(Font.header)({ theme })
      }
    }
  })
}));
addDecorator(storyFn => (
  <AndradeTheme>
    <StorybookStyle/>
    {storyFn()}
  </AndradeTheme>
));
addParameters({
  options: {
    name: "Andrade",
    addonPanelInRight: true,
    showRoots: true,
    theme: myTheme
  }
});