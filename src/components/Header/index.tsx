import React from "react";
import styled, { css } from "styled-components";
import { getFont } from "../../utils";
import { Font } from "../../types/enums";

/**
 * Header properties
 */
export interface HeaderProps extends React.HTMLAttributes<HTMLHeadingElement> {
  /** Set h tag size */
  size?: 1 | 2 | 3 | 4 | 5 | 6;
}

const Header = styled(({ size, ...props }: HeaderProps) => {
  const Tag = `h${size}`;
  return <Tag {...props} />;
})<HeaderProps>`
  font-family: ${getFont(Font.header)};
  margin: 0;
  ${props => {
    switch (props.size) {
      case 1:
        return css`
          font-size: 3rem;
          line-height: 1.05rem;
        `;
      case 2:
        return css`
          font-size: 2.25rem;
          line-height: 1.25rem;
        `;
      case 3:
        return css`
          font-size: 1.75rem;
          line-height: 1.25rem;
        `;
      case 4:
        return css`
          font-size: 1.125rem;
          line-height: 1.22222rem;
        `;
      default:
        return css`
          font-size: 1rem;
          line-height: 1.375rem;
        `;
    }
  }};
`;

Header.displayName = "Header";

export default Header;
