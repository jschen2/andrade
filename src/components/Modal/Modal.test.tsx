import { ReactWrapper, ShallowWrapper } from "enzyme";
import { mountComponent, shallowComponent } from "../../utils/testUtils";

import Modal, { ModalProps } from ".";

describe("Modal", () => {
  let props: ModalProps;
  let mountModal: ReactWrapper | undefined;
  let shallowModal: ShallowWrapper | undefined;

  const mountTestComponent = (): ReactWrapper => {
    if (!mountModal) {
      mountModal = mountComponent(Modal, props);
    }

    return mountModal;
  };

  const shallowTestComponent = (): ShallowWrapper => {
    if (!shallowModal) {
      shallowModal = shallowComponent(Modal, props);
    }

    return shallowModal;
  };

  beforeEach(() => {
    props = {
      isOpen: true,
      toggle: jest.fn()
    };

    mountModal = undefined;
    shallowModal = undefined;
  });

  it("should render correctly", () => {
    expect(shallowTestComponent()).toMatchSnapshot();
  });

  it("should mount correctly", () => {
    expect(mountTestComponent().find(Modal)).toBeDefined();
  });
});
