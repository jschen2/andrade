import React from "react";

import StyledToggle from "./Toggle.styles";

export interface ToggleProps extends React.HTMLAttributes<HTMLDivElement> {
  disabled?: boolean;
}

export const Toggle = ({
  children,
  disabled,
  onClick,
  ...props
}: ToggleProps): React.ReactElement => {
  const [active, setActive] = React.useState(-1);
  const toggleRef: React.RefObject<any> = React.useRef();
  const toggleChildren = React.Children.toArray(children);
  const [buttonLeft, setButtonLeft] = React.useState(0);
  const [buttonWidth, setButtonWidth] = React.useState(0);

  React.useEffect(() => {
    if (toggleRef?.current) {
      const selected = toggleRef.current.getElementsByTagName("button")[0];
      if (selected) {
        const { width } = selected.getBoundingClientRect();
        setButtonLeft(selected.offsetLeft - 8);
        setButtonWidth(width);
      }
    }
  }, [toggleRef.current, toggleChildren.length]);

  const extendOnClick = (index: number) => (e: React.MouseEvent) => {
    setActive(index);
    const selected = toggleRef.current.getElementsByTagName("button")[index];
    if (selected) {
      const { width } = selected.getBoundingClientRect();
      setButtonLeft(selected.offsetLeft - 8);
      setButtonWidth(width);
    }
    if (onClick) {
      onClick(e as any);
    }
  };

  return (
    <StyledToggle
      disabled={disabled}
      left={buttonLeft}
      width={buttonWidth}
      ref={toggleRef}
      {...props}
    >
      {toggleChildren.map((child, idx) => {
        if (React.isValidElement(child)) {
          return React.cloneElement(child, {
            active: active === idx,
            disabled,
            ghost: true,
            onClick: extendOnClick(idx)
          });
        }

        return child;
      })}
    </StyledToggle>
  );
};

export default Toggle;
