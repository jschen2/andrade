import React from "react";
import styled from "styled-components";
import { Color } from "../../types/enums";
import { getColor } from "../../utils";

import Container from "../Container";
import Icon from "../Icon";
import SidebarProvider from "../SidebarContext";
import SidebarReadme from "./README.md";
import Sidebar from ".";

export default {
  component: Sidebar,
  parameters: {
    info: {
      inline: true,
      text: SidebarReadme
    }
  },
  title: "Sidebar"
};

const DivWrapper = styled.div`
  border: 1px solid ${getColor(Color.gray, 900)};
  box-sizing: border-box;
`;

export const BasicUse = (): React.ReactElement => {
  const [active, setActive] = React.useState(2);
  const isActive = (num: number): boolean => num === active;
  const handleOnClick = (num: number) => () => {
    setActive(num);
  };
  return (
    <div style={{ marginLeft: "calc(-3rem - 3px)" }}>
      <SidebarProvider>
        <Sidebar>
          <Sidebar.Header>
            <>Andrade UI</>
            <>A</>
          </Sidebar.Header>
          <Sidebar.Body>
            <Sidebar.Button
              active={isActive(1)}
              onClick={handleOnClick(1)}
              subButtons={[
                {
                  active: isActive(5),
                  children: "First Sub Child",
                  onClick: handleOnClick(5)
                },
                {
                  active: isActive(6),
                  children: "Second Sub Child",
                  onClick: handleOnClick(6)
                }
              ]}
            >
              <Icon className="fa-ad" />
              <Container>First Child</Container>
            </Sidebar.Button>
            <Sidebar.Button active={isActive(2)} onClick={handleOnClick(2)}>
              <Icon className="fa-address-book" iconSetClassName="fab" />
              <Container>Second Child</Container>
            </Sidebar.Button>
            <Sidebar.Button active={isActive(3)} onClick={handleOnClick(3)}>
              <Icon className="fa-archway" />
              <Container>Third Child</Container>
            </Sidebar.Button>
            <Sidebar.Button active={isActive(4)} onClick={handleOnClick(4)}>
              <Icon className="fa-backpack" iconSetClassName="fab" />
              <Container>Fourth Child</Container>
            </Sidebar.Button>
          </Sidebar.Body>
        </Sidebar>
        <Sidebar.Content>
          <DivWrapper>Content</DivWrapper>
        </Sidebar.Content>
      </SidebarProvider>
    </div>
  );
};
