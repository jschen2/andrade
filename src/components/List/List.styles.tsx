import styled from "styled-components";
import { getColor } from "../../utils";
import { Color } from "../../types/enums";

const StyledListItem = styled.li`
  align-items: center;
  border-bottom: 1px solid ${getColor(Color.gray, 200)};
  display: flex;
  margin: 0;
  width: 100%;

  &:last-child {
    border-bottom: none;
  }

  .title {
    font-weight: bold;
    padding-bottom: 0;
  }

  > div {
    flex: 1 1 auto;
    padding-left: 0;
    padding-right: 0;
  }
`;

export default StyledListItem;
