import React from "react";
import styled, { css } from "styled-components";
import { borderRadius, getColor, isNullOrUndefined } from "../../utils";
import { ButtonSize, Color } from "../../types/enums";
import CircleButton from "../CircleButton";
import Icon from "../Icon";
import Header from "../Header";
import StyledListItem from "./List.styles";

export interface ListProps
  extends Omit<React.HTMLAttributes<HTMLDivElement>, "title"> {
  /** Show/hide header */
  header?: boolean;
  /** Set to ordered list */
  ordered?: boolean;
  /** Content to used header */
  title?: React.ReactNode;
}

export const StyledList = styled.div`
  ${borderRadius};
  box-shadow: ${props => props.theme.shadows[0]};
  display: flex;
  flex-direction: column;
  height: 100%;
  overflow: hidden;
  padding: 0;
  width: 100%;
`;

export const ListHeader = styled.div<{ titlePresent: boolean }>`
  align-items: center;
  ${borderRadius};
  background-color: ${getColor(Color.gray, 150)};
  display: flex;
  flex: 0 0 3.25rem;
  justify-content: ${props =>
    props.titlePresent ? "flex-end" : "space-between"};
  overflow: hidden;
  width: 100%;
`;

export const ListBody = styled(({ ordered, ...props }) =>
  !ordered ? <ul {...props} /> : <ol {...props} />
)`
  flex: 1 1 auto;
  margin: 0;
  list-style: none;
  overflow: auto;
  width: 100%;

  ${props =>
    props.ordered &&
    css`
      list-style: decimal;

      ${StyledListItem} {
        margin-left: 1.25rem;
        width: calc(100% - 1.25rem);
      }
    `}
`;

const List = ({
  children,
  header,
  ordered,
  title,
  ...props
}: ListProps): React.ReactElement => {
  const [sort, setSort] = React.useState("");

  const onSortClick = (): void => {
    switch (sort) {
      case "ASC":
        setSort("DESC");
        break;
      case "DESC":
        setSort("");
        break;
      default:
        setSort("ASC");
        break;
    }
  };

  return (
    <StyledList {...props}>
      {header && (
        <ListHeader titlePresent={isNullOrUndefined(title) || title === ""}>
          {title && <Header size={4}>{title}</Header>}
          {!ordered && (
            <CircleButton
              color={Color.purple}
              onClick={onSortClick}
              size={ButtonSize.sm}
            >
              <Icon
                className={`fa-sort${
                  sort === "ASC" ? "-up" : sort === "DESC" ? "-down" : ""
                }`}
                size={1}
              />
            </CircleButton>
          )}
        </ListHeader>
      )}
      <ListBody ordered={ordered}>
        {React.Children.toArray(children).sort((a, b) => {
          if (
            React.isValidElement(a) &&
            React.isValidElement(b) &&
            sort !== ""
          ) {
            if (a.props.title > b.props.title) {
              return sort === "ASC" ? 1 : -1;
            }

            if (a.props.title < b.props.title) {
              return sort === "ASC" ? -1 : 1;
            }
          }

          return 0;
        })}
      </ListBody>
    </StyledList>
  );
};

List.defaultProps = {
  header: true,
  ordered: false
};

export default List;
