import { object } from "@storybook/addon-knobs";
import React from "react";

import { Color } from "../../types/enums";
import ProgressReadme from "./README.md";
import Progress, { ProgressLine } from ".";

export default {
  component: Progress,
  parameters: {
    info: {
      inline: true,
      text: ProgressReadme
    }
  },
  title: "Progress"
};

export const BasicUse = (): React.ReactElement => {
  const progressLines = object("Progress Data", [
    {
      color: Color.purple,
      percentage: 50
    },
    {
      color: Color.blue,
      percentage: 25
    }
  ]);
  return (
    <Progress>
      {progressLines.map(progress => (
        <ProgressLine {...progress} progressLabel={true} />
      ))}
    </Progress>
  );
};
