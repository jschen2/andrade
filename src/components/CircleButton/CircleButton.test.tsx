import { ReactWrapper, ShallowWrapper } from "enzyme";

import { mountComponent, shallowComponent } from "../../utils/testUtils";
import { ButtonSize, Color } from "../../types/enums";
import { ButtonProps } from "../Button";
import CircleButton from "./index";

describe("CircleButton", () => {
  let props: ButtonProps;
  let shallowButton: ShallowWrapper | undefined;
  let mountButton: ReactWrapper | undefined;

  const testComponent = (): ReactWrapper => {
    if (!mountButton) {
      mountButton = mountComponent(CircleButton, props);
    }

    return mountButton;
  };

  const shallowTestComponent = (): ShallowWrapper => {
    if (!shallowButton) {
      shallowButton = shallowComponent(CircleButton, props);
    }

    return shallowButton;
  };

  beforeEach(() => {
    props = {
      color: Color.blue,
      outline: false,
      size: ButtonSize.md
    };

    shallowButton = undefined;
    mountButton = undefined;
  });

  it("should renders correctly", () => {
    expect(shallowTestComponent()).toMatchSnapshot();
  });

  describe("if 'outline' is true", () => {
    beforeEach(() => {
      props.outline = true;
    });

    it("should render outline button", () => {
      expect(testComponent().find(CircleButton).length).toBe(1);
    });
  });

  describe("if 'size' is sm", () => {
    beforeEach(() => {
      props.size = ButtonSize.sm;
    });

    it("should render small button", () => {
      expect(testComponent().find(CircleButton).length).toBe(1);
    });
  });

  describe("if 'size' is lg", () => {
    beforeEach(() => {
      props.size = ButtonSize.lg;
    });

    it("should render large button", () => {
      expect(testComponent().find(CircleButton).length).toBe(1);
    });
  });
});
