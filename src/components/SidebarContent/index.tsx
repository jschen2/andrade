import React from "react";
import { SidebarContext, SidebarContextProps } from "../SidebarContext";

import StyledSidebarContent from "./SidebarContent.styles";

export interface SidebarContentProps
  extends React.HTMLAttributes<any>,
    SidebarContextProps {}

export const SidebarContent = (
  props: SidebarContentProps
): React.ReactElement => {
  const { expanded } = React.useContext(SidebarContext);
  return <StyledSidebarContent expanded={expanded} {...props} />;
};

export default SidebarContent;
