import { select, text } from "@storybook/addon-knobs";
import React from "react";
import { Status } from "../../types/enums";

import { getEnumValues } from "../../utils";
import Button from "../Button";
import NotificationHubReadme from "./README.md";
import NotificationHub, { Notify } from ".";

export default {
  component: NotificationHub,
  parameters: {
    info: {
      inline: true,
      text: NotificationHubReadme
    }
  },
  title: "NotificationHub"
};

export const BasicUse = (): React.ReactElement => {
  const onClick = (): void => {
    Notify({
      content: text("Title", "Alerting alerting"),
      title: text("Content", "Alert"),
      type: select("Type", ["", ...getEnumValues(Status)], "") as Status
    });
  };
  return (
    <>
      <Button onClick={onClick}>Notify</Button>
      <NotificationHub />
    </>
  );
};
