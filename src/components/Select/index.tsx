import React from "react";
import Field, { FieldProps } from "../Field";
import { isNullOrUndefined, onOutsideClick } from "../../utils";
import Container from "../Container";
import Icon from "../Icon";
import Input from "../Input";
import SelectMenu, { Option } from "./SelectMenu";
import {
  CloseIcon,
  MenuIcon,
  SelectLabelProps,
  StyledSelect,
  StyledSelectLabel
} from "./Select.styles";

export interface SelectProps extends Omit<FieldProps, "onChange" | "value"> {
  /** Show/hide clear button */
  clearButton?: boolean;
  /** input field props */
  inputProps?: FieldProps;
  /** Set multi selection */
  multi?: boolean;
  /** Extended onChange function that returns event and data value */
  onChange?(e: React.MouseEvent, option: Option | Option[]): void;
  /** Options to be rendered in Select */
  options: Option[];
  /** selected option value */
  value?: Option | Option[];
}

export const SelectLabel = ({
  children,
  removeOption,
  ...props
}: SelectLabelProps): React.ReactElement => (
  <StyledSelectLabel {...props}>
    <Container>{children}</Container>
    <Icon className="fa-times" onClick={removeOption} />
  </StyledSelectLabel>
);

export const Select = ({
  clearButton,
  disabled,
  multi,
  onChange,
  options,
  value,
  inputProps,
  ...otherProps
}: SelectProps): React.ReactElement => {
  const containerRef: React.RefObject<any> = React.useRef();
  const inputRef: React.RefObject<any> = React.useRef();
  const [iconTop, setIconTop] = React.useState(0);
  const [isMenuOpen, setIsMenuOpen] = React.useState(false);
  const [selectedOption, setSelectedOption] = React.useState<
    Option | undefined
  >(!multi && value ? (value as Option) : undefined);
  const [selectedOptions, setSelectedOptions] = React.useState<Option[]>(
    multi && value ? [...(value as Option[])] : []
  );
  const [searchValue, setSearchValue] = React.useState("");

  const onSearchValueChange = (
    e: React.ChangeEvent<HTMLInputElement>
  ): void => {
    setSearchValue(e.target.value);
  };

  const onFieldClick = (): void => {
    setIsMenuOpen(true);
    if (multi) {
      setSelectedOption({ label: " ", value: " " });
    }
  };

  const toggleMenu = (): void => {
    setIsMenuOpen(!isMenuOpen);
  };

  const clearMenu = (): void => {
    setSelectedOption(undefined);
    setSelectedOptions([]);
  };

  const selectOption = (option: Option) => (e: React.MouseEvent) => {
    if (multi) {
      const tempArr = [...selectedOptions];
      const index = tempArr.findIndex(op => op.value === option.value);
      if (index === -1) {
        tempArr.push(option);
        setSelectedOptions(tempArr);
        setSelectedOption({ label: " ", value: " " });
        setSearchValue("");

        if (onChange) {
          onChange(e, tempArr);
        }
      }
    } else {
      setSelectedOption(option);

      if (onChange) {
        onChange(e, option);
      }
    }
    setIsMenuOpen(false);
  };

  const removeOption = (index: number) => () => {
    const tempArr = [...selectedOptions];
    tempArr.splice(index, 1);
    setSelectedOptions(tempArr);

    if (multi && tempArr.length === 0) {
      setSelectedOption(undefined);
    }
  };

  React.useEffect(() => {
    if (!isNullOrUndefined(containerRef.current)) {
      window.addEventListener(
        "click",
        onOutsideClick(containerRef.current, setIsMenuOpen)
      );

      return function cleanup() {
        window.removeEventListener(
          "click",
          onOutsideClick(containerRef.current, setIsMenuOpen)
        );
      };
    }

    return undefined;
  }, [containerRef.current]);

  React.useEffect(() => {
    if (
      !isNullOrUndefined(inputRef.current) &&
      inputRef.current.offsetTop !== iconTop
    ) {
      setIconTop(inputRef.current.offsetTop);
    }
  }, [inputRef.current]);

  const filteredOptions = options.filter(
    option =>
      selectedOptions.findIndex(op => op.value === option.value) === -1 &&
      option.value.search(searchValue) !== -1
  );

  return (
    <StyledSelect
      disabled={disabled}
      ref={containerRef}
      isMenuOpen={isMenuOpen}
      {...otherProps}
    >
      <Field
        disabled={disabled}
        innerRef={inputRef}
        onClick={onFieldClick}
        onChange={onSearchValueChange}
        value={!isNullOrUndefined(selectedOption) ? selectedOption.value : ""}
        {...inputProps}
      />
      {clearButton &&
        (!isNullOrUndefined(selectedOption) || selectedOptions.length > 0) && (
          <CloseIcon onClick={clearMenu} iconTop={iconTop} />
        )}
      {multi && (
        <Container
          style={{
            display: "flex",
            flexWrap: "wrap",
            left: "1em",
            maxWidth: "calc(100% - 5.5rem)",
            position: "absolute",
            top: "0.5rem",
            width: "100%"
          }}
        >
          {selectedOptions.map(({ label }, index: number) => {
            const key = `${label}-${index}`;
            return (
              <SelectLabel key={key} removeOption={removeOption(index)}>
                {label}
              </SelectLabel>
            );
          })}
          <Input
            disabled={disabled}
            value={searchValue}
            onClick={onFieldClick}
            onChange={onSearchValueChange}
            style={{
              border: "none",
              boxShadow: "none",
              flex: "auto",
              minWidth: "1rem",
              height: "100%",
              padding: "0.5rem",
              zIndex: 0
            }}
          />
        </Container>
      )}
      <MenuIcon
        onClick={!disabled ? toggleMenu : undefined}
        iconTop={iconTop}
      />
      {isMenuOpen && (
        <SelectMenu
          node={containerRef.current}
          options={filteredOptions}
          selectOption={selectOption}
          selectedOption={selectedOption}
        />
      )}
    </StyledSelect>
  );
};

Select.defaultProps = {
  clearButton: true,
  multi: false
};

export default Select;
