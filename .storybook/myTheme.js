import { create } from "@storybook/theming/create";
import theme from "../src/components/AndradeTheme/theme.json";

export default create({
  base: "light",
  fontBase: theme.fonts.body,
  fontCode: theme.fonts.code,
  fontHeader: theme.fonts.header
})