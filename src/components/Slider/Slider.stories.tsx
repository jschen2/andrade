import { action } from "@storybook/addon-actions";
import { boolean, number, object, select } from "@storybook/addon-knobs";
import React from "react";

import { Color } from "../../types/enums";

import SliderReadme from "./README.md";
import Slider from ".";

export default {
  component: Slider,
  parameters: {
    info: {
      inline: true,
      text: SliderReadme
    }
  },
  title: "Slider"
};

export const BasicUse = (): React.ReactElement => (
  <Slider
    color={select("Color", Object.values(Color), Color.purple)}
    disabled={boolean("Disabled?", false)}
    value={object("Value", [28])}
    max={number("Max", 100)}
    min={number("Min", 0)}
    onChange={action("Changed")}
    range={boolean("Range?", false)}
  />
);

export const WithStep = (): React.ReactElement => (
  <Slider
    color={select("Color", Object.values(Color), Color.purple)}
    disabled={boolean("Disabled?", false)}
    value={object("Value", [30])}
    max={number("Max", 100)}
    min={number("Min", 0)}
    onChange={action("Changed")}
    range={boolean("Range?", false)}
    step={10}
  />
);

export const AsRange = (): React.ReactElement => (
  <Slider
    color={select("Color", Object.values(Color), Color.purple)}
    disabled={boolean("Disabled?", false)}
    value={object("Value", [28, 45])}
    range={boolean("Range?", true)}
    onChange={action("Changed")}
  />
);

export const AsRangeWithStep = (): React.ReactElement => (
  <Slider
    color={select("Color", Object.values(Color), Color.purple)}
    disabled={boolean("Disabled?", false)}
    value={object("Value", [30, 50])}
    max={number("Max", 100)}
    min={number("Min", 0)}
    onChange={action("Changed")}
    range={boolean("Range?", true)}
    step={10}
  />
);
