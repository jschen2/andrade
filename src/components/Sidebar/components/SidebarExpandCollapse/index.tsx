import React from "react";

import { FlexProps } from "../../../Flex";
import Icon from "../../../Icon";
import { SidebarContext, SidebarContextProps } from "../../../SidebarContext";
import StyledSidebarExpandCollapse from "./SidebarExpandCollapse.styles";

const SidebarExpandCollapse = (
  props: FlexProps & SidebarContextProps
): React.ReactElement => {
  const { expanded, setExpanded } = React.useContext(SidebarContext);
  const onClick = (): void => {
    if (setExpanded) {
      setExpanded(!expanded);
    }
  };
  return (
    <StyledSidebarExpandCollapse expanded={expanded} {...props}>
      <span onClick={onClick} onKeyDown={onClick} role="button" tabIndex={0}>
        <Icon className={`fa-angle-double-${expanded ? "left" : "right"}`} />
      </span>
      {expanded && <span>Collapse</span>}
    </StyledSidebarExpandCollapse>
  );
};

export default SidebarExpandCollapse;
