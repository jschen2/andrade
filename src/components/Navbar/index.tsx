import React from "react";

import { Alignment } from "../../types/enums";
import { FlexProps } from "../Flex";
import StyledNavbar from "./Navbar.styles";

export const Navbar = (props: FlexProps): React.ReactElement => (
  <StyledNavbar
    alignItems={Alignment.center}
    justifyContent={Alignment.spaceBetween}
    {...props}
  />
);

export default Navbar;
