import React from "react";

import BreadcrumbsReadme from "./README.md";
import Breadcrumbs, { Breadcrumb } from ".";

export default {
  component: Breadcrumbs,
  parameters: {
    info: {
      inline: true,
      text: BreadcrumbsReadme
    }
  },
  title: "Breadcrumbs"
};

export const BasicUse = (): React.ReactElement => {
  const [active, setActive] = React.useState(-1);
  return (
    <Breadcrumbs>
      <Breadcrumb active={active === 1} onClick={() => setActive(1)}>
        Home
      </Breadcrumb>
      <Breadcrumb active={active === 2} onClick={() => setActive(2)}>
        Second
      </Breadcrumb>
      <Breadcrumb active={active === 3} onClick={() => setActive(3)}>
        Third
      </Breadcrumb>
    </Breadcrumbs>
  );
};
