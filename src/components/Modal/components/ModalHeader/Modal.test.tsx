import { ReactWrapper, ShallowWrapper } from "enzyme";
import { mountComponent, shallowComponent } from "../../../../utils/testUtils";

import ModalHeader, { ModalHeaderProps } from ".";

describe("ModalHeader", () => {
  let props: ModalHeaderProps;
  let mountModalHeader: ReactWrapper | undefined;
  let shallowModalHeader: ShallowWrapper | undefined;

  const mountTestComponent = (): ReactWrapper => {
    if (!mountModalHeader) {
      mountModalHeader = mountComponent(ModalHeader, props);
    }

    return mountModalHeader;
  };

  const shallowTestComponent = (): ShallowWrapper => {
    if (!shallowModalHeader) {
      shallowModalHeader = shallowComponent(ModalHeader, props);
    }

    return shallowModalHeader;
  };

  beforeEach(() => {
    props = {
      title: "Modal",
      toggle: jest.fn()
    };

    mountModalHeader = undefined;
    shallowModalHeader = undefined;
  });

  it("should render correctly", () => {
    expect(shallowTestComponent()).toMatchSnapshot();
  });

  it("should mount correctly", () => {
    expect(mountTestComponent().find(ModalHeader)).toBeDefined();
  });
});
