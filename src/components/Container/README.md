Basic component to be used for wrapping other components and adding properties
that are meant to encompass all of its children.