module.exports = {
  env: {
    browser: true,
    es6: true,
    jest: true
  },
  extends: [
    'plugin:prettier/recommended',
    'plugin:react/recommended',
    'plugin:import/typescript',
    'plugin:@typescript-eslint/recommended',
    'airbnb'
  ],
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
  },
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 2018,
    sourceType: 'module',
  },
  plugins: [
    'react',
    '@typescript-eslint',
    'react-hooks',
    'prettier'
  ],
  rules: {
    "@typescript-eslint/explicit-function-return-type": [2, { allowExpressions: true }],
    "@typescript-eslint/no-explicit-any": "off",
    "arrow-parens": [2, "as-needed"],
    "comma-dangle": "off",
    "function-paren-newline": "off",
    "indent": "off",
    "implicit-arrow-linebreak": "off",
    "import/extensions": "off",
    "import/named": "off",
    "import/no-extraneous-dependencies": ["error", {"devDependencies": true, "optionalDependencies": true, "peerDependencies": true}],
    "import/no-named-as-default": "off",
    "no-nested-ternary": "off",
    "no-plusplus": "off",
    "no-unexpected-multiline": "off",
    "import/order": ["error", {"groups": ["builtin", "external", "internal", "parent", "sibling", "index"]}],
    "react/jsx-boolean-value": [2, "always"],
    "react/jsx-curly-newline": "off",
    "react/jsx-filename-extension": [1, { "extensions": [".jsx", ".tsx"] }],
    "react/jsx-props-no-spreading": "off",
    "react/sort-comp": [2, {
      order: [
        'static-methods',
        'everything-else',
        'lifecycle',
        'render'
      ]
    }],
    "react/state-in-constructor": "off",
    "object-curly-newline": "off",
    "operator-linebreak": "off",
    "no-confusing-arrow": "off",
    "quotes": [2, "double", { "avoidEscape": true }],
  },
  settings: {
  "import/resolver": {
    "node": {
      "extensions": [".js", ".jsx", ".ts", ".tsx"]
    }
  }
}
};
