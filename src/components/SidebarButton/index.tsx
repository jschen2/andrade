import uniqueId from "lodash/uniqueId";
import React from "react";
import { Placement } from "../../types/enums";
import Flex from "../Flex";
import { PopoverButton } from "../Popover/Popover.styles";
import Container from "../Container";
import StyledSidebarButton, {
  SidebarButtonPopover,
  SidebarButtonProps,
  SidebarButtonToggle,
  SidebarSubButtonGroup
} from "./SidebarButton.styles";

const SidebarButton = ({
  children,
  expanded,
  id,
  subButtons,
  ...props
}: SidebarButtonProps): React.ReactElement => {
  const buttonId = React.useRef(id || uniqueId("sidebar-button-"));
  const buttonRef: React.RefObject<any> = React.useRef();
  const [showChildren, setShowChildren] = React.useState(false);
  const [isOpen, setIsOpen] = React.useState(false);

  const onArrowClick = (e: React.MouseEvent): void => {
    e.preventDefault();
    e.stopPropagation();
    setShowChildren(prevShowChildren => !prevShowChildren);
  };

  const onMouseOver = (): void => {
    setIsOpen(true);
  };

  const onMouseOut = (e: React.MouseEvent | React.FocusEvent): void => {
    if (
      buttonRef.current &&
      !(buttonRef.current as Element).contains(e.relatedTarget as Element)
    ) {
      setIsOpen(false);
    }
  };

  const separatedChildren = React.Children.toArray(children);
  const iconIndex = separatedChildren.findIndex(child => {
    if (React.isValidElement(child)) {
      return (child.type as any).displayName === "Icon";
    }

    return false;
  });

  let icon;
  if (iconIndex !== -1) {
    icon = separatedChildren.splice(iconIndex, 1);
  }

  return (
    <>
      <Flex ref={buttonRef} id={buttonId.current}>
        <StyledSidebarButton
          onMouseOver={onMouseOver}
          onFocus={onMouseOver}
          onMouseOut={onMouseOut}
          onBlur={onMouseOut}
          {...props}
        >
          <Flex>
            {icon &&
              React.cloneElement(icon[0] as any, {
                noPadding: true
              })}
            {expanded && separatedChildren}
          </Flex>
          {subButtons && expanded && (
            <SidebarButtonToggle
              showChildren={showChildren}
              onClick={onArrowClick}
            />
          )}
        </StyledSidebarButton>
        {!expanded && (
          <SidebarButtonPopover
            className={`${buttonId.current}-popover`}
            isOpen={isOpen}
            node={buttonRef.current}
            offsetLeft={0}
            onMouseOut={onMouseOut}
            onBlur={onMouseOut}
            placement={Placement.rightStart}
            showArrow={false}
            target={buttonId.current as string}
            style={{ position: "fixed", zIndex: 1 }}
          >
            <PopoverButton className={`${buttonId.current}-popover-button`}>
              {separatedChildren}
            </PopoverButton>
            {subButtons &&
              subButtons.map(({ children: btnChildren, ...button }, idx) => {
                const subId = uniqueId("sub-button-");
                const key = `${subId}-${idx}`;
                return (
                  <PopoverButton id={subId} key={key} {...button}>
                    {btnChildren}
                  </PopoverButton>
                );
              })}
          </SidebarButtonPopover>
        )}
      </Flex>
      {expanded && subButtons && showChildren && (
        <SidebarSubButtonGroup>
          <Container>
            {subButtons.map((button, idx) => {
              const subId = uniqueId("sub-button-");
              const key = `${subId}-${idx}`;
              return (
                <Flex id={subId} key={key}>
                  <SidebarButton expanded={expanded} {...button} />
                </Flex>
              );
            })}
          </Container>
        </SidebarSubButtonGroup>
      )}
    </>
  );
};

export default SidebarButton;
