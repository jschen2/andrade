import styled from "styled-components";
import { Color } from "../../../types/enums";
import { getColor, transition } from "../../../utils";

const StyledSVGGridlines = styled.g`
  line {
    stroke: ${getColor(Color.gray, 300)};
    shape-rendering: crispEdges;
    ${transition("all")};
  }

  path {
    stroke-width: 0;
  }
`;

export default StyledSVGGridlines;
