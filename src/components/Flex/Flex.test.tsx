import { ReactWrapper, ShallowWrapper } from "enzyme";
import { mountComponent, shallowComponent } from "../../utils/testUtils";
import { Alignment } from "../../types/enums";

import Flex, { FlexProps } from ".";

describe("Flex", () => {
  let props: FlexProps;
  let mountFlex: ReactWrapper | undefined;
  let shallowFlex: ShallowWrapper | undefined;

  const mountTestComponent = (): ReactWrapper => {
    if (!mountFlex) {
      mountFlex = mountComponent(Flex, props);
    }

    return mountFlex;
  };

  const shallowTestComponent = (): ShallowWrapper => {
    if (!shallowFlex) {
      shallowFlex = shallowComponent(Flex, props);
    }

    return shallowFlex;
  };

  beforeEach(() => {
    props = {
      alignItems: Alignment.center,
      justifyContent: Alignment.center
    };

    mountFlex = undefined;
    shallowFlex = undefined;
  });

  it("should render correctly", () => {
    expect(shallowTestComponent()).toMatchSnapshot();
  });

  it("should mount correctly", () => {
    expect(mountTestComponent().find(Flex)).toBeDefined();
  });
});
