import { ReactWrapper, ShallowWrapper } from "enzyme";
import { mountComponent, shallowComponent } from "../../utils/testUtils";

import { SidebarProps } from "./Sidebar.styles";
import Sidebar from ".";

describe("Sidebar", () => {
  let props: SidebarProps;
  let mountSidebar: ReactWrapper | undefined;
  let shallowSidebar: ShallowWrapper | undefined;

  const mountTestComponent = (): ReactWrapper => {
    if (!mountSidebar) {
      mountSidebar = mountComponent(Sidebar, props);
    }

    return mountSidebar;
  };

  const shallowTestComponent = (): ShallowWrapper => {
    if (!shallowSidebar) {
      shallowSidebar = shallowComponent(Sidebar, props);
    }

    return shallowSidebar;
  };

  beforeEach(() => {
    props = {};

    mountSidebar = undefined;
    shallowSidebar = undefined;
  });

  it("should render correctly", () => {
    expect(shallowTestComponent()).toMatchSnapshot();
  });

  it("should mount correctly", () => {
    expect(mountTestComponent().find(Sidebar)).toBeDefined();
  });
});
