import React from "react";

import { FlexProps } from "../Flex";
import Portal from "../Portal";

import StyledModal, { ModalOverlay } from "./Modal.styles";

export interface ModalProps extends FlexProps {
  dimBackground?: boolean;
  isOpen: boolean;
  toggle(): void;
}

export const Modal = ({
  dimBackground,
  isOpen,
  toggle,
  ...props
}: ModalProps): React.ReactElement | null => {
  if (isOpen) {
    return (
      <Portal>
        {dimBackground && <ModalOverlay onClick={toggle} />}
        <StyledModal {...props} />
      </Portal>
    );
  }

  return null;
};

Modal.defaultProps = {
  dimBackground: true,
  isOpen: false
};

export default Modal;
