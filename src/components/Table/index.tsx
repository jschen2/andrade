import cloneDeep from "lodash/cloneDeep";
import debounce from "lodash/debounce";
import React from "react";
import { Alignment } from "../../types/enums";

import { chunk, getDocument } from "../../utils";
import Checkbox from "../Checkbox";
import Container from "../Container";
import Flex from "../Flex";
import Icon from "../Icon";
import Select from "../Select";
import { Option } from "../Select/SelectMenu";
import TableBody from "./components/TableBody";
import { TableActions, tableReducer } from "./Table.reducer";

import {
  PageButton,
  StyledTable,
  TableCell,
  TableDragImage,
  TableDragLine,
  TableHeader,
  TableHeaderCell,
  TableHeaderGrip,
  TableHeaderRow,
  TableProps,
  TableRow
} from "./Table.styles";
import ColumnReorder from "./components/ColumnReorder";

const pageSizeOptions = ["10", "25", "50", "75", "100"];

export const Table = ({
  columns,
  data,
  dragSelect,
  id,
  multiSelect,
  onRowsSelected,
  pageSize,
  pagination,
  showHideColumns
}: TableProps): React.ReactElement => {
  const tableRef: React.RefObject<any> = React.useRef();
  const dragImageRef: React.RefObject<any> = React.useRef();
  const dragLineRef: React.RefObject<any> = React.useRef();
  const [
    {
      active,
      flexBasis,
      pageNum,
      replacedColumn,
      selectedPageSize,
      selectedRows,
      sortingKey,
      startingLeft,
      tableColumns
    },
    dispatch
  ] = React.useReducer(tableReducer, {
    active: -1,
    flexBasis: new Array(columns.length).fill(undefined),
    pageNum: 0,
    replacedColumn: -1,
    selectedPageSize: pageSize || Number(pageSizeOptions[0]),
    selectedRows: [],
    sortingKey: { accessor: "", direction: "" },
    startingLeft: 0,
    tableColumns: columns.map(column => ({
      hidden: false,
      column
    }))
  });
  const tableData = React.useMemo(
    () => data.map((d, idx) => ({ id: idx, original: d })),
    [columns, data]
  );

  React.useEffect(() => {
    dispatch({
      type: TableActions.SetTableColumns,
      payload: columns.map(column => ({
        hidden: false,
        column
      }))
    });
  }, [columns]);

  React.useEffect(() => {
    if (tableRef.current) {
      dispatch({
        type: TableActions.SetFlexBasis,
        payload: flexBasis.map(() =>
          Math.floor(
            (tableRef.current.getBoundingClientRect().width -
              32 -
              (multiSelect ? 32 : 0)) /
              columns.length
          )
        )
      });
    }
  }, [tableRef.current, multiSelect]);

  const buildPageNumbers = (): string[] => {
    const numPages = Math.ceil(tableData.length / selectedPageSize);
    if (numPages <= 5) {
      return new Array(numPages)
        .fill(undefined)
        .map((val, idx) => String(idx + 1));
    }
    if (pageNum + 1 >= 5 && pageNum + 1 < numPages - 4) {
      return [
        "1",
        "...",
        String(pageNum),
        String(pageNum + 1),
        String(pageNum + 2),
        "...",
        String(numPages)
      ];
    }
    if (pageNum + 1 >= numPages - 4) {
      return [
        "1",
        "...",
        String(numPages - 4),
        String(numPages - 3),
        String(numPages - 2),
        String(numPages - 1),
        String(numPages)
      ];
    }
    return ["1", "2", "3", "4", "5", "...", String(numPages)];
  };

  const onMouseMove = debounce((e: React.MouseEvent | MouseEvent): void => {
    e.preventDefault();
    e.stopPropagation();
    const index = Number(tableRef.current.getAttribute("data-index"));
    if (index !== -1) {
      const element = (tableRef.current as HTMLElement).querySelector(
        `[data-index="${index + (multiSelect ? 1 : 0)}"]`
      );
      if (element) {
        const { left, width } = element.getBoundingClientRect();
        const newLeft = e.clientX;

        const newFlexBasis = [...flexBasis];
        const adding = newLeft - left - width;
        const newMin = width + adding;

        if (newMin >= 0) {
          newFlexBasis[index] = newMin;
        }

        dispatch({
          type: TableActions.SetFlexBasis,
          payload: newFlexBasis
        });
      }
    }
  }, 10);

  const onMouseUp = (): void => {
    dispatch({
      type: TableActions.SetActive,
      payload: -1
    });
    const doc = getDocument(tableRef.current);
    doc.removeEventListener("mousemove", onMouseMove);
    doc.removeEventListener("mouseup", onMouseUp);
  };

  const onMouseDown = (idx: number) => (e: React.MouseEvent): void => {
    e.preventDefault();
    e.stopPropagation();
    dispatch({
      type: TableActions.SetActive,
      payload: idx
    });
    const doc = getDocument(tableRef.current);
    doc.addEventListener("mousemove", onMouseMove);
    doc.addEventListener("mouseup", onMouseUp);
  };

  const sortColumns = (accessor: string) => () => {
    let payload = {
      accessor,
      direction: "ASC"
    };
    if (sortingKey.accessor === accessor) {
      if (sortingKey.direction === "ASC") {
        payload = {
          accessor,
          direction: "DESC"
        };
      } else if (sortingKey.direction === "DESC") {
        payload = {
          accessor: "",
          direction: ""
        };
      }
    }
    dispatch({
      type: TableActions.SetSortingKey,
      payload
    });
  };

  const selectRows = (selected: string | number) => (e: React.MouseEvent) => {
    e.stopPropagation();
    let newRows = [...selectedRows];
    if (selected === "ALL") {
      if (selectedRows.length === data.length) {
        newRows = [];
      } else {
        newRows = data.map((d, idx) => idx);
      }
    } else if (newRows.includes(Number(selected))) {
      const index = newRows.findIndex(row => row === selected);
      newRows.splice(index, 1);
    } else {
      newRows.push(Number(selected));
    }
    dispatch({
      type: TableActions.SetSelectedRows,
      payload: newRows
    });
    if (onRowsSelected) {
      onRowsSelected(newRows);
    }
  };

  const onDragStart = (str: string) => (e: React.DragEvent) => {
    const img = document.createElement("img");
    img.src =
      "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7";
    e.currentTarget.classList.add("dragging");
    e.dataTransfer.setDragImage(img, -5000, -5000);
    dispatch({
      type: TableActions.SetStartingLeft,
      payload: e.clientX
    });
    const { left, top } = tableRef.current.getBoundingClientRect();
    dragImageRef.current.innerHTML = str;
    dragImageRef.current.style.left = `${e.clientX - left + 8}px`;
    dragImageRef.current.style.top = `${e.clientY - top + 8}px`;
    dragImageRef.current.style.opacity = 1;
    dragLineRef.current.style.opacity = 1;
  };

  const onDrag = (e: React.DragEvent): void => {
    e.persist();
    e.stopPropagation();

    const { left, top } = e.currentTarget.getBoundingClientRect();

    dragImageRef.current.style.left = `${e.clientX - left - 16}px`;
    dragImageRef.current.style.top = `${e.clientY - top - 16}px`;
  };

  const onDragEnter = (idx: number) => (e: React.DragEvent) => {
    dispatch({
      type: TableActions.SetReplacedColumn,
      payload: idx
    });
    const { left, width } = e.currentTarget.getBoundingClientRect();
    const isGreater = startingLeft < e.clientX;
    dragLineRef.current.style.left = `${left +
      (isGreater ? width : 0) -
      tableRef.current.getBoundingClientRect().left -
      8}px`;
  };

  const onDragLeave = (e: React.DragEvent): void => {
    if ((e.target as HTMLElement).tagName === "th") {
      dispatch({
        type: TableActions.SetReplacedColumn,
        payload: -1
      });
    }
  };

  const onDragEnd = (idx: number) => (e: React.DragEvent): void => {
    e.persist();
    e.preventDefault();
    e.currentTarget.classList.remove("dragging");

    if (idx !== -1 && replacedColumn !== -1) {
      const newTableColumns = [...tableColumns];
      const tempColumn = cloneDeep(newTableColumns[idx]);
      newTableColumns.splice(idx, 1);
      newTableColumns.splice(replacedColumn, 0, tempColumn);
      dispatch({
        type: TableActions.SetTableColumns,
        payload: newTableColumns
      });
    }
    dragImageRef.current.style.left = "";
    dragImageRef.current.style.top = "";
    dragImageRef.current.style.opacity = 0;
    dragLineRef.current.style.opacity = 0;
  };

  const showHideColumn = (idx: number) => () => {
    const newTableColumns = [...tableColumns];
    newTableColumns[idx].hidden = !newTableColumns[idx].hidden;
    dispatch({
      type: TableActions.SetTableColumns,
      payload: newTableColumns
    });
  };

  return (
    <Container style={{ height: "100%", overflow: "hidden" }}>
      <TableDragLine ref={dragLineRef} />
      <TableDragImage ref={dragImageRef} />
      {showHideColumns && (
        <ColumnReorder
          columns={tableColumns}
          id={`${id}-column-filter`}
          setTableColumns={(newColumns: any[]) =>
            dispatch({
              type: TableActions.SetTableColumns,
              payload: newColumns
            })
          }
          showHideColumn={showHideColumn}
        />
      )}
      <StyledTable ref={tableRef} data-index={active} onDragOver={onDrag}>
        <TableHeader>
          <TableHeaderRow>
            {multiSelect && (
              <TableHeaderCell style={{ minWidth: "2rem" }}>
                <Checkbox
                  checked={selectedRows.length === data.length}
                  indeterminate={selectedRows.length > 0}
                  onClick={selectRows("ALL")}
                />
              </TableHeaderCell>
            )}
            {tableColumns.map(({ column, hidden }, idx) => {
              if (!hidden) {
                const key = `${column.header}-${idx}`;
                return (
                  <TableHeaderCell
                    draggable={true}
                    key={key}
                    style={{ width: `${flexBasis[idx]}px` }}
                    onClick={sortColumns(column.accessor)}
                    onDragEnter={onDragEnter(idx)}
                    onDragLeave={onDragLeave}
                    onDragStart={onDragStart(column.header)}
                    onDragEnd={onDragEnd(idx)}
                    type={column.type}
                    data-index={idx}
                  >
                    <span>
                      {column.header}
                      {sortingKey.accessor === column.accessor && (
                        <Icon
                          className={`fa-sort-${
                            sortingKey.direction === "ASC" ? "up" : "down"
                          }`}
                        />
                      )}
                    </span>
                    <span
                      onMouseDown={onMouseDown(idx)}
                      role="button"
                      tabIndex={0}
                    >
                      <TableHeaderGrip className="fa-grip-lines-vertical" />
                    </span>
                  </TableHeaderCell>
                );
              }

              return null;
            })}
          </TableHeaderRow>
        </TableHeader>
        <TableBody
          dragSelect={dragSelect}
          selectedRows={selectedRows}
          setSelectedRows={(newRows: number[]) =>
            dispatch({
              type: TableActions.SetSelectedRows,
              payload: newRows || []
            })
          }
        >
          {chunk(
            [...tableData].sort((a, b) => {
              const { accessor, direction } = sortingKey;
              const isAsc = direction === "ASC";
              if (direction !== "") {
                if (a.original[accessor] < b.original[accessor]) {
                  return isAsc ? -1 : 1;
                }

                if (a.original[accessor] > b.original[accessor]) {
                  return isAsc ? 1 : -1;
                }
              }

              return 0;
            }),
            (pagination ? selectedPageSize : tableData.length) as number
          )[pageNum].map((cells: any, idx: number) => {
            const key = `table-row-${idx}`;
            return (
              <TableRow
                key={key}
                rowId={cells.id}
                selected={selectedRows.includes(cells.id)}
              >
                {multiSelect && (
                  <TableCell style={{ minWidth: "2rem" }}>
                    <Checkbox
                      checked={selectedRows.includes(cells.id)}
                      onClick={selectRows(cells.id)}
                    />
                  </TableCell>
                )}
                {tableColumns.map(({ column, hidden }, cIdx) => {
                  if (!hidden) {
                    const cKey = `${column.accessor}-${idx}-${cIdx}`;
                    return (
                      <TableCell
                        key={cKey}
                        style={{
                          width: `calc(${flexBasis[cIdx]}px - 1.75rem)`
                        }}
                        type={column.type}
                      >
                        {cells.original[column.accessor]}
                      </TableCell>
                    );
                  }

                  return null;
                })}
              </TableRow>
            );
          })}
        </TableBody>
      </StyledTable>
      {pagination && (
        <Flex alignItems={Alignment.center} justifyContent={Alignment.flexEnd}>
          <Select
            clearButton={false}
            id="page-size-select"
            onChange={(e, option: Option) => {
              const num = Number(option.value);
              const numPages = tableData.length / num - 1;
              if (pageNum > numPages) {
                dispatch({
                  type: TableActions.SetPageNum,
                  payload: numPages
                });
              }
              dispatch({
                type: TableActions.SetSelectedPageSize,
                payload: num
              });
            }}
            options={pageSizeOptions.map(size => ({
              label: size,
              value: size
            }))}
            value={{
              label: String(selectedPageSize),
              value: String(selectedPageSize)
            }}
            style={{ width: "max-content" }}
          />
          <PageButton
            role="button"
            tabIndex={0}
            onClick={() => {
              if (pageNum !== 0) {
                dispatch({
                  type: TableActions.SetPageNum,
                  payload: pageNum - 1
                });
              }
            }}
          >
            <Icon className="fa-chevron-double-left" />
          </PageButton>
          <div>
            {buildPageNumbers().map((val, idx) => {
              const key = `pagination-button-${val}`;
              if (val === "...") {
                const ellipsisKey = `non-button-ellipsis-${idx}`;
                return <PageButton key={ellipsisKey}>{val}</PageButton>;
              }
              return (
                <PageButton
                  active={pageNum + 1 === Number(val)}
                  key={key}
                  role="button"
                  onClick={() =>
                    dispatch({
                      type: TableActions.SetPageNum,
                      payload: Number(val) - 1
                    })
                  }
                  onKeyDown={() =>
                    dispatch({
                      type: TableActions.SetPageNum,
                      payload: Number(val) - 1
                    })
                  }
                  tabIndex={0}
                >
                  {val}
                </PageButton>
              );
            })}
          </div>
          <PageButton
            role="button"
            tabIndex={0}
            onClick={() => {
              if (pageNum !== tableData.length / selectedPageSize - 1) {
                dispatch({
                  type: TableActions.SetPageNum,
                  payload: pageNum + 1
                });
              }
            }}
          >
            <Icon className="fa-chevron-double-right" />
          </PageButton>
        </Flex>
      )}
    </Container>
  );
};

Table.defaultProps = {
  dragSelect: false,
  id: "table",
  pagination: false,
  pageSize: 10,
  showHideColumns: false
};

export default Table;
