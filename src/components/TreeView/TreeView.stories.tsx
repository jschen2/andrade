import { object } from "@storybook/addon-knobs";
import React from "react";
import Container from "../Container";
import Icon from "../Icon";

import TreeViewReadme from "./README.md";
import TreeView from ".";

const tree = [
  {
    actions: [<Icon key="icon-1" className="fa-plus" />],
    onClick: () => console.log("Clicked"),
    title: "First child",
    children: [
      {
        title: "First Sub Child",
        onClick: () => console.log("Clicked")
      },
      {
        title: "Second Sub Child",
        onClick: () => console.log("Clicked"),
        children: [
          {
            title: "First Sub Sub Child"
          }
        ]
      }
    ]
  },
  {
    title: "Second Child",
    children: [
      {
        title: "First Sub Child"
      },
      {
        title: "Second Sub Child"
      }
    ]
  }
];

export default {
  component: TreeView,
  parameters: {
    info: {
      inline: true,
      text: TreeViewReadme
    }
  },
  title: "TreeView"
};

export const BasicUse = (): React.ReactElement => (
  <Container style={{ width: "300px" }}>
    <TreeView tree={object("Tree", tree)} />
  </Container>
);
