import { ReactWrapper, ShallowWrapper } from "enzyme";
import { mountComponent, shallowComponent } from "../../../utils/testUtils";
import StyledChartTooltip from "./ChartTooltip.styles";
import ChartTooltip, { ChartTooltipProps } from "./index";

describe("ChartTooltip", () => {
  let props: ChartTooltipProps;
  let mountChartTooltip: ReactWrapper | undefined;
  let shallowChartTooltip: ShallowWrapper | undefined;

  const shallowTestComponent = (): ShallowWrapper => {
    if (!shallowChartTooltip) {
      shallowChartTooltip = shallowComponent(ChartTooltip, props);
    }
    return shallowChartTooltip;
  };

  const mountTestComponent = (): ReactWrapper => {
    if (!mountChartTooltip) {
      mountChartTooltip = mountComponent(ChartTooltip, props);
    }
    return mountChartTooltip;
  };

  beforeEach(() => {
    props = {
      children: "Child",
      isOpen: true,
      target: ""
    };
    shallowChartTooltip = undefined;
    mountChartTooltip = undefined;
  });

  it("should render correctly", () => {
    expect(shallowTestComponent()).toMatchSnapshot();
  });

  it("should mount correctly", () => {
    mountTestComponent();
    expect(mountTestComponent().find(StyledChartTooltip).length).toBe(1);
  });
});
