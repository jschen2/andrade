import { ReactWrapper, ShallowWrapper } from "enzyme";
import { mountComponent, shallowComponent } from "../../utils/testUtils";

import Avatar, { AvatarProps } from ".";

describe("Avatar", () => {
  let props: AvatarProps;
  let mountAvatar: ReactWrapper | undefined;
  let shallowAvatar: ShallowWrapper | undefined;

  const mountTestComponent = (): ReactWrapper => {
    if (!mountAvatar) {
      mountAvatar = mountComponent(Avatar, props);
    }

    return mountAvatar;
  };

  const shallowTestComponent = (): ShallowWrapper => {
    if (!shallowAvatar) {
      shallowAvatar = shallowComponent(Avatar, props);
    }

    return shallowAvatar;
  };

  beforeEach(() => {
    props = {
      src: "#"
    };

    mountAvatar = undefined;
    shallowAvatar = undefined;
  });

  it("should render correctly", () => {
    expect(shallowTestComponent()).toMatchSnapshot();
  });

  it("should mount correctly", () => {
    expect(mountTestComponent().find(Avatar)).toBeDefined();
  });
});
