import { ReactWrapper, ShallowWrapper } from "enzyme";
import { mountComponent, shallowComponent } from "../../utils/testUtils";

import TreeView, { TreeViewProps } from ".";

describe("TreeView", () => {
  let props: TreeViewProps;
  let mountTreeView: ReactWrapper | undefined;
  let shallowTreeView: ShallowWrapper | undefined;

  const mountTestComponent = (): ReactWrapper => {
    if (!mountTreeView) {
      mountTreeView = mountComponent(TreeView, props);
    }

    return mountTreeView;
  };

  const shallowTestComponent = (): ShallowWrapper => {
    if (!shallowTreeView) {
      shallowTreeView = shallowComponent(TreeView, props);
    }

    return shallowTreeView;
  };

  beforeEach(() => {
    props = {
      tree: [
        {
          title: "Child",
          onClick: jest.fn(),
          children: [
            {
              title: "Sub child",
              children: [
                {
                  title: "Sub sub child"
                }
              ]
            }
          ]
        }
      ]
    };

    mountTreeView = undefined;
    shallowTreeView = undefined;
  });

  it("should render correctly", () => {
    expect(shallowTestComponent()).toMatchSnapshot();
  });

  it("should mount correctly", () => {
    expect(mountTestComponent().find(TreeView)).toBeDefined();
  });

  describe("if `tree` only has one child", () => {
    beforeEach(() => {
      props.tree = [
        {
          title: "Child",
          children: [null]
        }
      ];
    });

    it("should render correctly", () => {
      expect(shallowTestComponent()).toMatchSnapshot();
    });

    it("should mount correctly", () => {
      expect(mountTestComponent().find(TreeView)).toBeDefined();
    });
  });
});
