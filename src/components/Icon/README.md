A styled wrapper for `FontAwesome` icons. All that is needed is the icon
classname. If the style is desired to be changed, use the `iconSetClassName`.