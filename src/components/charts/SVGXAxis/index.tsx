import { axisBottom } from "d3-axis";
import { select } from "d3-selection";
import React from "react";
import { isNullOrUndefined } from "../../../utils";
import StyledSVGXAxis from "./SVGXAxis.styles";

export interface XAxisProps {
  xScale: any;
  yAxisZero?: number;
}

const SVGXAxis = ({
  xScale,
  yAxisZero,
  ...props
}: XAxisProps): React.ReactElement => {
  const axisRef: React.RefObject<any> = React.useRef();

  React.useEffect(() => {
    if (!isNullOrUndefined(axisRef.current)) {
      select(axisRef.current).call(g =>
        g.call(
          axisBottom(xScale)
            .ticks(xScale.range()[1] / 70)
            .tickSizeOuter(0)
        )
      );
    }
  }, [axisRef.current, xScale, yAxisZero]);

  return (
    <StyledSVGXAxis
      ref={axisRef}
      transform={`translate(0, ${yAxisZero})`}
      {...props}
    />
  );
};

export default SVGXAxis;
