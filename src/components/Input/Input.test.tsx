import { ReactWrapper, ShallowWrapper } from "enzyme";
import { mountComponent, shallowComponent } from "../../utils/testUtils";

import Input, { InputProps } from ".";

describe("Input", () => {
  let props: InputProps;
  let mountInput: ReactWrapper | undefined;
  let shallowInput: ShallowWrapper | undefined;

  const mountTestComponent = (): ReactWrapper => {
    if (!mountInput) {
      mountInput = mountComponent(Input, props);
    }

    return mountInput;
  };

  const shallowTestComponent = (): ShallowWrapper => {
    if (!shallowInput) {
      shallowInput = shallowComponent(Input, props);
    }

    return shallowInput;
  };

  beforeEach(() => {
    props = {
      value: undefined,
      invalid: false
    };

    mountInput = undefined;
    shallowInput = undefined;
  });

  it("should render correctly", () => {
    expect(shallowTestComponent()).toMatchSnapshot();
  });

  it("should render 'input' element", () => {
    expect(mountTestComponent().find("input")).toBeDefined();
  });

  describe("if 'invalid' is true", () => {
    beforeEach(() => {
      props.invalid = true;
    });

    it("should render invalid correctly", () => {
      expect(shallowTestComponent()).toMatchSnapshot();
    });

    it("should render invalid 'input' element", () => {
      expect(
        mountTestComponent()
          .find(Input)
          .props().invalid
      ).toBe(true);
    });
  });
});
