import { action } from "@storybook/addon-actions";
import { boolean, text } from "@storybook/addon-knobs";
import React from "react";

import FieldReadme from "./README.md";
import Field from ".";

export default {
  component: Field,
  parameters: {
    info: {
      inline: true,
      text: FieldReadme
    }
  },
  title: "Field"
};

export const BasicUse = (): React.ReactElement => {
  const [value, setValue] = React.useState("");
  const onFieldChange = (e: React.ChangeEvent<HTMLInputElement>): void => {
    setValue(e.target.value);
    action("Value")(e.target.value, e);
  };
  return (
    <Field
      disabled={boolean("Disabled?", false)}
      label={text("Label", "Name")}
      id={text("ID", "Name")}
      invalid={boolean("Invalid?", false)}
      value={value}
      onChange={onFieldChange}
    >
      {text("Help text", "Add text here to help the user")}
    </Field>
  );
};
