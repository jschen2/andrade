import React from "react";

import Container from "../../../Container";
import StyledListItem from "../../List.styles";

export interface ListItemProps extends React.HTMLAttributes<HTMLLIElement> {
  /** Content to be displayed in item */
  children?: React.ReactNode;
  /** Title to be used for item */
  title?: string;
}

const ListItem = ({
  children,
  title,
  ...props
}: ListItemProps): React.ReactElement => {
  const newChildren: React.ReactNode[] = React.Children.toArray(children);
  const index = newChildren.findIndex(child => {
    if (React.isValidElement(child)) {
      return (child.type as any).displayName === "Avatar";
    }

    return false;
  });
  let image;
  if (index !== -1) {
    image = newChildren.splice(index, 1);
  }

  return (
    <StyledListItem {...props}>
      {image}
      <Container>
        {title && <div className="title">{title}</div>}
        <div className="content">{newChildren}</div>
      </Container>
    </StyledListItem>
  );
};

export default ListItem;
