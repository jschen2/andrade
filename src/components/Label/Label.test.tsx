import { ReactWrapper, ShallowWrapper } from "enzyme";
import React from "react";
import {
  mountComponent,
  mountWithTheme,
  shallowComponent,
  shallowWithTheme
} from "../../utils/testUtils";
import { Color } from "../../types/enums";

import CloseButton from "../CloseButton";
import Label, { LabelProps } from ".";

describe("Label", () => {
  let props: LabelProps;
  let mountLabel: ReactWrapper | undefined;
  let shallowLabel: ShallowWrapper | undefined;

  const mountTestComponent = (): ReactWrapper => {
    if (!mountLabel) {
      mountLabel = mountComponent(Label, props);
    }

    return mountLabel;
  };

  const shallowTestComponent = (): ShallowWrapper => {
    if (!shallowLabel) {
      shallowLabel = shallowComponent(Label, props);
    }

    return shallowLabel;
  };

  beforeEach(() => {
    props = {
      color: Color.blue,
      onClose: jest.fn()
    };

    mountLabel = undefined;
    shallowLabel = undefined;
  });

  it("should render correctly", () => {
    expect(shallowTestComponent()).toMatchSnapshot();
  });

  it("should mount correctly", () => {
    expect(mountTestComponent().find(Label)).toBeDefined();
  });

  describe("if Label has children", () => {
    it("should render correctly", () => {
      const local = shallowWithTheme(
        <Label {...props}>
          <Label.Icon className="fa-user" />
          Label
          <Label.Section>Sub-Label</Label.Section>
          <CloseButton />
        </Label>
      );
      expect(local).toMatchSnapshot();
    });

    it("should mount correctly", () => {
      const local = mountWithTheme(
        <Label {...props}>
          <Label.Icon className="fa-user" />
          Label
          <Label.Section>Sub-Label</Label.Section>
          <CloseButton />
        </Label>
      );
      expect(local.find(Label)).toBeDefined();
    });
  });

  describe("if Label has children and `color` is null", () => {
    it("should render correctly", () => {
      const local = shallowWithTheme(
        <Label color={null as any}>
          <Label.Icon className="fa-user" />
          Label
          <Label.Section>Sub-Label</Label.Section>
          <CloseButton />
        </Label>
      );
      expect(local).toMatchSnapshot();
    });

    it("should mount correctly", () => {
      const local = mountWithTheme(
        <Label color={null as any}>
          <Label.Icon className="fa-user" />
          Label
          <Label.Section>Sub-Label</Label.Section>
          <CloseButton />
        </Label>
      );
      expect(local.find(Label)).toBeDefined();
    });
  });
});
