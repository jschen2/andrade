import { select } from "@storybook/addon-knobs";
import React from "react";
import { TabPosition } from "../../types/enums";
import { getEnumValues } from "../../utils";

import TabsReadme from "./README.md";
import Tabs from ".";

export default {
  component: Tabs,
  parameters: {
    info: {
      inline: true,
      text: TabsReadme
    }
  },
  title: "Tabs"
};

export const BasicUse = (): React.ReactElement => {
  const [active, setActive] = React.useState(1);
  const onClick = (idx: number) => () => setActive(idx);
  return (
    <Tabs
      tabPosition={
        select(
          "Tab Position",
          getEnumValues(TabPosition),
          TabPosition.top
        ) as TabPosition
      }
    >
      <Tabs.Tab active={active === 1} onClick={onClick(1)}>
        Tab 1
      </Tabs.Tab>
      <Tabs.Tab active={active === 2} onClick={onClick(2)}>
        Tab 2
      </Tabs.Tab>
      <Tabs.Tab active={active === 3} onClick={onClick(3)}>
        Tab 3
      </Tabs.Tab>
      <Tabs.Content>{active !== -1 && `Tab ${active}`}</Tabs.Content>
    </Tabs>
  );
};
