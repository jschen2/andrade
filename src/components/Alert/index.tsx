import React from "react";
import { Alignment, Color, Status } from "../../types/enums";
import { FlexProps } from "../Flex";
import Icon from "../Icon";

import StyledAlert from "./Alert.styles";

export interface AlertProps extends Omit<FlexProps, "color"> {
  type?: Status;
}

const getAlertProps = (
  type?: Status
): {
  className: string;
  color: Color;
} => {
  switch (type) {
    case Status.error:
      return {
        className: "fa-do-not-enter",
        color: Color.red
      };
    case Status.info:
      return {
        className: "fa-exclamation-circle",
        color: Color.blue
      };
    case Status.warning:
      return {
        className: "fa-radiation-alt",
        color: Color.yellow
      };
    case Status.success:
      return {
        className: "fa-check-circle",
        color: Color.green
      };
    default:
      return {
        className: "",
        color: Color.gray
      };
  }
};

export const Alert = ({
  children,
  type,
  ...props
}: AlertProps): React.ReactElement => {
  const { className, color } = getAlertProps(type);
  return (
    <StyledAlert
      alignItems={Alignment.center}
      color={color}
      justifyContent={Alignment.flexStart}
      {...props}
    >
      {type && <Icon className={className} size={2} />}
      <div>{children}</div>
    </StyledAlert>
  );
};

export default Alert;
