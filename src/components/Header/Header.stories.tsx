import { select } from "@storybook/addon-knobs";
import React from "react";

import HeaderReadme from "./README.md";
import Header from ".";

export default {
  component: Header,
  parameters: {
    info: {
      inline: true,
      text: HeaderReadme
    }
  },
  title: "Header"
};

export const BasicUse = (): React.ReactElement => (
  <Header size={select("Size", [1, 2, 3, 4, 5, 6], 1)}>
    {`Header ${select("Size", [1, 2, 3, 4, 5, 6], 1)}`}
  </Header>
);
