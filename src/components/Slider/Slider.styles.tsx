import styled, { css } from "styled-components";
import { Color } from "../../types/enums";
import { borderRadius, getColor, getShadow, hexToRgb } from "../../utils";
import Container from "../Container";
import Popover from "../Popover";

export const StyledSlider = styled(Container)<{ disabled?: boolean }>`
  background-color: ${getColor(Color.gray, 150)};
  ${borderRadius};
  height: 0.5rem;
  position: relative;
  width: 100%;

  ${props =>
    props.disabled &&
    css`
      cursor: not-allowed;
      opacity: 0.5;
    `};
`;

export const Rail = styled(Container)<{
  color: Color;
  left: number;
  value: number;
}>`
  background-color: ${props => getColor(props.color, 500)};
  ${borderRadius};
  left: ${props => props.left}%;
  height: 0.5rem;
  position: absolute;
  width: ${props => props.value}%;
`;

export const Handle = styled(Container)<{
  active: boolean;
  color: Color;
  disabled?: boolean;
}>`
  background-color: ${props => getColor(props.color, 500)};
  border-radius: 100%;
  ${getShadow(0)};
  height: 1rem;
  position: absolute;
  top: -0.25rem;
  width: 1rem;
  z-index: 2;

  ${props =>
    props.disabled &&
    css`
      cursor: not-allowed;
    `};

  ${props =>
    props.active &&
    css`
      &:after {
        content: "";
        background-color: rgba(
          ${hexToRgb(getColor(props.color, 500)(props))},
          0.35
        );
        border-radius: 100%;
        height: calc(100% + 0.5rem);
        left: -0.25rem;
        position: absolute;
        top: -0.25rem;
        width: calc(100% + 0.5rem);
        z-index: -1;
      }
    `};
`;

export const SliderPopover = styled(Popover)<{ color: Color }>`
  background-color: ${props => getColor(props.color, 500)};
  color: ${getColor(Color.gray, 0)};
  font-weight: bold;
`;

export const Step = styled(Container)<{
  color: Color;
  left: number;
  onRail: boolean;
}>`
  ${props =>
    props.onRail &&
    css`
      background-color: ${getColor(Color.gray, 0)};
    `};
  ${props =>
    !props.onRail &&
    css`
      background-color: ${getColor(props.color, 500)};
    `};
  height: 100%;
  left: ${props => props.left}%;
  position: absolute;
  width: 1px;
`;
