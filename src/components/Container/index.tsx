import React from "react";
import styled from "styled-components";

export type ContainerProps = React.HTMLAttributes<HTMLDivElement>;

export const Container = styled.div<ContainerProps>`
  color: inherit;
  padding: 0;
  position: relative;
  width: 100%;
`;

Container.displayName = "Container";

export default Container;
