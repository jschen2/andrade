import { boolean } from "@storybook/addon-knobs";
import React from "react";

import SwitchReadme from "./README.md";
import Switch from ".";

export default {
  component: Switch,
  parameters: {
    info: {
      inline: true,
      text: SwitchReadme
    }
  },
  title: "Switch"
};

export const BasicUse = (): React.ReactElement => {
  const [checked, setChecked] = React.useState(false);

  const onClick = (): void => setChecked(!checked);

  return (
    <Switch
      checked={checked}
      disabled={boolean("Disabled?", false)}
      onClick={onClick}
    />
  );
};
