import React from "react";
import styled from "styled-components";
import Flex, { FlexProps } from "../../../Flex";
import { Alignment } from "../../../../types/enums";
import Header from "../../../Header";
import { CloseButton } from "../../../Modal/components/ModalHeader/ModalHeader.styles";

export interface DrawerHeaderProps extends FlexProps {
  toggle?(): void;
}

export const StyledDrawerHeader = styled(Flex)`
  flex: 0 0 38px;
`;

const DrawerHeader = ({
  children,
  toggle,
  ...props
}: DrawerHeaderProps): React.ReactElement => (
  <StyledDrawerHeader
    alignItems={Alignment.center}
    justifyContent={Alignment.spaceBetween}
    {...props}
  >
    <Header size={3}>{children}</Header>
    {toggle && <CloseButton onClick={toggle} />}
  </StyledDrawerHeader>
);

export default DrawerHeader;
