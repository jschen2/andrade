import { select } from "@storybook/addon-knobs";
import React from "react";
import { ButtonSize, Color } from "../../types/enums";
import { getEnumValues } from "../../utils";

import IndicatorReadme from "./README.md";
import Indicator from ".";

export default {
  component: Indicator,
  parameters: {
    info: {
      inline: true,
      text: IndicatorReadme
    }
  },
  title: "Indicator"
};

export const BasicUse = (): React.ReactElement => (
  <Indicator
    color={select("Color", getEnumValues(Color), Color.purple) as Color}
    intensity={select(
      "Color Intensity",
      [100, 200, 300, 400, 500, 600, 700, 800, 900],
      500
    )}
    size={
      select("Size", getEnumValues(ButtonSize), ButtonSize.md) as ButtonSize
    }
  />
);
