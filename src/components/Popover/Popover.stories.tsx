import { boolean, select } from "@storybook/addon-knobs";
import React from "react";

import { Color, Placement } from "../../types/enums";
import Button from "../Button";
import PopoverReadme from "./README.md";
import Popover from ".";

export default {
  component: Popover,
  parameters: {
    info: {
      inline: true,
      text: PopoverReadme
    }
  },
  title: "Popover"
};

export const BasicUse = (): React.ReactElement => {
  const [isOpen, setIsOpen] = React.useState(false);

  const onClick = (): void => {
    setIsOpen(!isOpen);
  };

  return (
    <div>
      <Button color={Color.blue} id="popover-target" onClick={onClick}>
        Click me
      </Button>
      <Popover
        isOpen={isOpen}
        target="popover-target"
        setToTargetWidth={boolean("Set to target width", true)}
        placement={select("Placement", Object.values(Placement), Placement.top)}
      >
        Hello!
      </Popover>
    </div>
  );
};
