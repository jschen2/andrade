import { ReactWrapper, ShallowWrapper } from "enzyme";
import { act } from "react-dom/test-utils";

import { mountComponent, shallowComponent } from "../../utils/testUtils";
import Container from "../Container";
import Field from "../Field";
import Icon from "../Icon";
import Input from "../Input";
import Popover from "../Popover";
import StyledPopover from "../Popover/Popover.styles";
import { CloseIcon, MenuIcon } from "./Select.styles";
import SelectMenu, { MenuOption } from "./SelectMenu";
import Select, { SelectLabel, SelectProps } from ".";

describe("Select", () => {
  let props: SelectProps;
  let mountSelect: ReactWrapper | undefined;
  let shallowSelect: ShallowWrapper | undefined;

  const mountTestComponent = (): ReactWrapper => {
    if (!mountSelect) {
      mountSelect = mountComponent(Select, props);
    }

    return mountSelect;
  };

  const shallowTestComponent = (): ShallowWrapper => {
    if (!shallowSelect) {
      shallowSelect = shallowComponent(Select, props);
    }

    return shallowSelect;
  };

  beforeEach(() => {
    props = {
      options: [
        {
          label: "Option 1",
          value: "Option 1"
        },
        {
          label: "Option 2",
          value: "Option 2"
        }
      ]
    };

    mountSelect = undefined;
    shallowSelect = undefined;
  });

  it("should render correctly", () => {
    expect(shallowTestComponent()).toMatchSnapshot();
  });

  it("should mount correctly", () => {
    expect(mountTestComponent().find(Select)).toBeDefined();
  });

  it("should open menu on 'MenuIcon' click", () => {
    act(() => {
      mountTestComponent()
        .find(Select)
        .find(MenuIcon)
        .props()
        .onClick();
    });
    mountTestComponent().update();
    expect(document.body.querySelector("#portal")).toBeDefined();
  });

  it("should select option", () => {
    act(() => {
      (mountTestComponent().find(Field) as any).props().onClick();
    });

    mountTestComponent().update();

    expect(
      mountTestComponent()
        .find(Select)
        .find(Popover)
    ).toBeDefined();

    mountTestComponent().update();

    act(() => {
      mountTestComponent()
        .find(Select)
        .find(SelectMenu)
        .find(StyledPopover)
        .find(MenuOption)
        .at(0)
        .props()
        .onClick();
    });

    mountTestComponent().update();

    expect(
      mountTestComponent()
        .find(Select)
        .find(CloseIcon).length
    ).toBe(1);

    act(() => {
      mountTestComponent()
        .find(Select)
        .find(CloseIcon)
        .props()
        .onClick();
    });

    mountTestComponent().update();

    expect(
      mountTestComponent()
        .find(Select)
        .find(CloseIcon).length
    ).toBe(0);
  });

  describe("if 'multi' is true", () => {
    beforeEach(() => {
      props.multi = true;
    });

    it("should render correctly", () => {
      expect(shallowTestComponent()).toMatchSnapshot();
    });

    it("should mount correctly with 'Container'", () => {
      expect(
        mountTestComponent()
          .find(Select)
          .find(Container).length
      ).toBe(1);
    });

    it("should search for 'MenuOption' on 'Input' change", () => {
      act(() => {
        mountTestComponent()
          .find(Select)
          .find(Input)
          .at(1)
          .props()
          .onClick();
      });

      mountTestComponent().update();

      act(() => {
        mountTestComponent()
          .find(Select)
          .find(Input)
          .at(1)
          .props()
          .onChange({
            target: {
              value: "2"
            }
          });
      });

      mountTestComponent().update();

      expect(mountTestComponent().find(MenuOption).length).toBe(1);

      act(() => {
        mountTestComponent()
          .find(Select)
          .find(Input)
          .at(1)
          .props()
          .onChange({
            target: {
              value: "Kla"
            }
          });
      });

      mountTestComponent().update();

      expect(mountTestComponent().find(MenuOption).length).toBe(0);
    });

    it("should select MenuOption and render 'SelectLabel'", () => {
      act(() => {
        (mountTestComponent().find(Field) as any).props().onClick();
      });

      mountTestComponent().update();

      expect(
        mountTestComponent()
          .find(Select)
          .find(Popover)
      ).toBeDefined();

      mountTestComponent().update();

      act(() => {
        mountTestComponent()
          .find(Select)
          .find(MenuOption)
          .at(0)
          .props()
          .onClick();
      });

      mountTestComponent().update();

      expect(
        mountTestComponent()
          .find(Select)
          .find(SelectLabel).length
      ).toBe(1);

      act(() => {
        (mountTestComponent()
          .find(Select)
          .find(SelectLabel) as any)
          .find(Icon)
          .props()
          .onClick();
      });

      mountTestComponent().update();

      expect(
        mountTestComponent()
          .find(Select)
          .find(SelectLabel).length
      ).toBe(0);
    });
  });
});
