import styled from "styled-components";
import { Color } from "../../types/enums";
import { borderRadius, getColor, getShadow, hexToRgb } from "../../utils";
import Flex from "../Flex";
import { StyledIcon } from "../Icon";

const StyledAlert = styled(Flex)<{ color: Color }>`
  ${borderRadius};
  ${getShadow(0)};
  border: 2px solid
    rgba(${props => hexToRgb(getColor(props.color, 500)(props))}, 0.6);
  overflow: hidden;
  position: relative;

  &:after {
    left: initial;
    right: 0;
  }

  & > ${StyledIcon} {
    color: rgba(${props => hexToRgb(getColor(props.color, 500)(props))}, 0.6);
    height: auto;
    line-height: 1.375;
    padding: 0.5rem;
    width: auto;
  }
`;

export default StyledAlert;
