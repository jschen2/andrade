import styled, { css } from "styled-components";
import Flex from "../../../Flex";
import { getColor, getShadow } from "../../../../utils";
import { Color } from "../../../../types/enums";

const StyledSidebarExpandCollapse = styled(Flex)<{ expanded?: boolean }>`
  align-items: center;
  background-color: ${getColor(Color.gray, 125)};
  border-radius: 0.25rem 0.25rem 0 0;
  flex: 0 0 38px;
  ${getShadow(0)};
  justify-content: space-between;
  text-align: center;

  span {
    ${props =>
      props.expanded &&
      css`
        &:first-child {
          border-right: 1px solid ${getColor(Color.gray, 150)};
          flex: 0 0 46px;

          &:hover {
            background-color: ${getColor(Color.gray, 150)};
          }
        }
        &:last-child {
          flex: 1 1 75%;
          overflow: hidden;
          white-space: nowrap;
        }
      `};
    ${props =>
      !props.expanded &&
      css`
        flex: 0 0 46px;
      `};
  }
`;

export default StyledSidebarExpandCollapse;
