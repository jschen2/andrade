import { ReactWrapper, ShallowWrapper } from "enzyme";
import React from "react";
import { act } from "react-dom/test-utils";
import { mountWithTheme, shallowWithTheme } from "../../utils/testUtils";
import StyledPopover, { PopoverButton } from "../Popover/Popover.styles";

import StyledNavbarButton, {
  NavbarButtonProps,
  NavbarDropdownContent
} from "./NavbarButton.styles";
import NavbarButton from ".";

describe("NavbarButton", () => {
  let props: NavbarButtonProps;
  let mountNavbarButton: ReactWrapper | undefined;
  let shallowNavbarButton: ShallowWrapper | undefined;

  const mountTestComponent = (): ReactWrapper => {
    if (!mountNavbarButton) {
      mountNavbarButton = mountWithTheme(
        <NavbarButton {...props}>
          Button
          <NavbarDropdownContent>
            <PopoverButton>Child Button</PopoverButton>
          </NavbarDropdownContent>
          {null}
        </NavbarButton>
      );
    }

    return mountNavbarButton;
  };

  const shallowTestComponent = (): ShallowWrapper => {
    if (!shallowNavbarButton) {
      shallowNavbarButton = shallowWithTheme(
        <NavbarButton {...props}>
          Button
          <NavbarDropdownContent>
            <PopoverButton>Child Button</PopoverButton>
          </NavbarDropdownContent>
          {null}
        </NavbarButton>
      );
    }

    return shallowNavbarButton;
  };

  beforeEach(() => {
    props = {
      id: "navbar-button"
    };

    mountNavbarButton = undefined;
    shallowNavbarButton = undefined;
  });

  it("should render correctly", () => {
    expect(shallowTestComponent()).toMatchSnapshot();
  });

  it("should mount correctly", () => {
    expect(mountTestComponent().find(NavbarButton)).toBeDefined();
  });

  it("should open Popover", () => {
    act(() => {
      mountTestComponent()
        .find(StyledNavbarButton)
        .props()
        .onMouseOver();
    });
    mountTestComponent().update();
    expect(
      mountTestComponent()
        .find(NavbarButton)
        .find(StyledPopover)
    ).toBeDefined();
    act(() => {
      mountTestComponent()
        .find(StyledNavbarButton)
        .props()
        .onMouseOut({
          relatedTarget: null
        });
    });
    mountTestComponent().update();
    expect(
      mountTestComponent()
        .find(NavbarButton)
        .find(StyledPopover).length
    ).toBe(0);
  });

  describe("if `id` is undefined", () => {
    beforeEach(() => {
      props.id = undefined;
    });

    it("should render correctly", () => {
      expect(shallowTestComponent()).toMatchSnapshot();
    });

    it("should mount correctly", () => {
      expect(mountTestComponent().find(NavbarButton)).toBeDefined();
    });

    it("should open Popover", () => {
      act(() => {
        mountTestComponent()
          .find(StyledNavbarButton)
          .props()
          .onMouseOver();
      });
      mountTestComponent().update();
      expect(
        mountTestComponent()
          .find(NavbarButton)
          .find(StyledPopover)
      ).toBeDefined();
      act(() => {
        mountTestComponent()
          .find(StyledNavbarButton)
          .props()
          .onMouseOut({
            relatedTarget: mountTestComponent()
              .find(NavbarButton)
              .getDOMNode()
          });
      });
      mountTestComponent().update();
      expect(
        mountTestComponent()
          .find(NavbarButton)
          .find(StyledPopover).length
      ).toBe(1);
    });
  });
});
