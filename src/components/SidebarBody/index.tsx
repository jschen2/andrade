import uniqueId from "lodash/uniqueId";
import React from "react";
import { SidebarContext } from "../SidebarContext";
import { FlexProps } from "../Flex";
import StyledSidebarBody from "./SidebarBody.styles";

export type SidebarBodyProps = FlexProps;

const SidebarBody = ({
  children,
  ...props
}: SidebarBodyProps): React.ReactElement => {
  const { expanded, setExpanded } = React.useContext(SidebarContext);
  return (
    <StyledSidebarBody {...props}>
      {React.Children.toArray(children).map(child => {
        if (React.isValidElement(child)) {
          return React.cloneElement(child, {
            id: child.props.id || uniqueId("sidebar-button-"),
            expanded,
            setExpanded
          });
        }

        return child;
      })}
    </StyledSidebarBody>
  );
};

export default SidebarBody;
