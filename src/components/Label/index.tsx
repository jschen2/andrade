import React from "react";
import styled, { css } from "styled-components";
import { Color } from "../../types/enums";
import { borderRadius, getColor, getShadow, hexToRgb } from "../../utils";
import CloseButton from "../CloseButton";
import Flex, { FlexProps } from "../Flex";
import Icon, { IconProps } from "../Icon";

export interface LabelProps extends FlexProps {
  color: Color;
  onClose?(e: React.MouseEvent): void;
}

export const LabelIcon = styled(props => <Icon noPadding={true} {...props} />)<
  IconProps
>`
  border-right: 1px solid;
  margin-left: 0.25rem;
  padding-right: 0.25rem;

  &:last-child {
    border-right: none;
    margin-right: 0;
  }
`;

LabelIcon.displayName = "LabelIcon";

export const LabelSection = styled.span<{ color?: Color }>`
  padding: 0.4rem 0.25rem;
  margin: -0.25rem;
  ${props =>
    props.color &&
    css`
      background-color: rgba(
        ${hexToRgb(getColor(props.color, 600)(props))},
        0.32
      );
      color: ${getColor(props.color, 700)};
    `};
  ${props =>
    !props.color &&
    css`
      background-color: rgba(
        ${hexToRgb(getColor(Color.gray, 600)(props))},
        0.32
      );
      color: ${getColor(Color.gray, 700)};
    `};
`;

LabelSection.displayName = "LabelSection";

export const StyledLabel = styled(Flex)<Omit<LabelProps, "onClose">>`
  align-items: center;
  ${borderRadius};
  font-size: 0.75rem;
  font-weight: bold;
  ${getShadow(0)};
  line-height: 1;
  width: max-content;

  ${CloseButton} {
    border-left: 1px solid;
    height: 1.25rem;
    width: 1.25rem;
  }

  ${props =>
    props.color &&
    css`
      background-color: rgba(
        ${hexToRgb(getColor(props.color, 500)(props))},
        0.32
      );
      border: 1px solid ${getColor(props.color, 500)};
      color: ${getColor(props.color, 700)};

      & > span {
        color: ${getColor(props.color, 700)};
        display: flex;
        padding: 0.25rem;
      }
    `};
  ${props =>
    !props.color &&
    css`
      background-color: rgba(
        ${hexToRgb(getColor(Color.gray, 500)(props))},
        0.32
      );
      border: 1px solid ${getColor(Color.gray, 500)};
      color: ${getColor(Color.gray, 700)};

      & > span {
        color: ${getColor(Color.gray, 700)};
        display: flex;
        padding: 0.25rem;
      }
    `};
`;

export const Label = ({
  children,
  color,
  onClose,
  ...props
}: LabelProps): React.ReactElement => (
  <StyledLabel color={color} {...props}>
    {React.Children.toArray(children).map((child, index) => {
      const key = `label-child-${index}`;
      if (React.isValidElement(child)) {
        if (
          (child.type as React.ComponentType).displayName === "LabelSection"
        ) {
          return React.cloneElement(child, {
            color
          });
        }
        if (
          (child.type as React.ComponentType).displayName === "LabelIcon" ||
          (child.type as React.ComponentType).displayName === "CloseButton"
        ) {
          return child;
        }
      }

      return <span key={key}>{child}</span>;
    })}
    {onClose && <CloseButton onClick={onClose} />}
  </StyledLabel>
);

Label.Icon = LabelIcon;
Label.Section = LabelSection;

Label.defaultProps = {
  color: Color.purple
};

export default Label;
