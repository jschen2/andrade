import React from "react";

import { boolean } from "@storybook/addon-knobs";
import Button from "../Button";
import { Alignment, Color } from "../../types/enums";
import ModalHeader from "./components/ModalHeader";
import ModalReadme from "./README.md";
import { ModalBody, ModalFooter } from "./Modal.styles";
import Modal from ".";

export default {
  component: Modal,
  parameters: {
    info: {
      inline: true,
      text: ModalReadme
    }
  },
  title: "Modal"
};

export const BasicUse = (): React.ReactElement => {
  const [isOpen, setIsOpen] = React.useState(false);
  const toggleModal = (): void => setIsOpen(!isOpen);
  return (
    <>
      <Button color={Color.purple} onClick={toggleModal}>
        Click me
      </Button>
      <Modal
        dimBackground={boolean("Dim background?", true)}
        isOpen={isOpen}
        toggle={toggleModal}
      >
        <ModalHeader title="Modal" toggle={toggleModal} />
        <ModalBody>Hello</ModalBody>
        <ModalFooter justifyContent={Alignment.flexEnd}>
          <Button outline={true} color={Color.blue} onClick={toggleModal}>
            Cancel
          </Button>
          <Button color={Color.green}>Save</Button>
        </ModalFooter>
      </Modal>
    </>
  );
};
