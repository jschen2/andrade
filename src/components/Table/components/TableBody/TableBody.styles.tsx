import styled from "styled-components";
import { Color } from "../../../../types/enums";
import { borderRadius, getColor, hexToRgb } from "../../../../utils";
import Container from "../../../Container";
import { sharedTableStyles } from "../../Table.styles";

export const DragSquare = styled(Container)`
  background-color: rgba(
    ${props => hexToRgb(getColor(Color.blue, 500)(props))},
    0.32
  );
  border: 2px solid ${getColor(Color.blue, 500)};
  left: 0;
  height: 0;
  position: fixed;
  top: 0;
  width: 0;
`;

const StyledTableBody = styled.tbody`
  ${sharedTableStyles};
  display: block;
  ${borderRadius};
  height: calc(100% - 4.25rem);
  overflow: auto;
  padding: 0;
  position: relative;
  width: 100%;
`;

export default StyledTableBody;
