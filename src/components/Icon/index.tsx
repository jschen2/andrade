import styled from "styled-components";
import React from "react";

import { combineClasses } from "../../utils";

import "../../fonts/css/fontawesome.min.css";
import "../../fonts/css/regular.min.css";

export interface IconProps extends React.HTMLAttributes<HTMLElement> {
  /** Change FontAwesome icon style */
  iconSetClassName?: string;
  /** Remove padding from around icon */
  noPadding?: boolean;
  /** Multiplier to change icon size */
  size?: 0.5 | 1 | 2 | 3 | 4 | 5;
}

export const StyledIcon = styled(({ noPadding, size, ...props }) => (
  <i {...props} />
))<IconProps>`
  align-items: center;
  color: inherit;
  display: inline-flex;
  font-family: "Font Awesome 5 Pro";
  font-size: ${props => props.size}rem;
  height: ${props => props.size + (props.noPadding ? 0 : 1)}rem;
  justify-content: center;
  padding: 0;
  width: ${props => props.size + (props.noPadding ? 0 : 1)}rem;
`;

export const Icon = ({
  className,
  iconSetClassName,
  ...props
}: IconProps): React.ReactElement => (
  <StyledIcon
    className={combineClasses(iconSetClassName, className)}
    {...props}
  />
);

Icon.displayName = "Icon";

Icon.defaultProps = {
  iconSetClassName: "far",
  noPadding: false,
  size: 1
};

export default Icon;
