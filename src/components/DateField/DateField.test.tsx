import { ReactWrapper, ShallowWrapper } from "enzyme";
import moment from "moment";
import { act } from "react-dom/test-utils";

import Field from "../Field";
import Popover from "../Popover";
import { mountComponent, shallowComponent } from "../../utils/testUtils";
import CalendarControls from "./components/CalendarControls";
import { CalendarDay } from "./DateField.styles";
import Calendar from "./components/Calendar";
import DateField, { DateFieldProps } from ".";

describe("DateField", () => {
  let props: DateFieldProps;
  let shallowDateField: ShallowWrapper | undefined;
  let mountDateField: ReactWrapper | undefined;

  const mountTestComponent = (): ReactWrapper => {
    if (!mountDateField) {
      mountDateField = mountComponent(DateField, props);
    }

    return mountDateField;
  };

  const shallowTestComponent = (): ShallowWrapper => {
    if (!shallowDateField) {
      shallowDateField = shallowComponent(DateField, props);
    }

    return shallowDateField;
  };

  beforeEach(() => {
    props = {
      label: "Date",
      value: moment("2020 02 10", "YYYY MM DD"),
      onChange: jest.fn(),
      onClick: jest.fn()
    };

    shallowDateField = undefined;
    mountDateField = undefined;
  });

  it("should render correctly", () => {
    expect(shallowTestComponent()).toMatchSnapshot();
  });

  it("should render 'Field' component", () => {
    expect(mountTestComponent().find(Field).length).toBe(1);
  });

  it("should render 'Popover' component on focus", () => {
    mountTestComponent()
      .find(Field)
      .simulate("focus");
    expect(mountTestComponent().find(Popover).length).toBe(1);
  });

  it("should set new date by Field text change", () => {
    act(() => {
      (mountTestComponent().find(Field) as any).props().onFocus({});
    });
    mountTestComponent().update();
    act(() => {
      (mountTestComponent().find(Field) as any).props().onChange({
        target: {
          value: "02/11/2020"
        }
      });
    });
    mountTestComponent().update();
    act(() => {
      (mountTestComponent().find(Field) as any).props().onKeyDown({
        keyCode: 13,
        target: {
          value: "02/11/2020"
        }
      } as any);
    });
    mountTestComponent().update();
    expect(
      mountTestComponent()
        .find(Field)
        .props().value
    ).toBe("02/11/2020");
  });

  it("should select day + 7 in 'Popover'", () => {
    act(() => {
      (mountTestComponent()
        .find(DateField)
        .find(Field) as any)
        .props()
        .onFocus({});
    });
    mountTestComponent().update();
    act(() => {
      mountTestComponent()
        .find(DateField)
        .find(Popover)
        .find(Calendar)
        .find(CalendarDay)
        .at(17)
        .props()
        .onClick({});
    });
    expect(props.onClick).toHaveBeenCalled();
  });

  it("should change year in 'Popover' + 1", () => {
    act(() => {
      (mountTestComponent()
        .find(DateField)
        .find(Field) as any)
        .props()
        .onFocus({});
    });
    mountTestComponent().update();
    act(() => {
      mountTestComponent()
        .find(DateField)
        .find(Popover)
        .find(CalendarControls)
        .at(0)
        .props()
        .add();
    });
    mountTestComponent().update();
    expect(
      mountTestComponent()
        .find(DateField)
        .find(Field)
        .props().value
    ).toBe("02/10/2021");
  });

  it("should click outside", () => {
    act(() => {
      (mountTestComponent()
        .find(DateField)
        .find(Field) as any)
        .props()
        .onFocus({});
    });
    mountTestComponent().update();
    act(() => {
      document.body.focus();
      document.body.dispatchEvent(new Event("click", { bubbles: true }));
    });
    mountTestComponent().update();
    expect(props.onChange).not.toHaveBeenCalled();
    mountTestComponent().unmount();
  });

  describe("if 'id' is defined", () => {
    beforeEach(() => {
      props.id = "test-id";
      props.onClick = undefined;
    });

    it("should render correctly", () => {
      expect(shallowTestComponent()).toMatchSnapshot();
    });

    it("should render 'Field' component", () => {
      expect(mountTestComponent().find(Field).length).toBe(1);
    });

    it("should select day - 7 in 'Popover'", () => {
      act(() => {
        (mountTestComponent()
          .find(DateField)
          .find(Field) as any)
          .props()
          .onFocus({});
      });
      mountTestComponent().update();
      act(() => {
        mountTestComponent()
          .find(DateField)
          .find(Popover)
          .find(Calendar)
          .find(CalendarDay)
          .at(10)
          .props()
          .onClick({});
      });
      expect(props.onClick).toBeUndefined();
    });
  });

  describe("if 'value' is undefined", () => {
    beforeEach(() => {
      props.value = undefined;
    });

    it("should render correctly", () => {
      expect(shallowTestComponent()).toMatchSnapshot();
    });

    it("should render 'Field' component", () => {
      expect(mountTestComponent().find(Field).length).toBe(1);
    });

    it("should ignore onChange", () => {
      act(() => {
        (mountTestComponent()
          .find(DateField)
          .find(Field) as any)
          .props()
          .onFocus({});
      });
      mountTestComponent().update();
      act(() => {
        (mountTestComponent()
          .find(DateField)
          .find(Field)
          .props() as any).onKeyDown({
          keyCode: 13,
          target: {
            value: ""
          }
        } as any);
      });
      expect(props.onChange).not.toHaveBeenCalled();
    });
  });

  describe("if `onChange` is undefined", () => {
    beforeEach(() => {
      props.onChange = undefined;
    });

    it("should ignore onChange", () => {
      act(() => {
        (mountTestComponent()
          .find(DateField)
          .find(Field) as any)
          .props()
          .onFocus({});
      });
      mountTestComponent().update();
      act(() => {
        (mountTestComponent()
          .find(DateField)
          .find(Field)
          .props() as any).onKeyDown({
          keyCode: 13,
          target: {
            value: "-"
          }
        } as any);
      });
      expect(props.onChange).toBeUndefined();
    });
  });
});
