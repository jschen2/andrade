import styled from "styled-components";

import { PopoverButton } from "../../../Popover/Popover.styles";
import { getColor, getShadow, hexToRgb, transition } from "../../../../utils";
import { Color } from "../../../../types/enums";

export const ColumnDragImage = styled(PopoverButton)`
  border: 1px solid
    rgba(${props => hexToRgb(getColor(Color.gray, 900)(props))}, 0.32);
  border-bottom: 0.25rem solid ${getColor(Color.blue, 500)};
  ${getShadow(1)};
  left: -100%;
  pointer-events: none;
  position: fixed;
  top: -100%;
  width: max-content;
`;

export const ColumnDragLine = styled.div`
  background-color: ${getColor(Color.blue, 500)};
  ${getShadow(1)};
  height: 0.25rem;
  opacity: 0;
  padding: 0;
  position: absolute;
  left: 0.5rem;
  ${transition("opacity top")};
  width: calc(100% - 1rem);
`;
