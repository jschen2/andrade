import React from "react";
import styled, {
  css,
  FlattenInterpolation,
  ThemeProps
} from "styled-components";
import { borderRadius, getColor, getShadow, Props } from "../../utils";
import { Color } from "../../types/enums";

export interface InputProps extends React.HTMLAttributes<HTMLInputElement> {
  /** Set disabled input state */
  disabled?: boolean;
  /** Set invalid input state */
  invalid?: boolean;
  /** Input value */
  value?: string;
}

export const applyFieldTheme = (
  // eslint-disable-next-line no-unused-vars, @typescript-eslint/no-unused-vars
  props: Props & {
    disabled?: boolean;
    invalid?: boolean;
  }
): FlattenInterpolation<ThemeProps<any>> => css`
  ${borderRadius};
  background: ${getColor(Color.gray, 125)};
  border: none;
  ${getShadow(0)};

  ${props.disabled &&
    css`
      cursor: not-allowed;
      opacity: 0.5;
      pointer-events: all;
    `};

  ${props.invalid &&
    css`
      border: 2px solid ${getColor(Color.red, 200)};
    `};
`;

export const Input = styled.input<InputProps>`
  ${applyFieldTheme};
`;

Input.defaultProps = {
  invalid: false,
  type: "text"
};

Input.displayName = "Input";

export default Input;
