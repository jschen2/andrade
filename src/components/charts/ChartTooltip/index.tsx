import React from "react";
import { Placement } from "../../../types/enums";
import { PopoverProps } from "../../Popover";
import StyledChartTooltip, { MouseTarget } from "./ChartTooltip.styles";

export type ChartTooltipProps = PopoverProps;

export const ChartTooltip = ({
  target,
  ...props
}: ChartTooltipProps): React.ReactElement => {
  const [{ left, top }, setTooltipLocation] = React.useState({
    left: 0,
    top: 0
  });
  const moveEvent = (e: MouseEvent | TouchEvent): void => {
    const { clientX, clientY, touches } = e as any;
    const x = clientX || touches[0].clientX;
    const y = clientY || touches[0].clientY;
    setTooltipLocation({
      left: x,
      top: y - 24
    });
  };

  React.useEffect(() => {
    window.addEventListener("mousemove", moveEvent);
    window.addEventListener("touchmove", moveEvent);

    return function cleanup() {
      window.removeEventListener("mousemove", moveEvent);
      window.removeEventListener("touchmove", moveEvent);
    };
  }, []);

  return (
    <>
      <MouseTarget
        id={target}
        style={{
          transform: `translate(${left}px, ${top}px)`
        }}
      />
      <StyledChartTooltip
        target={target}
        placement={Placement.top}
        {...props}
      />
    </>
  );
};

export default ChartTooltip;
