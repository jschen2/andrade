import React from "react";
import { Position } from "../../types/enums";
import { ModalOverlay } from "../Modal/Modal.styles";
import Portal from "../Portal";
import { DrawerProps, StyledDrawer } from "./Drawer.styles";

export const Drawer = ({
  dimBackground,
  isOpen,
  toggle,
  ...props
}: DrawerProps): React.ReactElement | null => (
  <Portal>
    {isOpen && dimBackground && <ModalOverlay onClick={toggle} />}
    <StyledDrawer isOpen={isOpen} {...props} />
  </Portal>
);

Drawer.defaultProps = {
  dimBackground: true,
  isOpen: false,
  position: Position.right
};

export default Drawer;
