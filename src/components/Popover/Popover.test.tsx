import { ReactWrapper, ShallowWrapper } from "enzyme";

import { mountComponent, shallowComponent } from "../../utils/testUtils";
import { Placement } from "../../types/enums";

import { StyledPopover } from "./Popover.styles";
import Popover, { PopoverProps } from ".";

describe("Popover", () => {
  let props: PopoverProps;
  let mountPopover: ReactWrapper | undefined;
  let shallowPopover: ShallowWrapper | undefined;

  const mountTestComponent = (): ReactWrapper => {
    if (!mountPopover) {
      mountPopover = mountComponent(Popover, props);
    }

    return mountPopover;
  };

  const shallowTestComponent = (): ShallowWrapper => {
    if (!shallowPopover) {
      shallowPopover = shallowComponent(Popover, props);
    }

    return shallowPopover;
  };

  beforeEach(() => {
    props = {
      isOpen: true,
      placement: Placement.bottom,
      showArrow: true,
      target: "popover-target"
    };

    Element.prototype.getBoundingClientRect = () => ({
      height: 0,
      width: 0,
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
      x: 0,
      y: 0,
      toJSON: () => ({})
    });

    const target = document.createElement("div");
    target.id = props.target;
    document.body.appendChild(target);

    mountPopover = undefined;
    shallowPopover = undefined;
  });

  it("should render correctly", () => {
    expect(shallowTestComponent()).toMatchSnapshot();
  });

  it("should mount correctly", () => {
    expect(mountTestComponent().find(StyledPopover)).toBeDefined();
  });

  describe("if 'placement' is bottomStart", () => {
    beforeEach(() => {
      props.placement = Placement.bottomStart;
    });

    it("should mount correctly", () => {
      expect(
        mountTestComponent()
          .find(StyledPopover)
          .props()
      ).toMatchObject({
        left: 0,
        top: 8
      });
    });
  });

  describe("if 'placement' is bottomEnd", () => {
    beforeEach(() => {
      props.placement = Placement.bottomEnd;
    });

    it("should mount correctly", () => {
      expect(
        mountTestComponent()
          .find(StyledPopover)
          .props()
      ).toMatchObject({
        left: 0,
        top: 8
      });
    });
  });

  describe("if 'placement' is left", () => {
    beforeEach(() => {
      props.placement = Placement.left;
    });

    it("should mount correctly", () => {
      expect(
        mountTestComponent()
          .find(StyledPopover)
          .props()
      ).toMatchObject({
        left: -8,
        top: 0
      });
    });
  });

  describe("if 'placement' is leftStart", () => {
    beforeEach(() => {
      props.placement = Placement.leftStart;
    });

    it("should mount correctly", () => {
      expect(
        mountTestComponent()
          .find(StyledPopover)
          .props()
      ).toMatchObject({
        left: -8,
        top: 0
      });
    });
  });

  describe("if 'placement' is leftEnd", () => {
    beforeEach(() => {
      props.placement = Placement.leftEnd;
    });

    it("should mount correctly", () => {
      expect(
        mountTestComponent()
          .find(StyledPopover)
          .props()
      ).toMatchObject({
        left: -8,
        top: 0
      });
    });
  });

  describe("if 'placement' is right", () => {
    beforeEach(() => {
      props.placement = Placement.right;
    });

    it("should mount correctly", () => {
      expect(
        mountTestComponent()
          .find(StyledPopover)
          .props()
      ).toMatchObject({
        left: 0,
        top: 0
      });
    });
  });

  describe("if 'placement' is rightStart", () => {
    beforeEach(() => {
      props.placement = Placement.rightStart;
    });

    it("should mount correctly", () => {
      expect(
        mountTestComponent()
          .find(StyledPopover)
          .props()
      ).toMatchObject({
        left: 0,
        top: 0
      });
    });
  });

  describe("if 'placement' is rightEnd", () => {
    beforeEach(() => {
      props.placement = Placement.rightEnd;
    });

    it("should mount correctly", () => {
      expect(
        mountTestComponent()
          .find(StyledPopover)
          .props()
      ).toMatchObject({
        left: 8,
        top: 0
      });
    });
  });

  describe("if 'placement' is top", () => {
    beforeEach(() => {
      props.placement = Placement.top;
    });

    it("should mount correctly", () => {
      expect(
        mountTestComponent()
          .find(StyledPopover)
          .props()
      ).toMatchObject({
        left: 0,
        top: -8
      });
    });
  });

  describe("if 'placement' is topStart", () => {
    beforeEach(() => {
      props.placement = Placement.topStart;
    });

    it("should mount correctly", () => {
      expect(
        mountTestComponent()
          .find(StyledPopover)
          .props()
      ).toMatchObject({
        left: 0,
        top: -8
      });
    });
  });

  describe("if 'placement' is topEnd", () => {
    beforeEach(() => {
      props.placement = Placement.topEnd;
    });

    it("should mount correctly", () => {
      expect(
        mountTestComponent()
          .find(StyledPopover)
          .props()
      ).toMatchObject({
        left: 0,
        top: -8
      });
    });
  });

  describe("if 'setToTargetWidth' is true", () => {
    beforeEach(() => {
      props.setToTargetWidth = true;
    });

    it("should mount correctly", () => {
      expect(
        mountTestComponent()
          .find(StyledPopover)
          .props()
      ).toMatchObject({
        left: 0,
        top: 0
      });
    });
  });
});
