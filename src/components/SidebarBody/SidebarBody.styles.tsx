import styled from "styled-components";
import Flex from "../Flex";

const StyledSidebarBody = styled(Flex)`
  flex: 1 1 auto;
  flex-direction: column;
  padding: 0.5rem;
`;

export default StyledSidebarBody;
