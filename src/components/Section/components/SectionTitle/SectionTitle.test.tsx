import { ReactWrapper, ShallowWrapper } from "enzyme";
import { mountComponent, shallowComponent } from "../../../../utils/testUtils";
import SectionTitle, { SectionTitleProps } from ".";

describe("SectionTitle", () => {
  let props: SectionTitleProps;
  let mountSectionTitle: ReactWrapper | undefined;
  let shallowSectionTitle: ShallowWrapper | undefined;

  const mountTestComponent = (): ReactWrapper => {
    if (!mountSectionTitle) {
      mountSectionTitle = mountComponent(SectionTitle, props);
    }

    return mountSectionTitle;
  };

  const shallowTestComponent = (): ShallowWrapper => {
    if (!shallowSectionTitle) {
      shallowSectionTitle = shallowComponent(SectionTitle, props);
    }

    return shallowSectionTitle;
  };

  beforeEach(() => {
    props = {
      title: "Section"
    };

    mountSectionTitle = undefined;
    shallowSectionTitle = undefined;
  });

  it("should render correctly", () => {
    expect(shallowTestComponent()).toMatchSnapshot();
  });

  it("should mount correctly", () => {
    expect(mountTestComponent().find(SectionTitle)).toBeDefined();
  });
});
