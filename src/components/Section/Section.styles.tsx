import styled from "styled-components";
import { borderRadius, getColor, getShadow, transition } from "../../utils";
import { Color } from "../../types/enums";

export const StyledSection = styled.div<{ full?: boolean; shadow?: number }>`
  background-color: ${getColor(Color.gray, 0)};
  ${borderRadius};
  display: flex;
  flex-direction: column;
  ${props => getShadow(props.shadow as number)};
  height: max-content;
  overflow: hidden;
  ${props => props.full && "padding: 0"};
  ${transition("box-shadow")};
  width: max-content;
`;

export default StyledSection;
