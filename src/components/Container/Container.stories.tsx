import React from "react";

import ContainerReadme from "./README.md";
import Container from ".";

export default {
  component: Container,
  parameters: {
    info: {
      inline: true,
      text: ContainerReadme
    }
  },
  title: "Container"
};

export const BasicUse = (): React.ReactElement => (
  <Container>
    <span>Hello</span>
  </Container>
);
