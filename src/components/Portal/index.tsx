import React from "react";
import ReactDOM from "react-dom";
import { isNullOrUndefined } from "../../utils";

export interface PortalProps extends React.HTMLAttributes<HTMLDivElement> {
  node?: Element;
}

export const Portal = ({
  children,
  node: propNode
}: PortalProps): React.ReactElement => {
  let node: Element | null = document.querySelector("#portal");

  if (isNullOrUndefined(node)) {
    node = document.createElement("div");
    (node as any).tabIndex = "-1";
    node.id = "portal";
    (node as any).style = "padding: 0; width: 0; height: 0";
    document.body.appendChild(node);
  }

  if (!isNullOrUndefined(propNode)) {
    node = propNode;
  }

  return ReactDOM.createPortal(children, node);
};

export default Portal;
