import React from "react";
import styled, { css } from "styled-components";
import { Color } from "../../../../types/enums";
import {
  borderRadius,
  getColor,
  hexToRgb,
  transition
} from "../../../../utils";
import Flex from "../../../Flex";
import Icon, { StyledIcon } from "../../../Icon";

export const StyledTreeNode = styled.div`
  padding: 0;
  position: relative;
  &:before {
    background-color: rgba(
      ${props => hexToRgb(getColor(Color.purple, 100)(props))},
      0.32
    );
    content: "";
    height: 100%;
    left: calc(-0.125rem / 2);
    position: absolute;
    width: 0.125rem;
  }
`;

const TreeNodeChildren = styled.div`
  padding: 0 0 0 1rem;
`;

export interface TreeNodeProps
  extends Omit<React.HTMLAttributes<HTMLDivElement>, "title"> {
  actions?: React.ReactNode[];
  title: React.ReactNode;
}

const TreeNodeContent = styled(Flex)`
  justify-content: space-between;
  ${StyledIcon} {
    margin-right: -0.5rem;
  }

  ${Flex}:first-child {
    flex: 1 1 auto;
    min-width: min-content;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
  }
`;

const TreeNodeTitle = styled.div`
  ${props =>
    props.onClick &&
    css`
      ${borderRadius};
      cursor: pointer;
      ${transition("background-color")};
      &:hover {
        background-color: ${getColor(Color.gray, 125)};
      }
    `};
`;

const ActionContent = styled(Flex)`
  flex: 0 0 max-content;
`;

const TreeNode = ({
  actions,
  children,
  title,
  ...props
}: TreeNodeProps): React.ReactElement => {
  const [isOpen, setIsOpen] = React.useState(false);

  const showHideChildren = (): void => {
    setIsOpen(prevIsOpen => !prevIsOpen);
  };

  return (
    <StyledTreeNode>
      <TreeNodeContent>
        <Flex>
          {children && (
            <Icon
              className={`fa-chevron-${isOpen ? "down" : "right"}`}
              onClick={showHideChildren}
            />
          )}
          <TreeNodeTitle {...props}>{title}</TreeNodeTitle>
        </Flex>
        <ActionContent>{actions}</ActionContent>
      </TreeNodeContent>
      {children && isOpen && <TreeNodeChildren>{children}</TreeNodeChildren>}
    </StyledTreeNode>
  );
};

export default TreeNode;
