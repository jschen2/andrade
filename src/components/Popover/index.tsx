import React from "react";

import Portal from "../Portal";

import { Placement } from "../../types/enums";
import index from "../../utils/useCombinedRefs";
import { StyledPopover } from "./Popover.styles";

export interface PopoverProps extends React.HTMLAttributes<HTMLDivElement> {
  isOpen: boolean;
  offsetLeft?: number;
  offsetTop?: number;
  node?: Element;
  placement?: Placement;
  target: string;
  setToTargetWidth?: boolean;
  showArrow?: boolean;
}

export const Popover = React.forwardRef<HTMLDivElement, PopoverProps>(
  (
    {
      children,
      isOpen,
      node,
      offsetLeft,
      offsetTop,
      placement,
      target,
      setToTargetWidth,
      showArrow,
      ...props
    }: PopoverProps,
    ref: React.RefObject<HTMLDivElement>
  ): React.ReactElement | null => {
    const popoverRef = React.useRef();
    const combinedRef = index(ref, popoverRef);
    const [left, setLeft] = React.useState(0);
    const [top, setTop] = React.useState(0);
    const [width, setWidth] = React.useState<number>();

    const setPlacementVars = (): { left: number; top: number } => {
      const targetEl = document.getElementById(target);
      const {
        left: tLeft,
        height: tHeight,
        top: tTop,
        width: tWidth
      } = targetEl!.getBoundingClientRect();
      let nodeLeft = 0;
      let nodeTop = 0;
      let popoverWidth = 0;
      let popoverHeight = 0;

      if (combinedRef && combinedRef.current) {
        popoverWidth = (combinedRef.current as any).getBoundingClientRect()
          .width;
        popoverHeight = (combinedRef.current as any).getBoundingClientRect()
          .height;
      }

      if (node) {
        nodeLeft = node.getBoundingClientRect().left;
        nodeTop = node.getBoundingClientRect().top;
      }

      switch (placement) {
        case Placement.bottom:
          return {
            left: tLeft - popoverWidth / 2 + tWidth / 2,
            top: tTop + tHeight
          };
        case Placement.bottomStart:
          return {
            left: tLeft - nodeLeft,
            top: tTop - nodeTop + tHeight + 8
          };
        case Placement.bottomEnd:
          return {
            left: tLeft - nodeLeft - popoverWidth + tWidth,
            top: tTop - nodeTop + tHeight + 8
          };
        case Placement.left:
          return {
            left: tLeft - nodeLeft - popoverWidth - 8,
            top: tTop - nodeTop - popoverHeight / 2 + tHeight / 2
          };
        case Placement.leftStart:
          return {
            left: tLeft - nodeLeft - popoverWidth - 8,
            top: tTop - nodeTop
          };
        case Placement.leftEnd:
          return {
            left: tLeft - nodeLeft - popoverWidth - 8,
            top: tTop - nodeTop - popoverHeight + tHeight
          };
        case Placement.right:
          return {
            left: tLeft + tWidth,
            top: tTop - popoverHeight / 2 + tHeight / 2
          };
        case Placement.rightStart:
          return {
            left: tLeft + tWidth,
            top: tTop
          };
        case Placement.rightEnd:
          return {
            left: tLeft - nodeLeft + tWidth + 8,
            top: tTop - nodeTop - popoverHeight + tHeight
          };
        case Placement.top:
          return {
            left: tLeft - nodeLeft - popoverWidth / 2 + tWidth / 2,
            top: tTop - nodeTop - popoverHeight - 8
          };
        case Placement.topStart:
          return {
            left: tLeft - nodeLeft,
            top: tTop - nodeTop - popoverHeight - 8
          };
        case Placement.topEnd:
          return {
            left: tLeft - nodeLeft - popoverWidth + tWidth,
            top: tTop - nodeTop - popoverHeight - 8
          };
        default:
          return {
            left,
            top
          };
      }
    };

    React.useEffect(() => {
      const targetEl = document.getElementById(target);
      if (targetEl) {
        const { width: tWidth } = targetEl.getBoundingClientRect();
        const { left: pLeft, top: pTop } = setPlacementVars();
        setLeft(pLeft + (offsetLeft || 0));
        setTop(pTop + (offsetTop || 0));

        if (setToTargetWidth) {
          setWidth(tWidth);
        } else {
          setWidth(undefined);
        }
      }
    });

    if (isOpen) {
      return (
        <Portal node={node}>
          <StyledPopover
            {...{ left, placement, ref: combinedRef, top, width, showArrow }}
            {...props}
          >
            {children}
          </StyledPopover>
        </Portal>
      );
    }

    return null;
  }
);

Popover.displayName = "Popover";

Popover.defaultProps = {
  offsetLeft: 0,
  offsetTop: 0,
  placement: Placement.top,
  setToTargetWidth: false,
  showArrow: true
};

export default Popover;
