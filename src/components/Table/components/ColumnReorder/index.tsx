import cloneDeep from "lodash/cloneDeep";
import React from "react";

import Flex from "../../../Flex";
import {
  Alignment,
  ButtonSize,
  Color,
  Placement
} from "../../../../types/enums";
import { isNullOrUndefined, onOutsideClick } from "../../../../utils";
import CircleButton from "../../../CircleButton";
import Icon from "../../../Icon";
import Popover from "../../../Popover";
import ColumnButton from "../ColumnButton";
import { ColumnDragImage, ColumnDragLine } from "./ColumnReorder.styles";

export interface ColumnReorderProps {
  columns: {
    column: any;
    hidden: boolean;
  }[];
  id: string;
  setTableColumns(prevState: any): void;
  showHideColumn(index: number): () => void;
}

export const ColumnReorder = ({
  columns,
  id,
  setTableColumns,
  showHideColumn
}: ColumnReorderProps): React.ReactElement => {
  const flexRef: React.RefObject<any> = React.useRef();
  const popoverRef: React.RefObject<any> = React.useRef();
  const dragImageRef: React.RefObject<any> = React.useRef();
  const dragLineRef: React.RefObject<any> = React.useRef();
  const [startingTop, setStartingTop] = React.useState(0);
  const [isFilterOpen, setIsFilterOpen] = React.useState(false);
  const [replacedColumn, setReplacedColumn] = React.useState(-1);

  React.useEffect(() => {
    if (!isNullOrUndefined(flexRef.current)) {
      window.addEventListener(
        "click",
        onOutsideClick(flexRef.current, setIsFilterOpen)
      );

      return function cleanup() {
        window.removeEventListener(
          "click",
          onOutsideClick(flexRef.current, setIsFilterOpen)
        );
      };
    }

    return undefined;
  }, [flexRef.current]);

  const onDragStart = (str: string) => (e: React.DragEvent) => {
    const img = document.createElement("img");
    img.src =
      "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7";
    e.currentTarget.classList.add("dragging");
    e.dataTransfer.setDragImage(img, -5000, -5000);
    setStartingTop(e.clientY);
    dragImageRef.current.innerHTML = str;
    dragImageRef.current.style.left = `${e.clientX + 8}px`;
    dragImageRef.current.style.top = `${e.clientY + 8}px`;
    dragImageRef.current.style.opacity = 1;
    dragLineRef.current.style.opacity = 1;
  };

  const onDrag = (e: React.DragEvent): void => {
    e.persist();
    e.stopPropagation();

    dragImageRef.current.style.left = `${e.clientX - 16}px`;
    dragImageRef.current.style.top = `${e.clientY - 16}px`;
  };

  const onDragEnter = (idx: number) => (e: React.DragEvent) => {
    setReplacedColumn(idx);
    const { top, height } = e.currentTarget.getBoundingClientRect();
    const isGreater = startingTop < e.clientY;
    dragLineRef.current.style.top = `${top +
      (isGreater ? height : 0) -
      popoverRef.current.getBoundingClientRect().top}px`;
  };

  const onDragEnd = (idx: number) => (e: React.DragEvent): void => {
    e.persist();
    e.preventDefault();
    if (idx !== -1 && replacedColumn !== -1) {
      const newTableColumns = [...columns];
      const tempColumn = cloneDeep(newTableColumns[idx]);
      newTableColumns.splice(idx, 1);
      newTableColumns.splice(replacedColumn, 0, tempColumn);
      setTableColumns(newTableColumns);
    }
    e.currentTarget.classList.remove("dragging");
    dragImageRef.current.style.left = "";
    dragImageRef.current.style.top = "";
    dragImageRef.current.style.opacity = 0;
    dragLineRef.current.style.opacity = 0;
  };

  const openFilter = (): void => {
    setIsFilterOpen(true);
  };

  return (
    <Flex
      justifyContent={Alignment.flexEnd}
      ref={flexRef}
      style={{ position: "relative" }}
    >
      <CircleButton
        color={Color.purple}
        ghost={true}
        id={id}
        size={ButtonSize.sm}
        onClick={openFilter}
      >
        <Icon className="fa-filter" />
      </CircleButton>
      <Popover
        node={flexRef && flexRef.current}
        placement={Placement.bottomEnd}
        isOpen={isFilterOpen}
        target={id}
        showArrow={false}
        onDragOver={onDrag}
        ref={popoverRef}
        style={{ zIndex: 1 }}
      >
        {columns.map(({ column, hidden }, idx) => (
          <ColumnButton
            column={column}
            draggable={true}
            hidden={hidden}
            key={column.header}
            showHideColumn={showHideColumn(idx)}
            onDragStart={onDragStart(column.header)}
            onDragEnter={onDragEnter(idx)}
            onDragEnd={onDragEnd(idx)}
          >
            {idx + 1}
          </ColumnButton>
        ))}
        <ColumnDragLine ref={dragLineRef} />
        <ColumnDragImage ref={dragImageRef} />
      </Popover>
    </Flex>
  );
};

export default ColumnReorder;
