import { ReactWrapper, ShallowWrapper } from "enzyme";
import { mountComponent, shallowComponent } from "../../utils/testUtils";

import { TextAreaProps } from "./TextArea.styles";
import TextArea from ".";

describe("TextArea", () => {
  let props: TextAreaProps;
  let mountTextArea: ReactWrapper | undefined;
  let shallowTextArea: ShallowWrapper | undefined;

  const mountTestComponent = (): ReactWrapper => {
    if (!mountTextArea) {
      mountTextArea = mountComponent(TextArea, props);
    }

    return mountTextArea;
  };

  const shallowTestComponent = (): ShallowWrapper => {
    if (!shallowTextArea) {
      shallowTextArea = shallowComponent(TextArea, props);
    }

    return shallowTextArea;
  };

  beforeEach(() => {
    props = {};

    mountTextArea = undefined;
    shallowTextArea = undefined;
  });

  it("should render correctly", () => {
    expect(shallowTestComponent()).toMatchSnapshot();
  });

  it("should mount correctly", () => {
    expect(mountTestComponent().find(TextArea)).toBeDefined();
  });
});
