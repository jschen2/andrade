import styled from "styled-components";
import Container, { ContainerProps } from "../Container";

export interface GridProps extends ContainerProps {
  colGap?: string;
  cols?: number | string;
  gap?: string;
  rowGap?: string;
  rows?: number | string;
}

export const Grid = styled(Container)<GridProps>`
  display: grid;
  grid-template-columns: ${props =>
    typeof props.cols === "number" ? `repeat(${props.cols}, 1fr)` : props.cols};
  grid-gap: ${props => props.gap};
  grid-column-gap: ${props => props.colGap};
  grid-row-gap: ${props => props.rowGap};
  grid-template-rows: ${props =>
    typeof props.rows === "number" ? `repeat(${props.rows}, 1fr)` : props.rows};
  height: 100%;
`;

Grid.displayName = "Grid";

Grid.defaultProps = {
  colGap: "1rem",
  cols: 1,
  rowGap: "1rem",
  rows: 1
};

export default Grid;
