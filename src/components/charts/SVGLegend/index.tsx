import React from "react";
import styled from "styled-components";
import { Color } from "../../../types/enums";
import { getColor, getItemWidth } from "../../../utils";

export interface SVGLegendProps {
  names: string[];
  width: number;
}

const LegendCircle = styled.circle<{ index: number }>`
  fill: ${props =>
    getColor(Object.keys(Color)[props.index] as Color, 500)(props)};
`;

const SVGLegend = ({ names, width }: SVGLegendProps): React.ReactElement => {
  let previousStart = 0;
  return (
    <g>
      {[...names].reverse().map((name: string, index: number) => {
        previousStart += getItemWidth(name);
        if (index !== 0) {
          previousStart += 8;
        }
        return (
          <React.Fragment key={name}>
            <LegendCircle
              r={4}
              cx={width - previousStart - 8}
              cy={8}
              index={names.length - 1 - index}
            />
            <text x={width - previousStart} y={14}>
              {name}
            </text>
          </React.Fragment>
        );
      })}
    </g>
  );
};

export default SVGLegend;
